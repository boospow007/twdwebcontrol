var express 	= require('express')
var nodemailer = require('nodemailer') 
var router 		= express.Router()
var dt 			= require('../resource/datetime') 
var request		=require('request')
var db			=require('../resource/dbconnect') 
var mydb		=require('../resource/mysqlconnect') 
const sharp = require('sharp');
/* 
 * call gate to inet via servic ein upfront environment port 82
 */
router.use(function timeLog (req, res, next) {
	  console.log('GW : ', dt.myDateTime())
	  next()
	})
 
 //---Copy Pim Product from Upfront PIM  to CLoud PIM
router.get('/PLPP/:stcode/:skcode' , function (req, res) {
	var skc 	= req.params.skcode;
	var stc 	= req.params.stcode;
    var url 	= 'http://localhost:82/INET/PLPP/' +stc+ '/'+skc;
    request.get(url, function(error, response, body){
    	res.status(200).send(body);
    });
});
 
module.exports = router