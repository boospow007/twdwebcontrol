var express 	= require('express')
 
var router 		= express.Router()
var dt 			= require('../resource/datetime') 
var request		=require('request')
var db			=require('../resource/dbconnect') 
const sharp = require('sharp');
var fs 		=require('fs')
// https://sharp.pixelplumbing.com/en/stable/api-input/

router.use(function timeLog (req, res, next) {
	  console.log('Call Image Router.   Time: ', dt.myDateTime())
	  next()
	})
	
router.post('/URLTODB' , function (req, res) {
    
	var imgurl  =	req.body.url;
	var cat		= 	req.body.cat;
	cat=(cat+'00000000').slice(0,6);
	console.log('Save url  ' + imgurl +' to DB '+cat)

	request({url: imgurl, encoding: null}, 
			function(err, result, bodyBuffer) {
				if(err){
					console.log('Image2DB .'+err);
					res.end('Image2DB ERR Cant get Image from '+imgurl)
				}
				else {
					sharp.cache(false);
					sharp(bodyBuffer)
					.resize(630,340)  //.resize(500,500)
			    	.toBuffer()
			    	.then( data=>{
			    			var buf =data.toString('base64');
			    			var cmd="Update Catimage Set BASE64='@base' where CATCODE = '@cat'";
			    				 var pdata={
			    						 xid:0,
			    						 qry:cmd,
			    						 par:{
			    							 cat:cat,
			    							 base:buf
			    						    }
			    				 	       }
			    				  db.execute(pdata,function(resp){  //-----return with req ID
			     					   if(resp.dbcode==0){
			     						  res.send('imageURL2DB Copy & Resize Image Successful')
			     					   }
			     					   else
			     						  res.send('imageURL2DB ERR '+resp.dbmessage);
			    				 });
			    				 
			    			   })
			    			   
			    	.catch( err => {
			    		     console.log('Sharp Error '+err);
			    		     res.send('image2DB ERR '+err)
			    			} 
			    	      );
					
				}

		});

});
 
router.post('/FILETODB' , function (req, res) {
 
	//var fname  =req.body.fname; 
	var fname  ='./public/uploads/userPhoto.jpg'; // req.body.fname;
	
	var cat = req.body.cat;
	cat=(cat+'00000000').slice(0,6);

	console.log('Convert file '+fname +' to DB '+cat);
	
	sharp.cache(false);
	sharp(fname)		
		.resize(630,340)  //.resize(500,500)
		.toBuffer() 
		.then( async data=>{
				var buf =data.toString('base64');				
				var cmd=" Update catimage Set BASE64='@base' where catCode='@cat'";
				 
				cmd=cmd.replace('@cat',cat).replace('@base',buf);
				 //console.log(cmd);


				//test   
				// fs.writeFile("./public/uploads/"+Date.now()+"-"+cat+".jpg", buf, {encoding: 'base64'}, function(err){
				//    //Finished
				// });
				//test
				 
				db.dbGetQuery(cmd,function(resp){  //-----return with req ID
					if(resp.dbcode==0){
						var msg="Update CatImage OK"
						if(resp.dbrows>0)msg="Cannot Update Image"

						console.log(msg);
						
						/*--remove file ???
						fs.unlink(fname, function (err) {
							if (err) throw err;
							// if no error, file has been deleted successfully
							console.log('File deleted!');
						}); 
						**/
						
						res.status(200).end('Copy & Resize Image to CAT ' +cat +' '+msg);
					}
					else {
						console.log('Save image Error ' +resp.dbmessage);
						res.sendStatus(200).end('ERR '+resp.dbmessage);
					}
								
				});
				buf=null;
						
			})
					
		.catch( err => {
					console.log('Sharp Error '+err);
					res.send('image2DB ERR '+err);
				} 
				);
		
		

});

router.post('/BASE64TODB' , function (req, res) {
	var imageData  	= req.body.base64;  //'./public/uploads/userPhoto.jpg'; // req.body.fname;
	var cat 		= req.body.cat;
	cat=(cat+'00000000').slice(0,6);
	console.log('Convert Base64  to DB '+cat);

	var img = new Buffer(imageData, 'base64');
	sharp.cache(false);
    sharp(img)	
	.resize(630,340)  //.resize(500,500)
	.toBuffer()
	.then( data=>{
		var buf =data.toString('base64');
		var cmd=" Update catimage Set BASE64='@base' where catCode='@cat'";
		var pdata={
			xid:0,
			qry:cmd,
			par:{
				cat:cat,
				base:buf
			}
			}

		db.execute(pdata,function(resp){  //-----return with req ID

					if(resp.dbcode==0){
						console.log('Save base 64 image OK')
						res.status(200).end('base64db Copy & Resize Image Successful')
					}
					else {
						console.log('Base64image Error ' +resp.dbmessage)
						res.sendStatus(200).end('Base 64 ERR '+resp.dbmessage);
					}
						
				});
			}) 
	.catch( err => {
					console.log('Sharp Error '+err);
					res.send('image2DB ERR '+err)
				} 
		 );		
	 
});

router.post('/TOFILE' , function (req, res) {
	 
	var imgurl  =req.body.imgurl;
	var outputFile = req.body.tofile;
	
	//var imgurl = "https://media-cdn.tripadvisor.com/media/photo-s/0d/07/48/7e/caption.jpg"
	//var outputFile ="./public/uploads/userPhoto.jpg";

	console.log('copy ['+imgurl +'] to file '+outputFile)
	//var fname=para.fname;
	//var url ="ubu.upfront.co.th./TWDPIM/web/Image/0104/60176103.jpg";
//	var url ="ubu.upfront.co.th./TWDPIM/web/"+fname;
	//var outputFile ="D:/DEvelop/Node/posbynode/public/POSIMAGE/"+skc+".jpg";
	//var outputFile ="./PRODICT/image/sku"+skc+".jpg";

	request({url: imgurl, encoding: null}, 
			function(err, result, bodyBuffer) {
				if(err){
					console.log('can not get image');
					res.end('ERR Cant get Image '+fname)
				}
				else {
					sharp.cache(false);
					sharp(bodyBuffer)					
					.resize(630,340)  //.resize(500,500)
			    	.toFile(outputFile ,
			    	    function(err){
			    			if(err){
			    				  console.log(err);
			    				  res.send('ERR '+err)
			    			     }
			    			else  {
			    				console.log('Resize Image Successful');
			    				res.send('Copy & Resize Image Successful')
			    			   }
			    			}
			    	 );
				}

		});

});
 
module.exports = router