const 	config = require('config');
const request = require('request')
var  msConfig = config.get('mssqlConfig');
var http=require('http')
const querystring = require('querystring');


exports.dbGetQuery = function(sql,callBack){
   try { 
	   var dfresp ={
				dbcode:999,
				dbmessage:"Start",
				dbrows:0,
				dbfields:null,
		   		dbitems:null
            }
	   
	var posconfig =config.get('posmenu');
	var defaultdb =posconfig.get('defaultdb');
	
			if(sql.indexOf('^')< 0)
				{
					msConfig = config.get('mssqlConfig');
					getMsQuery2(sql,function(resp){
						callBack(resp);
					});
					
				}
			 else if(sql.indexOf('use.PLPPSRV^')>=0){
				//------------connect to Cloud Server
			   var qry =sql.split("^")[1];
			   //console.log('CAll WEB SErvice '+sql)
				   getPLPPService(qry,function(resp){
					   callBack(resp);
				 });
			
			}
			else if(sql.indexOf('use.CLOUD^')>=0){  //get data from CLOUD POS 
				//------------connect to Cloud Server
			   var qry =sql.split("^")[1];
				msConfig = config.get('cloudConfig');
				var host=msConfig.host;
					getMsQuery2(qry,function(resp){
						callBack(resp);
					});
				   
			}
			else if(sql.indexOf('use.PLPP^')>=0){
				//------------connect to Cloud Server
			   var qry =sql.split("^")[1];
				msConfig = config.get('plppConfig');
				var host=msConfig.host;
					getMsQuery2(qry,function(resp){
						callBack(resp);
					});
				  
			}
			else if(sql.indexOf('use.MYSQL^')>=0){  //--connect to MYsql
				 //------------connect to Cloud Server
				var qry =sql.split("^")[1];
				 var  mydb =require('../resource/mysqlconnect');
			    mydb.myGetQuery(qry,function(rset){
				   callBack(rset);
				   return;
			   });
			 }
			else
			 {
				 msConfig = config.get('mssqlConfig');
					getMsQuery2(sql,function(resp){
						callBack(resp);
					});
				 
			 }
   }
   catch(ex){
	   console.log('dbGetQuery ['+sql +'] return Error '+ex.message);
	   dfresp.dbmessage="dbGetQuery "+ex.message;
	   callBack(dfresp);
   }
}  
//--------excute with para ----
exports.execute =function(req,callBack){
	try { 
	//console.log(req);
	var para=req.par;
	var xid =req.xid;
	
	if ((xid===null)|| (typeof(xid)==='undefined') ){
		 xid='0';
	}


	var cmd =req.qry;
	//-------Replace cmd with parameter
	for(key in para){
		var rp =RegExp('@'+key,'g');
		cmd=cmd.replace(rp,para[key]);
	}
	//console.log(cmd);
	//-------exucute
	  //getMsQuery(cmd,function(resx){
		 msConfig = config.get('mssqlConfig');
		 getMsQuery2(cmd,function(resp){
		 callBack(resp);
	  });
	}
	catch(ex){
		console.log('db execute errr.. '+ex.message);
		callBack('db Exec Cause System Error');
	}
}
 
//-------remote call to dbGateway by Default------
function getRmQuery(host,sql,callBack){
	//console.log(sql);
	   var resp ={
				dbcode:999,
				dbmessage:"Start",
				dbfields:null,
		   		dbitems:null
             }
	try {    
	   console.log( 'Remote Conn to ..'+host);

	   var pdata={query:sql};

	   request.post({
		  url:     host,
		  body:  querystring.stringify(pdata)
		}, function(error, response, body){  //response return the wole response 
		 	//console.log(error);
			var jsn=JSON.parse(body);
		   // console.log(jsn);
			//------Convert to Our Format
			resp.dbcode		=jsn.Error.code;
			resp.dbmessage	=jsn.Error.message;
			resp.dbfields	=null;
			if(jsn.Item!=null){ 
				resp.dbitems	=jsn.Item;
				resp.dbrows		=jsn.Item.length;
				}
		//	console.log(resp);
	   		callBack(resp);
		}).setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');

	}
catch(ex){
	console.log('Get GWQuery Error' +ex.message);
	resp.dbmessage=ex.message;
	callBack(resp);}
}




//-----------MSSQL QUERY -------
//var  msConfig = config.get('mssqlConfig');
function   getCldQuery(sql,callback){
	 msConfig = config.get('cloudConfig');
	 getMsQuery2(sql,function(resp){
		callback(resp);
	});
}
 

function   getMsQuery2(sql,callback){
  //console.log(sql);
	try { 
	var Connection 	= require('tedious').Connection;
	var Request 	= require('tedious').Request; 
	var dbName		= msConfig.dbName;
	  var resp ={
				dbcode:999,
				dbmessage:"ERROR",
				dbrows:0,
				dbfields:null,
		   		dbitems:null
		   };
	  
	var config = {
	    server: msConfig.host, //"vpn.upfront.co.th",
		char_set:"utf8",
	    options:msConfig.options,
	    authentication: {
			    type: "default",
			    options: {  
			        userName: msConfig.userid, //"upfdba",
			        password: msConfig.password //"upfrontadmin"
			      }
				 }
	     }

 var connection = new  Connection(config); 
	
	connection.on('error'
      , function(err) {
				//console.log("MSSQL Connect err...\n"+err);
			    ecode=999;
			    emsg=err;
			    
		    	 resp.dbcode	=ecode;
				 resp.dbmessage	=emsg; 
				// callback(resp);
			    // return;
	        });
   
  connection.on('connect'
	, function(err) {
		if(err){
			  console.log("MSSQL Connect err...\n"+err);
			  resp.dbmessage=err.message;
			  callback(resp);
			  connection.close();
			 //  return;
			}
	else { 
    var results = []; 
    var fields	=[];
    var ecode =0;
    var emsg="OK";
    var rqerr="0";
    
    var request = new Request(sql
			,function(err, rowCount,rows) {
			  if (err) {
				 // ecode	=err['number']; //???No such value
				  emsg	=err['message'];
				  rqerr =emsg;
			      console.log('Ms Req Error ..'+err);
			      //---wait for event on error to report Error >>>
			      
			  } else {
				 console.log('Read db '+ rowCount + ' rows');
		    	//  callback(resp);
		   		 // connection.close();
				}
			});
    
    	request.on('columnMetadata', function (columns) { 
    		var item = {}; 
			columns.forEach(function (column) { 
				item[column.colName] =column.type.name;
				}); 
			fields.push(item); 
    	});
    	
      request.on("row", function (columns) { 
			var item = {}; 
			columns.forEach(function (column) { 
				item[column.metadata.colName] =column.value
				}); 
				results.push(item); 
		    }); 
       /*
      request.on("error", function (err) { 
    	  ecode	=err['number'];
    	  emsg =err['RequestError'];
    	   console.log('msGetQuery error ..'+emsg +' on database '+dbName);
    	   //callback(resp);
		    }); 
      */
      request.on('requestCompleted',function(){
    	  if(rqerr!='0'){
    			 resp.dbcode	=888;
    			 resp.dbmessage	=rqerr; 
    	  }
    	  else { 
	    	 resp.dbfields	=fields;
	    	 resp.dbitems	=results;
	    	 resp.dbcode	=ecode;
	    	 resp.dbrows	 =results.length;
			 resp.dbmessage	=emsg;
	    	  }
    	  //console.log('MsQuery Completed with '+resp.dbcode);
    	  callback(resp);
   		  connection.close();
      	}); 
      
      connection.execSql(request);
		}
	});
	}
	catch(ex){
		console.log('getQuery2 Error' +ex.message);
		resp.message="Error..."+ex.message
		callback(resp);
	} 
  
} 
 
 //-------remote call to dbGateway by Default------
 function getPLPPService(qry,callBack){
	var pConfig = config.get('posmenu');
	var host 	= pConfig.plppgateway;

	//var host="http://twpim.com:84/WEBSERV/QUERY"
	
	console.log('WEB service '+ host +',Query: '+ qry);
	  var resp ={
			   dbcode:999,
			   dbmessage:"Start",
			   dbfields:null,
				  dbitems:null
			}

		try {    	 
			//  var pdata={query:qry};
			request.post(host, {
				json: {
					query: qry
					}
				}, (error, res1, body) => {
				if (error) {
					console.log('Web Serv error '+error)
					callBack(error)
					return
					}
					console.log(body)	
					callBack(body)
					})
	
					/*
				request(host, function (error, response, body) {
					
					console.error('error:', error); // Print the error if one occurred
					console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					console.log('body:', body); // Print the HTML for the Google homepage.
					
					if(response.statusCode !='200')callBack('Can not Call Server '+ host)
					else callBack(JSON.parse(body))
					});
			*/
   }
catch(ex){
   console.log('Get GWQuery Error' +ex.message);
   resp.dbmessage=ex.message;
   callBack(resp);}
} 