const 	config = require('config');

exports.cldGetQuery = function(sql,callback){
			getCldQuery(sql,function(ret){
						callback(ret);
			 });
}  

exports.chkCldConn = function(host,callback){
	   chkCldConn(host,function(err,ret){
				callback(err,ret);
	 });
}  
//--------excute with para ----
exports.execute =function(req,callBack){
	var xid =req.xid;
	
	if ((xid===null)|| (typeof(xid)==='undefined') ){
		var resp={
				xid:null,
				dbcode:999,
				dbmessage:"Try to exec but No xid Paraemter Specify",
				dbitems:null
		  }
		callBack(resp)
	}
	var para=req.par;
	var cmd =req.qry;
	//-------Replace cmd with parameter
	for(key in para){
		var rp =RegExp('@'+key,'g');
		cmd=cmd.replace(rp,para[key]);
	}
	//-------exucute
	getCldQuery(cmd,function(resx){
 
		var resp={
				xid:xid,
				dbcode:resx.dbcode,
				dbmessage:resx.dbmessage,
				dbitems:resx.dbitems
		  } 
		callBack(resp);
	});
	 
}
var http=require('http')
const querystring = require('querystring');

//-----------MSSQL QUERY -------
const msConfig = config.get('cloudConfig');
function   getCldQuery(sql,callback){
	//console.log(sql);
	
	var Connection 	= require('tedious').Connection;
	var Request 	= require('tedious').Request; 
	var dbName		= msConfig.dbName;
	  var resp ={
				dbcode:999,
				dbmessage:"ERROR",
				dbfields:null,
		   		dbitems:null
		   };
	  
	var config = {
	    server: msConfig.host, //"vpn.upfront.co.th",
		char_set:"utf8",
	    options:msConfig.options,
	    authentication: {
			    type: "default",
			    options: {  
			        userName: msConfig.userid, //"upfdba",
			        password: msConfig.password //"upfrontadmin"
			      }
				 }
	     }

 var connection = new  Connection(config); 
	connection.on('error'
      , function(err) {
				console.log("MSSQL Connect err..."+err);
			    ecode=999;
			    emsg=err;
			    
		    	 resp.dbcode	=ecode;
				 resp.dbmessage	=emsg; 
				// callback(resp);  //---call back will cause err?????
			     return;
	        });
      
  connection.on('connect'
	, function(err) {
		if(err){
			  console.log("MSSQL Connect err...\n"+err);
			  resp.dbmessage=err;
			  callback(resp);
			  return;
			}
		
    var results = []; 
    var fields	=[];
    var ecode =0;
    var emsg="OK";
    
    var request = new Request(sql
			,function(err, rowCount) {
			  if (err) {
				  ecode	=err['number'];
				  emsg	=err['message'];
			    	console.log('error ..'+emsg );
			  } else {
					//console.log('Read db '+ rowCount + ' rows');
				}
			});
    
    	request.on('columnMetadata', function (columns) { 
    		var item = {}; 
			columns.forEach(function (column) { 
			   /**
				item ={ cname:column.colName,
						ctype:column.type.name,
						clen:column.dataLength
						}
				**/
				item[column.colName] =column.type.name;
			
				}); 
			fields.push(item); 
    	});
    	
      request.on("row", function (columns) { 
			var item = {}; 
			columns.forEach(function (column) { 
				item[column.metadata.colName] =column.value
				}); 
				results.push(item); 
		    }); 
      /*
      request.on("error", function (err) { 
    	  ecode	=err['number'];
    	  emsg =err['RequestError'];
    	   console.log('msGetQuery error ..'+emsg +' on database '+dbName);
    	   //callback(resp);
		    }); 
      */
      request.on('requestCompleted',function(){
    	 resp.dbfields	=fields;
    	 resp.dbitems	=results;
    	 resp.dbcode	=ecode;
		 resp.dbmessage	=emsg;
		 connection.close();  //---close connection
    	 //  console.log(resp);
    	  	callback(resp);
      	}); 
      
      connection.execSql(request);
	});
  
  
};

function   chkCldConn(host,callback){
	//console.log(sql);
	
	var Connection 	= require('tedious').Connection;
	var Request 	= require('tedious').Request; 
	   
	var config = {
	    server: msConfig.host, //"vpn.upfront.co.th",
		char_set:"utf8",
	    options:msConfig.options,
	    authentication: {
			    type: "default",
			    options: {  
			        userName: msConfig.userid, //"upfdba",
			        password: msConfig.password //"upfrontadmin"
			      }
				 }
	     }

 var cerr =false;
var  emsg="OK";
 var connection = new  Connection(config); 

	connection.on('error'
      , function(err) {
    	  			connection.close();
    	  			console.log('conn err '+err);
				// callback(cerr,emsg);
			     return;
	        });
	
	connection.on('end'
			,function(err){
				console.log('chk cld end '+err);
				callback(cerr,emsg);
			});
	
  connection.on('connect'
	, function(err) {
		cerr=err;
		emsg=err.message;
		connection.close();
		/*
		if(err){
			   callback(err,"Connection Error");
			  return;
			}
		else {
			// callback(err,"Connection OK");
			 return;
			}
			*/
	});
  
}
		 
