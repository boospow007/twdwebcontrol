var xcat;
var xclass;

function prodReady(){
    xcat  = $('#wxcat').text().trim().substring(0,4);
    xclass  = $('#wxclass').text();
    var rw=0;
    $('.filterTd').each(function(){
        var fval = $(this).text();
        var dvals =fval.split('|');

         createFilter(rw,$(this), dvals)

         rw+=1;
        });
    loadCProduct();

    getCategoryHaveNoProduct();
}

function getCategoryHaveNoProduct() {
  var qry = `select distinct c.class, cbu.catname
            from twdpim..catproperty c
            join twdpim..catbyuser cbu on cbu.BYCODE = c.BYCODE and c.CLASS = left(cbu.CATCODE, 6)
            where c.bycode = '${xcat}'
            and c.class not in (select distinct class 
                        from twdpim..catproduct2 
                      where bycode = '${xcat}')
            order by c.class`
  ajaxGetQuery(qry, (isok, resp) => {
    var jsn = JSON.parse(resp)
    if(isok){
      if(jsn.dbrows > 0) {
        (jsn.dbitems).forEach((itm, idx, arr) => {
          $('#newclass').append(`<option value='${itm.class}'>${itm.class + ' - ' + itm.catname}</option>`)
        })
      }
    } else {
      alert(jsn.dbmessage)
    }
  })
}

function createFilter(rw,obj, dvals){
    var  id=rw*100;
    var  htm ="";
    $(obj).html('');
    for(var i=0;i<  dvals.length;i++){
        var qry = ` USE TWDPIM
                    select SK_CODE 
                    FROM (Select b.skcode as SK_CODE,
                                 PROP=dbo.getProperty('${xcat}','${xclass}',b.skcode)
                    from CLOUDPOS..EPROD a right join CATPRODUCT2 b on a.sk_code=b.skcode
                    Where  BYCODE ='${xcat}' and CLASS  ='${xclass}') lst 
                    where lst.PROP like '%|${dvals[i]}|%'`;
        if(dvals[i] == 'ALL') {
            var idx=rw*100 + i +1;
            htm ="<span class='dvalSpn' id='SP" +idx +"' onclick='javascript:spnclick(this)'>" + dvals[i] + "</span>";
            $(obj).append(htm);
            continue;
        }
        chkCondition(qry, i, rw, dvals[i], htm, obj);
     }
    // $(obj).html(htm);
 
//   console.log(htm)
}

function chkCondition(qry, rnd, rw, val, htm, obj){
    ajaxGetQuery(qry, (isok, ret) => {
        var jsn = JSON.parse(ret);
        if(isok){
            if(jsn.dbrows > 0){
                var idx=rw*100 + rnd +1;
                if (val>' ') {
                   htm ="<span class='dvalSpn' id='SP" +idx +"' onclick='javascript:spnclick(this)'>" + val + "</span>";
                   $(obj).append(htm);
                }
            } else {

            }
        } else {
            alert(jsn.dbmessage)
        }
    })
}
//-------------Function Load Product
var sqlcond="";
 

function loadCProduct(){
    var xcat    = $('#wxcat').text().trim();
    var xclass  = $('#wxclass').text();
    xcat=xcat.substring(0,4)  

  //  alert(xclass)  
    var rowbtndelete = (profileList.indexOf('0066') > -1) ? `<td class='delete-action'><center><img src='../images/delete-button.png' onclick="javascript:deleteSKU('${xcat}','${xclass}',^SK_CODE^);"></center></td>` : '';
    var colbtndel = (profileList.indexOf('0066') > -1) ? '<th>DELETE</th>' : '';
    var pdata={patname:'CATEGORY3/prodinUcat.txt',
            para:{
                ucat:xcat,
                uclass:xclass,
                cond:sqlcond ,
                rowbtndelete: rowbtndelete,
                colbtndel: colbtndel}
                }
    
// console.log(JSON.stringify(pdata))

          ajaxXpandFile(pdata,function(isok,ret){
            $('#wprodDiv').html(ret);
            
            var tb = $('#prodTab tbody tr:eq(0) td:eq(0)').text();
            if(tb == 'No Result from DB') {
                $('#btn-copycategory').attr('disabled',true);
            } else {
                $('#btn-copycategory').attr('disabled',false);
            }
    });
 }

var selDim  =[null,null,null,null,null ,null,null,null];
var selDimVal   =['ALL','ALL','ALL','ALL','ALL','ALL','ALL','ALL'];
var dimName     =['','','','','','','','','']

function spnclick(obj){
    //--reset background color
    $('.dvalSpn').css('background-color','#aafada')
  //  $(obj).css('background-color','gray')

    var ids         =$(obj).attr('id');
    var id          =ids.replace('SP','')
    var rw          =parseInt(id/100)

    console.log(ids +' rw '+rw);
  
    selDim[rw]      =obj;
    selDimVal[rw]   =$(obj).text().trim();
    
    dimName[rw]= $('#filterTab tr:eq(' +rw +') td:eq(0)').text()  
    $('#findSp').text('Filter by ' 
        +dimName[0] +'=' +selDimVal[0]+','
        +dimName[1] +'=' +selDimVal[1]+','
        +dimName[2] +'=' +selDimVal[2]+','
        +dimName[3] +'=' +selDimVal[3]+','
        +dimName[4] +'=' +selDimVal[4]);
 //--------Convert below to loop to cover all property
    //---for each dimension
    $(selDim[0]).css('background-color','gray')
    $(selDim[1]).css('background-color','gray')
    $(selDim[2]).css('background-color','gray')
    $(selDim[3]).css('background-color','gray') 
    $(selDim[4]).css('background-color','gray') 
    $(selDim[5]).css('background-color','gray') 
    $(selDim[6]).css('background-color','gray') 
    sqlcond="";
 
    if(selDimVal[0]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP1')='"+ selDimVal[0]  +"' "  
    if(selDimVal[1]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP2')='"+ selDimVal[1] + "' "  
    if(selDimVal[2]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP3')='"+ selDimVal[2] + "' "  
    if(selDimVal[3]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP4')= '"+ selDimVal[3] + "' "  
    if(selDimVal[4]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP5')= '"+ selDimVal[4] + "' "  
    if(selDimVal[5]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP6')= '"+ selDimVal[5] + "' "  
    if(selDimVal[6]!='ALL') sqlcond += " and JSON_VALUE(PROP,'$.PROP7')= '"+ selDimVal[6] + "' "  
console.log('Cond '+sqlcond)

    loadCProduct();
    //---------
   
}

function openModalCopyCategory (){
  $('#newclass').val('')
  $('#md-copyclass').modal('show');
}

function copyCategory(ucat, uclass){
    var newclass = $('#newclass').val();
    if (newclass == ''){
      swalConfirm("warning", "กรุณาระบุ", 'Dept / Subdept / Class');
      // $('#md-copyclass').modal('hide');
      return 
    }

    var qry = `exec twdpim..copyCategory '${ucat}','${uclass}','${newclass}'`;
    var pdata = {
        query: qry
    }
    ajaxExec(pdata, (isok, ret) => {
        var jsn = JSON.parse(ret);
        if(jsn.dbcode == 0) {
            swalAl("success", "Successful", `Copy ข้อมูลจาก ${ucat + " : " + uclass}\r\nไปยัง ${ucat + " : " + newclass} สำเร็จ`);
            $('#md-copyclass').modal('hide');
            changeCat();
        } else {
            swalConfirm("error", "Failed", jsn.dbmessage);
            return;
        }
    })
}

function deleteSKU(ucat, uclass, skc) {
    swal({
        title: "Do you want to delete?",
        text: "SKU: " + skc,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
      },
      function () {
        try {
          var qry = "delete TWDPIM..CATPRODUCT2 where BYCODE='@ucat' and CLASS='@uclass' and SKCODE=@skc";
          var pdata = {
            query: qry,
            para: {
              ucat: ucat,
              uclass: uclass,
              skc: skc
            }
          }
          ajaxExec(pdata, function (isok, resp) {
            if (isok) {
              var jsn = JSON.parse(resp);
              if (jsn.dbcode == 0) {
                swalAl("success", "Successful", 'Deleted SKU:' + skc + ' successful');
                //check twdpim..catproduct2
                var qry = `select * from TWDPIM..catproduct2 where class = '${uclass}' and bycode = '${ucat}'`;
                ajaxGetQuery(qry, (isok, ret) => {
                  var jsn = JSON.parse(ret);
                  if(isok){
                    if(jsn.dbrows > 0){
                      showProd(uclass);
                    } else {
                      $('#CN888_' + uclass).html('');
                      if(uclass.substring(2,6) == '0000') {
                        $('#CN' + uclass.slice(0,2)).attr('cnode', 'N');
                        clickCat(uclass.slice(0,2));
                      } else if (uclass.substring(4,6) == '00') {
                        $('#CN' + uclass.slice(0,4)).attr('cnode', 'N');
                        clickCat(uclass.slice(0,4));
                      } else {
                        $('#CN' + uclass).attr('cnode', 'N');
                        clickCat(uclass);
                      }
                    }
                  } else {
                    swalAl("error", "Failed", jsn.dbmessage);
                  }
                })
              } else {
                swalAl("error", "Failed", jsn.dbmessage);
              }
            }
          })
        } catch (ex) {
          swalAl("error", "Failed", ex.message);
        }
      }
    );
  }

  function deleteAllSKUs(ucat, uclass) {
    var rowCount = $('#prodTab tbody tr').length;
    var haveSKU = ($('#prodTab tbody tr:eq(0)').find("td:eq(0)").text() == "No Result from DB") ? false : true;
    // console.log(chkResult);
    if (rowCount >= 1 && haveSKU) {
      swal({
          title: "Do you want to delete?",
          text: "All SKUs",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false,
        },
        function () {
          try {
            var qry = "delete TWDPIM..CATPRODUCT2 where BYCODE='@ucat' and CLASS='@uclass'";
            var pdata = {
              query: qry,
              para: {
                ucat: ucat,
                uclass: uclass,
              }
            }
            ajaxExec(pdata, function (isok, resp) {
              if (isok) {
                var jsn = JSON.parse(resp);
                if (jsn.dbcode == 0) {
                  swalAl("success", "Successful", 'Deleted all SKUs successful');

                  $('#CN888_' + uclass).html('');
                  if(uclass.substring(2,6) == '0000') {
                    $('#CN' + uclass.slice(0,2)).attr('cnode', 'N');
                    clickCat(uclass.slice(0,2));
                  } else if (uclass.substring(4,6) == '00') {
                    $('#CN' + uclass.slice(0,4)).attr('cnode', 'N');
                    clickCat(uclass.slice(0,4));
                  } else {
                    $('#CN' + uclass).attr('cnode', 'N');
                    clickCat(uclass);
                  }

                } else {
                  swalAl("error", "Failed", jsn.dbmessage);
                }
              }
            })
          } catch (ex) {
            swalAl("error", "Failed", ex.message);
          }
        }
      );
    } else {
      swalAl('error', 'Cannot delete!!!', 'No Result from DB');
    }
  }

/***
$HDR
<style>
.dvalSpn {
    float: left;
    width: 80px;
    border-radius: 5px 5px 5px;
    background-color: #aafada;
    text-align:center;
    font-size:small;
    border: 1px solid navy;
}
.dvalSpn:hover {
    background-color:#44aaaa;
    cursor:pointer;
}
</style>

 
<h4>Product Under  
<span id='wxcat'>@ucat<span>
<span id='wxclass'>@class</span></h4>

//<a href='javascript:loadCProduct()' class='btn btn-primary'>Load</a>
Property Filter:
<div id='filterDiv' class='well'>
 
<table id='filterTab'>
$SQL 
 Declare @jsn varchar(max)
 select @jsn=PROPERTY
 From TWDPIM..CATPROPERTY 
 where BYCODE ='@ucat'
  and CLASS   ='@class'

  Select NAME=isnull(NAME,'Unkown'),VAL
   from OPENJSON(@jsn)
 with (NAME varchar(50) '$.name',
 VAL varchar(5000) '$.VAL' 
 )
 where NAME>' ' 
$BDY
<tr><td>:00<td class ='filterTd' >:01
$ERX
<tr><st colspan-'2'>No Filter Defined
$END
</table>
<span id='findSp' style='font-size:x-small'></span>
</div>
<div class='row'>
    <div class='col-md-2 col-xs-5' style='padding:0px'>
        <form action="javascript:openModalCopyCategory();">
            <button class='btn btn-primary pull-left' type='submit' id='btn-copycategory' onclick="this.blur();">Copy @ucat : @class</button>
        </form>
    </div>
    <div class='col-md-8 col-xs-2'></div>
    <div class='col-md-2 col-xs-5' style='padding:0px'>
        <form action="javascript:deleteAllSKUs('@ucat','@class');">
            <button class='btn btn-danger pull-right' type='submit' id='btn-deleteall' onclick="this.blur()">Delete All SKUs</button>
        </form>
    </div>
</div>
<div id ='wprodDiv'>
Product to be displyed Here
</div>

<!-- Modal -->
<div id="md-copyclass" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="javascript:copyCategory('@ucat','@class');">
        <div class="modal-header">
            <h4 class="md-title"><strong>Copy @ucat : @class</strong></h4>
        </div>
        <div class="modal-body">
            <h4>กรุณาระบุ Dept / Subdept / Class ที่ท่านต้องการ</h4>
            <h4>เพื่อทำการ Copy ข้อมูล</h4><hr>
            <label for='newclass'>Move To <span style='color:red;'><strong>*</strong></span>:</label>
            <select id='newclass' class='form-control'>
              <option value=''>กรุณาระบุ - Dept / Subdept / Class ที่ท่านต้องการ</option>
            </select>
        </div>
        <div class="modal-footer">
            <button type='submit' class="btn btn-primary" onclick='this.blur();'>Confirm</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>

  </div>
</div>







//END  **/