USE [TWDPIM]
GO

/****** Object:  StoredProcedure [dbo].[xlsupdateusercatprod]    Script Date: 5/24/2020 9:37:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
CREATE  Procedure [dbo].[xlsupdateusercatprod]
 @ucatname char(4)
 ,@par varchar(max)
 ,@prod varchar(max)
 ,@pname varchar(8000)
as
 
if @ucatname is null or @ucatname =' '
begin
raiserror('No USERCAT NAME',18,2)
return
End
declare @updRW int,@insrw int

 --if exists(select * from sysobjects where name='XLSUSERCAT2')
--   drop table XLSUSERCAT2

 Select * into #XLSUSERCAT2
 from OPENJSON(@par)
 with (
  DPCODE  char(2)  '$.dpcd',
  DPNAME  varchar(50) '$.dpnm',
  SDCODE  char(2) '$.sdcd',
  SDNAME  varchar(50) '$.sdnm',
  CLCODE  char(2) '$.clcd',
  CLNAME  varchar(50) '$.clnm' 
  )

 ---product in this cat
 
 select JSON_VALUE(value,'$.clcd') as CLASS
,JSON_VALUE(value,'$.skcd') as SKCODE
,JSON_QUERY(value,'$.prop') PROP
into #tmpprod
from OPENJSON(@prod)
 
 
 

/*
 delete from  CATPRODUCT
 Select * from CATPRODUCT
 */
 ---property name
 --{"clcd":"CLASS","prop":["COLOR","TYPE","STYLE","SIZE","MATERAIL"]}
 /*
 Select *  
 into #tmppname
 from OPENJSON(@pname)
 with (
   CLS   char(6)  '$.clcd',
   PNAME   varchar(1000) '$.prop'   ---p1^p2^p3.....
  )  
*/
select JSON_VALUE(value,'$.clcd') as CLS
,JSON_QUERY(value,'$.prop') PNAME
 into #tmppname
from OPENJSON(@pname)

-----Class Name, only 1 class at a time
declare @class char(6)
Select  @class= CLS 
from #tmppname


 
  /**********/

  set Nocount on

  select top 0 CATCODE,CATNAME,MAPTO 
  into  #tmptab
  from CLOUDPOS..CATBYUSER

  
  Begin TRY
           
		  declare  scur cursor for 
		  Select DPCODE,DPNAME,SDCODE,SDNAME,CLCODE,CLNAME 
		  from #XLSUSERCAT2

		  Declare @dp char(2),@dpn varchar(50)
		  ,@sdc char(2),@sdn varchar(50)
		  ,@clc char(2),@cln varchar(50)
		  ,@scc char(2),@scn varchar(50)
		  ,@mpto char(8)

		  open  scur

		  FETCH NEXT FROM SCUR into @dp,@dpn,@sdc,@sdn,@clc,@cln 

		  WHILE @@FETCH_STATUS<>-1
		  BEGIN
    
			if not exists(select * from  #tmptab where  CATCODE=@dp+ '000000')
			insert #tmptab(CATCODE,CATNAME) 
			 values(@dp+'000000', @dpn )

			if not exists(select * from  #tmptab where CATCODE=@dp+@sdc+'0000')
			insert #tmptab (CATCODE,CATNAME) 
			   values(@dp+@sdc+'0000', @sdn )

    
			if not exists(select * from #tmptab where CATCODE=@dp+@sdc+@clc+'00')
			 insert #tmptab (CATCODE,CATNAME,MAPTO) 
			   values(@dp+@sdc+@clc+'00', @cln,'00000000' )
 
			 FETCH NEXT FROM SCUR into @dp,@dpn,@sdc,@sdn,@clc,@cln 
		  END
		  close scur
		  Deallocate scur

		--  delete from CATBYUSER where BYCODE =@ucatname 
		   
		    Insert CATBYUSER (BYCODE,CATCODE,CATNAME,MAPTO )  
		    SElect @ucatname,CATCODE,CATNAME,MAPTO 
		    From #tmptab
			WHere CATCODE not in(Select CATCODE 
						from CATBYUSER where BYCODE=@ucatname)

			Update CATBYUSER  Set  CATNAME=b.CATNAME ,MAPTO=b.MAPTO  
		    From   CATBYUSER a inner join #tmptab b on a.CATCODE=b.CATCODE
			WHere  BYCODE=@ucatname 

	 

			update CATPRODUCT set PROP=b.PROP
			      from CATPRODUCT a inner join #tmpprod b on a.SKCODE =b.SKCODE and a.CLASS=b.CLASS
			Where BYCODE=@ucatname  
			select @updrw =@@ROWCOUNT

			insert CATPRODUCT(BYCODE,CLASS,SKCODE,PROP )
			Select @ucatname ,*
			from  #tmpprod
			Where SKCODE not in(Select  SKCODE from CATPRODUCT Where BYCODE=@ucatname and CLASS=@class)

			select @insrw  =@@ROWCOUNT

			 ---^TYPE^STYLE^SIZE^MATERAIL^--------get property into table 
			  
/**Need PRocess [{"name":"PROP1","val":"COLOR"}
            ,{"name":"PROP2","val":"TYPE"},{"name":"PROP3","val":"STYLE"},{"name":"PROP4","val":"SIZE"},{"name":"PROP5","val":"MATERAIL"},{"name":"PROP6","val":""}]
			insert  CATPROPERTY(BYCODe,CLASS,PROPERTY )
			SELECT  @ucatname,CLS,PNAME
			from   #tmppname
 */	 
			-----Process Catproperty, Get Distint Property from CATPRODUCT
			declare @propname varchar(8000)

		  select  @propname =PNAME  from   #tmppname

		 declare pcur cursor for 
			 select  JSON_VALUE(value,'$.val') 
				from OPENJSON(@propname)
				Where JSON_VALUE(value,'$.val')>' '
			 
			 declare @cl varchar(10)
			 declare @propval varchar(8000) 
			 set  @propval=''
			 declare @pval table (pval varchar(50))
			 declare @pv varchar(1000)

			 open pcur

		    fetch next from pcur into @cl
			While @@FETCH_STATUS!=-1
			Begin
				 delete from @pval
				 set @pv=''
				 -----GEt Distinct value of property fron CATPRODUCT
				 insert @pval(pval)
				 Select  
				 distinct( JSON_VALUE(PROP,'$.'+@cl) )
				 from CATPRODUCT
				 where BYCODE='TRPS' 
				
				 select @pv =  @pv + pval+'|' 
				 from @pval
				 --remove last |
				 select @propval=@propval+'{"name":"'+@cl +'","VAL":"' +substring(@pv,1,len(@pv)-1)+ '"},'
			     fetch next from pcur into @cl
			 END
			 close pcur
			 deallocate pcur
			-- select @propval

			 if Exists(select * from CATPROPERTY where BYCODE=@ucatname and CLASS=@class)
				delete from CATPROPERTY where BYCODE=@ucatname and CLASS=@class
			insert CATPROPERTY(BYCODE,CLASS,PROPERTY)
			values(@ucatname,@class,'['+substring(@propval,1,len(@propval)-1)+']')

	       -- select * from CATPROPERTY
	        Select  MSG='Insert CATPRODUCT '+ convert(varchar(6),@insrw ) +' Row(s), Update '+convert(varchar(6),@updrw) + ' Rows(s)'

   END Try
   Begin Catch
		declare @msg varchar(600)
		Select @msg=ERROR_MESSAGE()
		raiserror(@msg,18,2)
   End catch

   -- alter TABLE CATBYUSER add MAPTO char(8)

/*
drop table CATPROPERTY

Create Table CATPROPERTY (
    BYCODE char(4)
    ,CLASS char(8)
    ,PROPERTY varchar(5000)
  
   )
 create unique clustered index catpidx on CATPROPERTY(BYCODE,CLASS)

 select * from XLSUSERCAT2
 update CATBYUSER set BYCODe='CONT' where BYCODE='CONS'

 select *   from CATBYUSER

 declare @jsn varchar(5000)
set @jsn= (Select * from CATBYUNAME for JSON AUTO)
select @jsn

 update CATBYUNAME set BYNAME='ช่างก่อสร้าง' where RECID=2
 -- delete from CATBYUSER where RECID>1026

  Select * from  CATPROPERTY where bycode='TRPS'
   
   select * into CATBYUSER from CLOUDPOS..CATBYUSER
Select   PROP1 
from CATPRODUCT 
group by PROP1


 

-- delete from  CATPROPERTY

 Select * From CATPROPERTY where BYCODE='TRPS'
 select * from CATBYUSER where BYCODE='TRPS'
  -- delete from  CATPRODUCT
 Select * from CATPRODUCT where BYCODE='TRPS' 
Select SK_CODE,PR_NAME,PR_MODEL,PR_BRAND, PR_ORGPRICE,ATBCODE
from EPROD a inner join CATPRODUCT b on a.sk_code=b.skcode

Select SK_CODE,PR_NAME,PR_MODEL,PR_BRAND, PR_ORGPRICE,ATBCODE
from EPROD a inner join CATPRODUCT b on a.sk_code=b.skcode
 where BYCODE='CONT' and Class='010101'

 Select * from tprop 

 create table CATPRODUCT (BYCODE char(4),CLASS char(6),SKCODE int,PROP varchar(5000))
 go
 Create Unique clustered index CATPRODIDX on CATPRODUCT(BYCODE,CLASS,SKCODE)
 */



GO

