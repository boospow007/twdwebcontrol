
var refStatus='';
function pageReady(){
	if (profileList.indexOf('0046') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Pickup Control');
		location.assign("/");
    }

	$('#sdate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
		todayHighlight: true
		//,endDate: '0d'
	});

	$('#edate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
		todayHighlight: true
		//,endDate: '0d'
	});
	
    var tb = $('#dataTab').DataTable({    
		"bPaginate": false,
		"bSort": true,
		"bFilter": false
		,"bInfo": false
		});
	//Sort by columns  and redraw            
	tb             
		.order([1, 'desc'])  //  date
		.draw();


	$('#dataTab tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			tb.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	} );

	var stdf=readCookie("WDFSTORE");
	$('#orderstore').val(stdf);
	
	var ref=$('#dataTab tr:eq(1) td:eq(0)').text().trim();
	var st =$('#dataTab tr:eq(1) td:eq(4)').text().trim();
	var stc =$('#dataTab tr:eq(1) td:eq(9)').text().trim();
	//readCookie("WDFSTORE");
	var stt =st.substring(0,1);

	//console.log(ref +','+stt)

	if (ref=='No data available in table') {         
        return;
	}
	
	//console.log('ref='+ref +',stt='+stt+',stc='+stc);
	itemsDetail(ref,stt,stc);

	
 

}

function itemsDetail(ref,st,stc){
	try { 	
	custref =ref;
	

	if(st >'A'){
		//--show pickup slip
		pickupSlip(ref,stc,st);
		return
	}
	var pdata ={patname:"/CSHOME/saleitem.txt",
			para: {ref:ref,stc:stc}};
 
	var sUrl  ="../XPAND/FILE";  // Call expand route
	
	$('#detailDiv').html(waitimg);
	$.ajax({
		type: "POST",
		url: sUrl,
		data: JSON.stringify(pdata),
		dataType: "text",
		contentType: "application/json; charset=UTF-8",
		success: function (ret) { //return is html string
			try {
				if(ret.substring(0,3)=='ERR'){
					$('#detailDiv').html(syserror +ret);
				}else { 
					$('#detailDiv').html(ret);
				    setTimeout(afterLoad,100) ; //wait abit ??
				  }
			}
			catch(ex){alert( 'Err. '+ex.message);
			$('#detailDiv').html('');
			}
			
			 return;
          },
		error: function (e) { 
		  alert('xpandfile Err.. '+e.responseText);
		}
	});
	}
	catch(ex){alert(ex.message);}
}
//-------------After Load Items
function afterLoad(){
	try   { 
	  //---convert numeric format
	  $('.numeric').each(function(){
			 var val=$(this).text();
			 $(this).css('align','right');
			 $(this).html(numericFormat(val))
		  });	
	  
	//---put image to each .pimage
	  $('.pimage').each(function(){
			 var skc=$(this).attr('id');
	         getImage(skc);
		  });	
	  
	   /*
	  //-----move edit image to top right
	  $('.editImg').each(function(){
		  var pos=$(this).parent().position();
		  pos.left+=$(this).parent().width()-60;
		  $(this).css('top',pos.top);
		  $(this).css('left',pos.left);
	   });
	  
	
	  
	 //---------move CustDiv
	  var cpos=  $('#detailDiv').position();
	  $('#custDiv').css('top',cpos.top);
	  $('#custDiv').css('left',cpos.left+500);
 
	  //-----Hide input if Sattus !=' '
	  refStatus=$('#refStatus').text().trim();
	  if(refStatus>'A'){
		    $('#addNewDiv').hide();
		    $('#btnClose').hide();
	  }
	  else
	   $('#txtBcd').focus();
	
	  // ------High light PLPP bonusTab
	    $('#bonusTab tr:gt(0)').each(function(){
		   var sq=$(this).find('td:eq(0)').text();
		   //alert(sq);
		   if(sq==99)$(this).css('background-color', '#E0A0A0');
	       });
	  */  
	}
	catch(ex){
      alert(ex.message);
		}
   }

 function getImage(skc){
			var url="/B/"+ skc   ;  // this function read image from DB
			//alert(url);
			$.get(url,function(data){
				$('#'+skc).prop('src', data  ) ;
			});
	 }

//---show pickup slip and Print it on the pricnte
function pickupSlip(ref,stc,odrstt){
	try { 
	 
	var pdata ={patname:"/CSHOME/pickupslip.txt",
			para: {ref:ref,stc:stc}};
 
	var sUrl  ="../XPAND/FILE";  // Call expand route
	
		$('#detailDiv').html(waitimg);
	
	$.ajax({
		type: "POST",
		url: sUrl,
		data: JSON.stringify(pdata),
		dataType: "text",
		contentType: "application/json; charset=UTF-8",
		success: function (ret) { //return is html string
		//alert(ret);
			try {
				if(ret.substring(0,3)=='ERR'){
					$('#detailDiv').html(syserror +ret);										
					$('#btnPrintSlip').hide();

				}else { 
					$('#detailDiv').html(ret);		
					
					//----creat Barcode image
					createBarcode();

					//if (odrstt=='Q') $('#btnPrintSlip').hide();
					//else $('#btnPrintSlip').show();
				  }
			}
			catch(ex){
				alert( 'Err. '+ex.message);
				$('#detailDiv').html('');
			}
				
			 return;
          },
		error: function (e) { 
		 alert('xpandfile Err.. '+e.responseText);
		}
	});
	}
	catch(ex){alert(ex.message);}
}


function createBarcode(){
	try { 
		$('.barCode').each(function(){
			var  prcd	=$(this).attr('name');
			var  id	=$(this).attr('id');
								  
			   JsBarcode('#'+id , prcd,
					   {
					 height:40,
					 displayValue: false
					   });
		});
		   
		}
	   catch(ex){alert(ex.message);}
   }

function showByCondition(){
	try {
		stcode=$('#orderstore').val();
		userId = readCookie('WUSERID');

		var ssdate = $('#sdate').val();
		var sedate= $('#edate').val();
		var time1 = $('#timetxt1').val();
		var time2 = $('#timetxt2').val();
		var tsearch = $('#smartsrch').val();
		var tref = $('#smartref').val();

		ssdate=ssdate.trim();
		sedate=sedate.trim();

		if ((ssdate==''&& sedate=='') || (ssdate==sedate) ){
			var t1 = parseInt(time1);
			var t2 = parseInt(time2);
			if (t1>t2){
				alert('เวลาสิ้นสุดต้องมากกว่าเวลาเริ่มต้น')
				return;
			}

		}
		else {
			var s1 = parseInt(ssdate);
			var s2 = parseInt(sedate);
			if (s1>s2){
				alert('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด')
				return;
			}

		}
		
	 
		$('#mainDiv').html(waitgif);
		//--------Create Pattern inside
		var   pat="./public/CSHOME/waitinglist.js";
		pat="CSHOME/waitinglist.js";

	 	var pdata ={patname:pat,
					para:{ 
						 store:stcode,
						 cashier:userId,
						 lockno:''
						 ,srhdate:ssdate		
						 ,srhenddate:sedate				 
						 ,srhtime1:time1	
						 ,srhtime2:time2	
						 ,textsrch:tsearch
						 ,textref:tref
						 ,userid:userId
					}
				 };
		ajaxXpandFile(pdata,function(bool,ret){
			if(ret.substring(0,3)=='ERR')ret=syserror+ret;
			
			$('#mainDiv').html(ret); 
			setNumericFormatDiv('mainDiv');    //set Numeric format on display --source in common.js
			//-----Call Startpage
			try {
				pageReady();
			}
			catch(ex){
				console.log(ex.message);
			}
			
		}); 
	}
   catch(Ex){alert(ex.message);}
}


/***  
$HDR
<style>
 div , table {  	 
        font-size: 12px ;	 
    }
.table tbody tr:hover {
       background-color:silver;
       cursor:pointer ;
    }

 </style>
<div>
 <span id='sdt' style='display:none;'> @srhdate</span>
 <span id='edt' style='display:none;'> @srhenddate</span> 
 <span id='stme1' style='display:none;'> @srhtime1</span>
 <span id='stme2' style='display:none;'> @srhtime2</span>
 <span id='stext' style='display:none;'> @textsrch</span>
 <span id='reftext' style='display:none;'> @textref</span>
</div>
 
<h4>รายการพิมพ์ใบกำกับสินค้า (อัพเดทล่าสุด <label id='lastupd'>#tday</label>) </h4>
<div class='row'>
<div class='col col-sm-8'>
 
<div style='background-color:yellowxxx;'>
	<div class="row">
		<div class='col-md-6' >
				สาขา <br>                  
				<select class='form-control' id='orderstore' >  
					<option value='%'>ทุกสาขา</option>                                                  
					#sbranch
				</select>
		</div>
	</div>
	<div class='row'>
		<div class='col-md-6' >
			ค้นหาลูกค้า หรือ เบอร์โทรศัพท์ 
			<input type='text'  id='smartsrch'  
				class='form-control' 				
				autocomplete="off"				
				placeholder='ชื่อลูกค้า หรือ เบอร์โทรศัพท์'
				maxlength='100' value ='' >
		</div>
		<div class='col-md-4' >
			เลขที่คำสั่งซื้อ หรือ เลขที่ Ticket
			<input type='text'  id='smartref'  
				class='form-control' 				
				autocomplete="off"				
				placeholder='เลขที่คำสั่งซื้อ หรือ เลขที่ Ticket'
				maxlength='50' value ='' >
		</div>
		<div class='col-md-2' >
				<br>
				<a href="javascript:showByCondition()" 
				class="btn btn-success btn-sm">แสดง</a>
			</div>
	</div>
	<div class='row'>
			<div class='col-md-3' >
				วันที่ Ticket เริ่มต้น <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="sdate" name ="sdate"  
				onFocus='this.select();' 				 
				 autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
			<div class='col-md-2' >
				เวลาเริ่มต้น <br>                  
				<select class='form-control' id='timetxt1' >                    
					#listtime1
				</select>
			</div>			  			  
			<div class='col-md-3' >
				วันที่ Ticket สิ้นสุด <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="edate" name ="edate"  
				onFocus='this.select();' 				 
				autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
			 
			<div class='col-md-2' >
				เวลาสิ้นสุด <br>                  
				<select class='form-control' id='timetxt2' >                    
					#listtime2
				</select>
			</div>  
				 
	</div>
 
</div>

 <div class='row'>
			<div class='col-md-12' >
                <br>
               <span style='background-color:#F8F8F8;width:100%;display:block;padding:3px 3px;'> 
                
               </span>
            </div>
           
    </div>
 
<h5>
Parameter: <label id='theday'>
ลูกค้าหรือเบอร์โทรศัพท์:#smartsrch / คำสั่งซื้อหรือเลขที่ Ticket:#smartref / วันที่ Ticket : #theday #timetxt1.00 -  #day2 #timetxt2.59</label>
</h5>

//#status

  <table id ='dataTab' class='table' style='font-size:13px;'>
  <thead>
   <th>คำสั่งซื้อ   
   <th>วันที่จัดคิว
   <th>ลูกค้า
   <th>จำนวนรายการ
   <th>สถานภาพ
   <th>เลขที่ Ticket
   <th>วันที่ Ticket
   <th>DIY
   <th>CON
   <th>Store
  </thead>
<tbody>

$SQL
 
exec CLOUDPOS..cs_ListPickupList '#startdate','#enddate','#timetxt1','#timetxt2','#smartsrch','@store','#smartref','@userid'


$BDY
<tr onclick ="javascript:itemsDetail('^REF^','^STATUS^','^STC^')">
<td>^REF^
<td>^RDD^ ^RDT^
<td>^UNAME^ 
<td style='text-align:right;'>^NITEM^ 
<td>^STATUS^ ^STNAME^ 
<td>^POSREF^
<td>^POSTTM^
<td style='text-align:right;'>^BHC^
<td style='text-align:right;'>^CON^
<td>^STC^
</tr>
$END
</tbody>
</table>


<br><br><br>
</div>
<div class='col col-sm-4'>
	<div id='detailDiv'>
	
	</div>
</div>

$TAB
|XID #status
|HDR
Remark:
|SQL
SELECT * from CLOUDPOS..SCSTATNAME
|BDY
[:00 :01] 
|END



$TAB 
|XID #listtime1
|HDR
|SQL 
exec CLOUDPOS..cs_loadTimePick  
|BDY
<option value=':00'>:00</option>
|END


$TAB 
|XID #listtime2
|HDR
|SQL 
   exec CLOUDPOS..cs_loadTimePick  
|BDY
<option value=':00'>:00</option>
|END


$TAB 
|XID #sbranch
|HDR
|SQL 
   exec CLOUDPOS..cs_loadStoreByUser '@userid', '@store'    
|BDY
<option value=':00'>:00 :01</option>
|END



//////////////////////////////////////////
$PRE
exec CLOUDPOS..cs_ListPickupList_Header '@srhdate','@srhenddate','@srhtime1','@srhtime2','@textsrch','@store','@textref','@userid'

				   
//END ******/