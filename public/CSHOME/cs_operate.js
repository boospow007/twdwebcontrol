/**
 * to be load with welccome.html
 */
var cashierId = "";
var userId = "";
var userGroup = 0;
var oprDate = "";
var storeCode = "";
var lockNumber = "001";
var cashierName = "";
var localSrv = "";
var cloudSrv = "";
var waitgif = "<img src='../images/wait.gif'>";
var syserror = " <img src ='../images/error.jpg' width='200px'><br> ";
var needRefresh = false;

function userHasLogin() {

	cashierId = readCookie('WUSERID');
	//  lockNumber = readCookie('LOCKNO');
	userGroup = readCookie('WUGROUP');
	// if(lockNumber==null)lockNumber='001';

	$('#user_login').text(cashierId);

	var htm = " <table id='sysConfig'> " +
		"<tr><td>User ID:<td>" + cashierId +
		"<tr><td>User Grp:<td>" + userGroup +
		"</table>";
	$('#whoLogin').html(htm);
	currentTime();
	setTimeout(closeWhoLogin, 5000);

}

function closeWhoLogin() {
	$('#whoLogin').hide();
}

function showProfile() {
	$('#whoLogin').show();
}

//-----Current Time
function currentTime() {
	try {
		var nd = new Date;
		var h = addZero(nd.getHours());
		var m = addZero(nd.getMinutes());
		var s = addZero(nd.getSeconds());

		var sec = nd.getSeconds()
		$('#localTime').text(h + ':' + m + ':' + s);


		// if((sec%10)==0) chekCloudSrv();  //1 minute interval
		setTimeout(currentTime, 1000);
	} catch (ex) {
		alert(ex.message);
	}
}

function addZero(x) {
	var xstr = 100 + x * 1;
	xstr += '';
	return (xstr.substring(1));
}


//------New VErsion 
function openDetail(pat) {
	var url = './DPAGE?' + btoa(pat);
	//window.open(url,'possale' );
	window.open(url, 'DETAIL', 'toolbar=no, menubar=no,location=no, resizable=no');
}

var currentPat = "";

function openMenu(pat, lv) {
	try {

		storeCode = readCookie("WDFSTORE")
		storeName = readCookie("WDFSTNAME")
		userId = readCookie('WUSERID');
		userGroup = readCookie('WUGROUP');
		if (userId == null || userId == '' || userId == 'undefined' || userGroup == null) {
			location.assign("/");
			return;
		}


		if (lv * 100 > userGroup) {
			upfAlert('You dont have authority to use this menu', function () {
				return;
			});
			return;
		}
		console.log('Open Pattern' + pat);
		needRefresh = false;
		//----------wait ...
		$('#mainDiv').html(waitgif);
		//--------Create Pattern inside
		// pat="./public/"+pat;
		var pdata = {
			patname: pat,
			para: {
				store: storeCode,
				cashier: cashierId,
				lockno: lockNumber,
				srhdate: '',
				srhtime1: '',
				srhtime2: '',
				textsrch: ''
			}
		};
		ajaxXpandFile(pdata, function (bool, ret) {
			if (ret.substring(0, 3) == 'ERR') ret = syserror + ret;

			$('#mainDiv').html(ret);
			setNumericFormatDiv('mainDiv'); //set Numeric format on display --source in common.js
			//-----Call Startpage
			try {
				pageReady();
			} catch (ex) {
				console.log(ex.message);
			}
			$('#whoLogin').hide();
			currentPat = pat;
		});
	} catch (Ex) {
		alert(ex.message);
	}
}

function pageReload() {
	openMenu(currentPat);
}

function welcome() {
	location.reload();
}

function keyCDPress(ev) {
	if (ev.keyCode == 13) {
		query2login();

	}
}
//---------------------
function logOut() {
	var mdal = $("#myModal");
	$('#mdHeader').html("LOGOUT");
	$("#mdBody").text("Confirm Logout");
	$("#myBtnOK").show();
	$("#myModal").modal();
	$("#myBtnOK").unbind();
	$("#myBtnOK").click(function () {
		query2Logout();
	});

	$("#myBtnCC").unbind();
	$("#myBtnCC").click(function () {
		$("#myModal").modal("hide");
	});
}

function query2Logout() {
	if (storeCode === 'undefined') storeCode = '920';
	var sUrl = "./POSSRV/LOGOUT/" + cashierId;

	//alert(sUrl);
	$.get(sUrl, function (ret) {
		//console.log(ret);
		alert(ret);
	});
	//--------Clear cookie
	removeCookie('WUSERID');
	removeCookie('WUNAME');
	removeCookie('WUGROUP');
	location.reload();
}
//------------------------
function upfAlert(text, callBack) {
	$("#mdBody").text(text);
	$('#mdHeader').html("ALert");
	$("#myBtnOK").text("CLOSE");
	$("#myBtnCC").hide();
	$("#myModal").modal('show');
	$("#myBtnOK").click(function () {
		$("#myModal").modal("hide");
		callBack(true);
	});
	setTimeout(function () {
		$("#myModal").modal("hide");
		callBack(true);
	}, 4000);
}

//-------------------------------
function upfConfirm(text, callBack) {
	$("#mdBody").text(text);
	$("#myBtnOK").text("YES");
	$("#myBtnOK").show();
	$("#myBtnCC").show();
	$("#myModal").modal('show');
	$("#myBtnCC").unbind();
	$("#myBtnCC").click(function () {
		$("#myModal").modal("hide");
		callBack(false);
	});
	$("#myBtnOK").unbind(); // ---importance, unbind the old event
	$("#myBtnOK").click(function () {
		$("#myModal").modal("hide");
		callBack(true);
	});
}
//--------------------reset password ----
function resetPassword() {
	var uid = $('#xuserid').val();
	if (uid == '') {
		alert('You must Enter User Id first');
		return;
	}
	//-------------chek email --
	var qry = "Select EMAIL from USERTAB where USERID='" + uid + "'";
	console.log(qry)
	ajaxGetQuery(qry, function (isok, ret) {
		console.log(ret);
		if (isok) {
			var jsn = JSON.parse(ret);
			if (jsn.dbrows <= 0) {
				alert('User ' + uid + ' Is not in System')
				return
			}
			var eml = jsn.dbitems[0].EMAIL;
			var usr64 = btoa(uid);
			if (jsn.dbcode > 0) {
				alert(jsn.dbmessage);
				return;
			}
			if (confirm('Reset Password to this Email [' + eml + ']') == 0) return;

			//-----Send mail to receiper
			// eml='paisan@upfront.co.th';
			var d = new Date()
			var para = 'USERID=' + uid + '&EMAIL=' + eml + '&TIME=' + d.toLocaleString;
			var pdata = {
				sendto: eml,
				subject: 'Request to change password to TWD E-ordering',
				mailbody: 'Click to This link https://upfdev.com:433/PASSW/' + btoa(para)
			}
			var sUrl = "../EMAIL/SEND";
			try {
				$.ajax({
					type: "POST",
					url: sUrl,
					data: JSON.stringify(pdata),
					dataType: "text",
					contentType: "application/json; charset=UTF-8",
					success: function (ret) { //return is array of data in json 
						alert(ret);
						return;
					},
					error: function (e) {
						alert(e.responseText);
					}
				});
			} catch (ex) {
				callBack(false, ex.message);
			}
		} else alert(qry + '  >>' + ret);
	});
}




function printDept() {
	// orange 	
	//$(".PrintArea").printArea();	
	//$('.PrintArea').each(function(){			
	//$(this).printArea();
	//});		

	try {
		$('.printsubDept').each(function () {

			//alert($(this));
			var id = $(this).attr('id');
			//alert(id);
			//$('#'+id).printArea();   not working!!!!			
			printDiv(id)
		});
	} catch (ex) {
		alert(ex.message);
	}
}

function printDiv(divID) {
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML =
		"<html><head><title></title></head><body>" +
		divElements + "</body>";

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;
}

function printSlip() {
	// blue : control code cutter
	try {
		var dataP = '';
		var data2 = '';
		$('.printsubDept').each(function () {

			var id = $(this).attr('id');
			data2 = $(this).html();

			//dataP=dataP+data2+'<label style="color:white;">'+String.fromCharCode(27) + 'i'+'</label>';
			//dataP=dataP+data2+'<div class="page-break"></div> ';

			dataP = dataP + data2 + '<label style="color:white;">' + String.fromCharCode(29) + 'V0' + '</label>';

			//setTimeout(printOutSlip('#'+id,id),3000);
		});
		console.log(dataP)

		var mywindow2 = window.open('', name, 'height=400,width=600');
		mywindow2.document.write('<html><head><title></title>');
		mywindow2.document.write('<link rel="stylesheet" href="/stylesheets/print.css" type="text/css"  media="print" charset="utf-8" />');
		mywindow2.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
		mywindow2.document.write('  .hideme {display:none;}');
		mywindow2.document.write(' </style></head><body>');
		mywindow2.document.write(dataP);
		mywindow2.document.write('</body></html>');

		setTimeout(function () {
			//mywindow2.print();
			//mywindow2.close();
		}, 1000)

	} catch (ex) {
		alert(ex.message);
	}

}

async function printOutSlip(obj, name) {
	console.log(name)
	var data2 = $(obj).html();
	console.log(data2)
	var mywindow2 = window.open('', name, 'height=400,width=600');
	mywindow2.document.write('<html><head><title></title>');
	mywindow2.document.write('<link rel="stylesheet" href="/stylesheets/print.css" type="text/css"  media="print" charset="utf-8" />');
	mywindow2.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
	mywindow2.document.write('  .hideme {display:none;}');
	mywindow2.document.write(' </style></head><body>');
	mywindow2.document.write(data2);
	mywindow2.document.write('<label style="color:white;">');
	mywindow2.document.write(String.fromCharCode(27) + " i"); //คำสั่งตัดกระดาษ
	mywindow2.document.write('</label>');
	mywindow2.document.write('</body></html>');

	setTimeout(function () {
		mywindow2.print();
		mywindow2.close();
	}, 3000)

}


function printWH(r, s) {
	// green : page-break
	// More Setting ที่ printer .. Margins ต้องเป็น None
	var datap = $('#printDiv').html();
	console.log(datap)


	var mywindow = window.open('', 'mydiv2', 'height=400,width=600');
	mywindow.document.write('<html><head><title></title>');
	mywindow.document.write('<link rel="stylesheet" href="/stylesheets/print.css" type="text/css"  media="print" charset="utf-8" />');
	mywindow.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
	mywindow.document.write('  @media all {  .page-break { display: none; }} @media print { .page-break { display: block; page-break-before: always;overflow: hidden!important; }} ');
	mywindow.document.write('  .hideme {display:none;}');
	mywindow.document.write(' </style></head><body>');

	mywindow.document.write(datap);

	mywindow.document.write('</body></html>');

	setTimeout(function () {
		mywindow.print();
		mywindow.close();
	}, 1000)

	confirmReady2Picking(r, s);
}

function confirmReady2Picking(rf, st) {

	try {
		var qry = "exec CLOUDPOS..cs_Ready2Picking '@ref' , '@stc' ";
		var pdata = {
			query: qry,
			para: {
				ref: rf,
				stc: st
			}
		}
		ajaxExec(pdata, function (isok, ret) {
			//alert(ret);
			var jsn = JSON.parse(ret);
			if (jsn.dbcode == 0) {
				//alert('พิมพ์คำสั่งซื้อของลูกค้าเรียบร้อยแล้ว');
				showByCondition();
			} else
				alert(jsn.dbmessage);

		});
	} catch (ex) {
		alert(ex.message);
	}
}