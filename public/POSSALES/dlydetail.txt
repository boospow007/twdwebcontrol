
/*** 
$HDR
<h3>DLYPTRANS  STCODE @stc REF @ref </h3>

<table id ='dataTab'  style='width:90%'>
<thead>
  <th>REF<th>PCD<TH>SKC<th>STY<th>VTY<th>TTM<th>QNT<th>AMT<th>FDS
 </thead>
$SQL
select * from
 CLOUDPOS..DLYPTRANS with(NOLOCK)
Where STCODE='@stc' and  REF='@ref'
$BDY
<tr>
<td>^REF^</td>
<td>^PCD^</td>
<td>^SKCODE^
<td>^STY^</td>
<td>^VTY^</td>
<td>^TTM^
<td>^QNT^</td>
<td>^AMT^</td>
<td>^FDS^</td>

$ERX
 <tr><td colspan='5'>@error
$END
</table>
<hr>
<h3>DLYPTRANS ON CLOUD  </h3>
#cldpos 

$TAB 
|XID #cldpos
|HDR 
<table class ='dataTab'  style='width:90%'>
<thead>
  <th>REF<th>PCD<th>STY<th>VTY<th><th>TTM<th>QNT<th>AMT<th>FDS
 </thead>
|SQL 
use.PLPPSRV^
select * from
POSXFER..DLYPTRANS with(NOLOCK)
Where STCODE='@stc' and  REF='@ref'
|BDY
<tr>
<td>^REF^</td>
<td>^PCD^</td>
//<td>^SKCODE^
<td>^STY^</td>
<td>^VTY^</td>
<td>^TTM^
<td>^QNT^</td>
<td>^AMT^</td>
<td>^FDS^</td>
|ERX
<tr><td colspan='5'>@error
|END

//END **/
 