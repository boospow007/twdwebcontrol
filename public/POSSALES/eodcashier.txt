 
$HDR
 <table id ='sumary' class="table table-striped"  >
 <thead><th>Tendor<th>Name <th> Quant<th>Amount</thead>
$SQL
	declare @ref varchar(10) 
	select @ref=max(REF) 
	from DLYPTRANS 
	where 
	STCODE='@stcode' 
	and USR='@userid'

  Select PCD,NM=rtrim(PAY_DESC) ,QNT,AMT 
	From DLYPTRANS a join PAYMENT b on a.PCD=b.PAY_NAME 
	where REF=@ref 
	and  datediff(dy,TTM,getdate()) =0
	and STY='C' 
$BDY
	<tr><td>:00
	<td>:01
	<td align='right'>:02
	<td align='right'>:03</tr>
$ERX
	<tr><td colspan='4'>@error</tr>
$END
    <tr class='success' id ='totalLine'>
	<td colspan='2'>TOTAL <td align='right'>0.00<td align='right'>0.00</tr>
   </table>