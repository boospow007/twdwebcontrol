﻿function pageReady() {
  //alert('Ready');
  if (profileList.indexOf('0025') > -1) {} 
  else {
         alert('คุณไม่มีสิทธิ์ใช้งานเมนู DB Daily Task');
         location.assign("/");
  }
} 

function showNewTask(op) {
 //  alert('Task op='+op);
  openMenu('SYSTEM/dbtask.js&type='+op);
}
 
function showStep(obj) {

   try {
		var jobid	=$(obj).find('td:eq(0)').text();
		var  sdt	=$(obj).find('td:eq(2)').text();
		var  row	=$(obj).attr("id");
        var  sd		=sdt.split(' ')[0];
        var  stname =$(obj).find('td:eq(1)').text();
		var pos = $(obj).position();
		var mpos = $('#sChart').position();

 
		var ctlf 	='SYSTEM/taskdetail.txt';
	 
		var pdata	= {
					   patname:ctlf,
					   para:{'jobid':jobid , 'rundt':sd}
		}

		ajaxXpandFile(pdata,
		function (isok,ret) {

		  
			$('#detailDiv').css('top', pos.top +mpos.top-20);
			$('#detailDiv').html('<h3>'+stname+'</h3>'+ret);
			$('#detailDiv').show();
		  }
		);
	 
	}
	catch (ex) {alert(ex.message);}
	
}
function editIgnore(){
	$('#detailDiv').hide();
}
   
 
 
/******
$HDR
<style>
.wrapTd{
white-space: nowrap;
text-overflow:ellipsis;
overflow: hidden;
max-width:200px;
text-align:left;
}
.rndDiv {
		border:2px solid #a1a1a1;
		background: #A0D0D0;
		padding: 10px 40px;
		color:black;
		text-align:left;
		border-radius: 12px;
     }
#dataTab tr:hover {
	background-color:silver;
	cursor:pointer;
}
</style>

<br>

<div class='row'>

 <a href="javascript:showNewTask(4);"  class='btn smn btn-primary pull-right'> Daily Job </a>
 <a href="javascript:showNewTask(8);"  class='btn smn btn-info pull-right' > Weekly Job </a>
 <a href="javascript:showNewTask(32);" class='btn smn btn-info pull-right' > Monthly Job  </a>


<div id='histDiv' class='col-md-12'>
<h3>Running Task at [#server]</h3>
#running
</div>
<br><br>

<div id='sChart' class='col-md-12'>
    <h3>Daily Job  </h3>


   <table id='dataTab'  class='table'>
     <thead >
         <th>Job Name</th><th>LastRun</th><th>เริ่มเวลา</th><th>ใช้เวลา(hh.mm.ss)</th>
         <th>Last Message</th>
   </thead>
   
 $SQL
//---multi statement cuase ERROR in PHP !!!!!!!!
use msdb 
 Select job_id,
Instance=max(instance_id)
into #instan
from sysjobhistory group by job_id

Select JD=convert(varchar(150), a.job_id) , a.name,
rd=isnull(convert(char(8), max(run_date)) ,' '),
Rt=isnull(substring(convert(varchar(8),
max(run_time)/100+10000 ),2,2)+'.'+ substring(convert(varchar(8),
max(run_time)/100+10000 ),4,2),' '),
dr=convert(char(7),isnull( max(run_duration ),0)+1000000),
st=case when exists(select * from sysjobhistory b where b.job_id=a.job_id and run_status=0 and instance_id in (Select instance from #instan))
       then 'Fail' else 'Success' end
into #tmp
from sysjobs a left join sysjobschedules d on a.job_id=d.job_id
    left join sysschedules c on c.schedule_id=d.schedule_id
    left join sysjobhistory b on b.job_id=a.job_id
    and instance_id in (Select instance
    from #instan)
    where a.enabled=1 and c.freq_type=4
    group by a.job_id , a.name
    order by max(run_date) desc ,max(run_time)

    Select Jd , Name, RD,RT,
    MSG=substring(dr,2,2)+'.'+substring(dr,4,2)+'.'+ substring(dr,6,2)
    , St    
, ROW = row_Number() over( order by jd)
    , Stt= case when st='Success' then '' else 'Red' end
    from #tmp
    order by rd desc,rt
 $BDY 
 <tr align ='left'  id ='row:06' onclick ="javascript:showStep(this)"  >
 <td class='wrapTd'>^Jd^</td>
 <td>^Name^
 <td align ='right'>^RD^ ^RT^
<td>^MSG^
<td>^St^
</tr>
 $END 
 </table>
To see history click at Job Name
<hr>
 
 
</div>
</div>
<div id='detailDiv' class='rndDiv' style='display:none;z-index:6;position:absolute;top:80px;' onmouseleave="editIgnore()">

$TAB
|XID #running
|HDR
<table class='table'>
<thead>
<th>JOB_NAME <th> START_TIME<th> Duration </thead>
|SQL
  SELECT s.name AS [JOB_NAME],
  sja.run_requested_date AS START_TIME, 
 convert(varchar, (getdate() - sja.run_requested_date), 8) AS Duration 
  FROM msdb..sysjobactivity sja, msdb..sysjobs s
 WHERE sja.job_id = s.job_id
   AND sja.run_requested_date >  getdate() - 1
   AND sja.stop_execution_date IS NULL
|BDY
<tr><td>:00<td>:01<td>:02
|END
</table>

 $PRE 
 select [@freq]=case when left('@type',1)='@' then '4' else '@type' end, 
 [@fname]=case when left('@type',1)='@' or '@type'='4' then 'Daily' 
 else case when '@type'='1' then 'One Time' 
 else case when '@type'='8' then 'Weekly' 
 else case when '@type'='32' then 'Monthly'
  else ' Unknown Frequency ' 
  end end end end  
,[#server]=@@SERVERNAME

//END ******/