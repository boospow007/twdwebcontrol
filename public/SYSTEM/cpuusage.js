
//<script src="HighChart/highcharts.js" type="text/javascript"></script>
//<script src="HighChart/modules/exporting.js" type="text/javascript"></script>
//<script src="HighChart/highcharts-more.js" type="text/javascript"></script>

//--------!!!!!!!!!!MAIN FUCNTION------------
function  pageReady() {
    if (profileList.indexOf('0031') > -1) {} 
    else {
        alert('คุณไม่มีสิทธิ์ใช้งานเมนู WebPage Hit');
        location.assign("/");
    }

    var options = {
    chart: {
        zoomType: 'x',
        spacingRight: 20
    },
    title: {
        text: 'CPU Usage in the last 4 hour'
    },
    subtitle: {
        text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Click the chart to zoom in'
    },
    xAxis: {
        type: 'datetime',
        maxZoom: 3600000, // 1 hour
        title: {
            text: null
        }
    },
    yAxis:[ {
        title: {
            text: 'Percent'
        }
        },
        {
        title: {
            text: 'Pages/min'
        },
        min:0,
        opposite:true
        }],
    tooltip: {
        shared: true
    },
    legend: {
        enabled: true
    },
    plotOptions: {
        area: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                      //alert('End Date has been set to  ' + yymmdd);
                    }
                }
            },
            fillColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                            [0, Highcharts.getOptions().colors[1]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
            },
            lineWidth: 2,
            marker: {
                enabled: false
            },
            shadow: false,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            threshold: null
        }
    },
    series: [{
        type: 'area',
        name: '%CPU Usage',
        pointInterval: 60 * 1000,
        pointStart: Date.UTC(2020, 06, 17),
        data: []
    }, {
        type: 'line',
        name: 'Pages/min',
        yAxis: 1,
        pointInterval:60 * 1000,
        pointStart: Date.UTC(2020, 06, 17),
        data: []
    }
    ]
}




   try {
        var mxr = $('#jdata tr:last').index();
        if (mxr <= 1) {
           $('#mainChart').text($('#jdata').text('Error..in Get Data to plot chart'));
           return;
       }

      //------Starting dattiem
            var sdate =  $('#jdata tr:eq(0)  td:eq(2)').text();
            var yr = sdate.substr(0, 4);
            var mn = sdate.substr(4, 2) * 1 - 1;
            var dy = sdate.substr(6, 2);
			var hr = sdate.substr(9, 2);
		 	var mnx  =sdate.substr(12, 2);

//alert(yr +' '+mn +' '+ dy+' '+hr+' '+mn);

            options.series[0].pointStart = Date.UTC(yr, mn, dy,hr,mnx);
            options.series[1].pointStart = Date.UTC(yr, mn, dy,hr,mnx);

     //--put data into array
    $('#jdata tr').each(function () {
        var i = $(this).index();
        var usage = parseFloat($(this).find('td:eq(1)').text().replace(/,/g, ''));
        var tr =  parseFloat($(this).find('td:eq(0)').text().replace(/,/g, ''));
        options.series[0].data[i] =  Math.round(usage * 100) / 100; ;  //Cpu use
        options.series[1].data[i] =  Math.round(tr * 100) / 100;           //trans /min

    });
        //options.series[0].data.push(gdata);
        $('#mainChart').highcharts(options); //-----Got it
       
    }
    catch (er) { alert(er.message); }
}


/****

$HDR
<div class='row'>
<div class='col-md-1'></div>
<div class='col-md-10'>
<center>
<h4>CPU Usage at [#server] as of [#tday]</h4>
#nvisit
</center>

<div id="mainChart" style="border:2px solid navy;min-width: 380px; height: 500px; margin: 0 auto">
Java script may not load properly
	</div>
</div>
<div class='col-md-1'></div>
</div>

//----------------carry data to Javascript
<table id='jdata' style='display:none' >
$SQL 
exec cpuusage
$BDY
<tr><td>:00<td>:01<td>:02</tr>
$END

$TAB
|XID #nvisit
|HDR
|SQL
 
select  cn=count (distinct mcid) 
,so =sum(case when status='P' then 1 else 0 end)
from scregister where rgdate >= convert(char(8),getdate(),112)
|BDY
จำนวนผู้เยียมชมวันนี้ :00 เครื่อง เกิดใบสั่งซื้อ :01 ใบ
|ERX

|END

$PRE
Select [#tday]=convert(varchar(20), getdate() )
,[#server]=@@SERVERNAME

//END ****/
