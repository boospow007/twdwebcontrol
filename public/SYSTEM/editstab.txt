 
$HDR
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header" style="padding:10px 10px;">
            <h3></span>Add/Edit Service Table</h3>
        </div>
    <div style="padding:20px 20px;">
$SQL
if( '@rec'='NEW') 
	Select TABNAME='A-XXX',NSEAT=4 ,TABID='NEW',ZONE='Balcony'
else
 	Select * from  ServTable
	where tabId='@rec'
$BDY
<form role="form" id="tableZone" action="" method="POST">
        <div class="form-group row">
        <label for="rectxt" class="col-sm-3 col-form-label col-form-label-sm">Table ID</label>
        <div class="col-sm-9">
        <input type="text" class="form-control form-control-sm" id="rectxt"  disabled='true' value="^TABID^">
        </div>
    </div>

    <div class="form-group row">
        <label for="nmetxt" class="col-sm-3 col-form-label col-form-label-sm">Table Name</label>
        <div class="col-sm-9">
        <input type="text" class="form-control form-control-sm" id="nmetxt"  value="^TABNAME^">
        </div>
    </div>

        <div class="form-group row">
        <label for="znetxt" class="col-sm-3 col-form-label col-form-label-sm">Zone</label>
        <div class="col-sm-9">
        <input type="text" class="form-control form-control-sm" id="znetxt"   value="^ZONE^">
        </div>
        </div>     
                
        <div class="form-group row">
        <label for="nsttxt" class="col-sm-3 col-form-label col-form-label-sm">Numer of Seat</label>
        <div class="col-sm-9">
        <input type="Number" class="form-control form-control-sm" id="nsttxt"   value="^NSEAT^">
        </div>
        </div>     
  
            </form>
$ERX
ERROR @error
$END
    </div>

    <div class="modal-footer">
                <button type="button" class="btn btn-success" 
                     onclick="javascript:confirmEnt()">
                    <span class="glyphicon glyphicon-ok"></span> Confirm  
                </button>
                        
                <button type="button" class="btn btn-danger" onclick="cancelEnt()">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>  

    </div>

</div>

//END ***/
