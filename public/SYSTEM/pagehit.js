var options;
//--------!!!!!!!!!!MAIN FUCNTION------------
function  pageReady() {
    $('.sdatepicker').datetimepicker({
        format: 'YYYYMMDD',
        maxDate: new Date().setDate(new Date().getDate() - 31),
        showClear: true
    });

    $('.edatepicker').datetimepicker({
        format: 'YYYYMMDD',
        maxDate: "now",
        showClear: true
    });
    
    options = {
    chart: {
        zoomType: 'x',
        spacingRight: 20
    },
    title: {
        text: 'Number of Visit vs PO Issued'
    },
    subtitle: {
        text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Click the chart to zoom in'
    },
    xAxis: {
        type: 'datetime',
        maxZoom: 3600000, // 1 hour
        title: {
            text: null
        }
    },
    yAxis:[ {
        title: {
            text: 'N vist'
        }
        },
        {
        title: {
            text: 'PO Issue'
        },
        min:0,
        opposite:true
        }],
    tooltip: {
        shared: true
    },
    legend: {
        enabled: true
    },
    plotOptions: {
        area: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                      //alert('End Date has been set to  ' + yymmdd);
                    }
                }
            },
            fillColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                            [0, Highcharts.getOptions().colors[1]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
            },
            lineWidth: 2,
            marker: {
                enabled: false
            },
            shadow: false,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            threshold: null
        }
    },
    series: [{
        type: 'area',
        name: 'N visit',
        pointInterval: 24* 60 * 60 * 1000,
        pointStart: Date.UTC(2020, 0, 01),
        data: []
    }, {
        type: 'line',
        name: 'PO Issued',
        yAxis: 1,
        pointInterval:24 * 60 *60 * 1000,
        pointStart: Date.UTC(2020, 0, 01),
        data: []
    }
    ]
}
    setHighchart(options);
}

function setHighchart(options){
    try {
        var mxr = $('#jdata tr:last').index();
        if (mxr <= 1) {
           $('#mainChart').text($('#jdata').text('Error..in Get Data to plot chart'));
           return;
       }

      //------Starting dattiem
            var sdate =  $('#jdata tr:eq(0)').text();
            var yr = sdate.substr(0, 4);
            var mn = sdate.substr(4, 2) * 1 - 1;
            var dy = sdate.substr(6, 2);
		//	var hr = sdate.substr(9, 2);
		//	var mn   =sdate.substr(12, 2);

console.log(yr +' '+mn +' '+ dy );

            options.series[0].pointStart = Date.UTC(yr, mn, dy ,0,0);
            options.series[1].pointStart = Date.UTC(yr, mn, dy ,0, 0);

     //--put data into array
    $('#jdata tr').each(function () {
        var i = $(this).index();
        var nvisit  =  parseFloat($(this).find('td:eq(1)').text().replace(/,/g, ''));
        var po      =  parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
        options.series[0].data[i] =  Math.round(nvisit * 100) / 100; ;  //Cpu use
        options.series[1].data[i] =  Math.round(po * 100) / 100;           //trans /min

    });
        //options.series[0].data.push(gdata);
        $('#mainChart').highcharts(options); //-----Got it
       
    }
    catch (er) { alert(er.message); }
}

function searchDetail(){
    var now = new Date();
    var nowD = (now.getDate().toString().length == 1) ? '0' + now.getDate().toString() : now.getDate().toString();
    var nowM = ((now.getMonth() + 1).toString().length == 1) ? '0' + (now.getMonth() + 1).toString() : (now.getMonth() + 1).toString();
    var nowY = now.getFullYear().toString();

    var sdate = $('#sdate').val();
    var edate = $('#edate').val();
    if(sdate == '' && (edate == '' || edate == nowY + nowM + nowD)){
        return;
    } else if(sdate != '' && edate == '') {
        edate = nowY + nowM + nowD;
    }

    var qry = `select SDATE,PAGEHIT,NPO
    from webphistory
    where SDATE between '${sdate}' and '${edate}'
    order by SDATE`;
    ajaxGetQuery(qry, (isok, ret) => {
        $('#mainChart').html('');
        $('#jdata').html('');
        var jsn = JSON.parse(ret);
        var body = '';
        if(isok){
            if(jsn.dbrows > 0) {
                jsn.dbitems.map(itm => {
                    body += `<tr><td>${itm.SDATE}<td>${itm.PAGEHIT}<td>${itm.NPO}</tr>`;
                })
                $('#rangeSrh').show();
                $('#rangeSrh').html(`<center><h3>Date range: [${sdate}] to [${edate}]</h3></center>`);
                $('#jdata').html(body);
                setHighchart(options)
            }
        } else {
            alert(jsn.dbmessage);
        }

    })
}


/****

$HDR
<div class='row'>
    <div class='col-md-1'></div>
    <div class='col-md-10'>
    <center>	<h3>CPU USAGE at [#server] as of [#tday]</h3></center>
    <span id='rangeSrh' style='display:none;'></span>
        <div id="mainChart" style="border:2px solid navy;min-width: 380px; height: 500px; margin: 0 auto">
    Java script may not load properly
        </div>
    </div>
    <div class='col-md-1'></div>
</div><br>
<div class='row'>
    <div class='col-md-1'></div>
    <div class='col-md-10'>
        <h4>ระบุช่วงวันที่ สำหรับดูข้อมูลย้อนหลัง</h4>
    </div>
    <div class='col-md-1'></div>
</div><br>
<div class='row'>
    <div class='col-md-1'></div>
    <div class='col-md-10'>
        <div class='row'>
            <form action="javascript:searchDetail();">
                <div class='col-md-2 col-xs-3' style='padding:0px' id='stimeDiv'>
                    <div class='input-group date sdatepicker'>
                        <input type='text' class="form-control" id='sdate' autocomplete="off" placeholder="SDATE (YYYYMMDD)"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class='col-md-1 col-xs-1 text-center' style='padding:0px; width: 50px;' id='stimeDiv'>
                    <h4>to</h4>
                </div>
                <div class='col-md-2 col-xs-3' style='padding-left:0px' id='etimeDiv'>
                    <div class='input-group date edatepicker'>
                        <input type='text' class="form-control" id='edate' autocomplete="off" placeholder="EDATE (YYYYMMDD)"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <button type='submit' class='btn btn-info' onclick="this.blur();">Search</button>
            </form>
        </div>
    </div>
    <div class='col-md-1'></div>
</div>

//----------------carry data to Javascript
<table id='jdata' style='display:none' >
$SQL 
 select SDATE,PAGEHIT,NPO 
  from webphistory
  where SDATE> convert(char(8),dateadd(dy,-30,getdate()),112)
  order by SDATE
$BDY
<tr><td>:00<td>:01<td>:02</tr>
$END

$PRE
Select [#tday]=convert(varchar(20), getdate() )
,[#server]=@@SERVERNAME

//END ****/
