 
 function pageReady(){
        var usrid=readCookie('WUSERID');
        if(usrid ==null){
            alert('You are not login yet')
            return;
        }
     $('#usrtxt').val(usrid);
     $('#chPasswModal').modal('show');
 }

 function confirmClick(){
     var usrid=$('#usrtxt').val();
     var opwd=$('#pwdtxt').val();
     var npwd=$('#nwdtxt').val();
     var cpwd=$('#cwdtxt').val();
    if(npwd.length<6) {
        alert("Password Should More tahn 6 characters")
        return
    }
    if(npwd!=cpwd) {
        alert('Confirm Passwrod is not the same')
        return
    }
    var qry="exec ChangePassword '@userid','@passw','@npassw' ";
    
    var pdata={query:qry,
            para:{userid:usrid,
            passw:opwd,
            npassw:npwd}}
    ajaxExec(pdata,function(isok,ret){
        if(isok){
            var jsn=JSON.parse(ret);
            if(jsn.dberror==0){
                alert('Password has been changed successful');
                location.reload();
            }
            else alert(jsn.dbmessage);
        }
        else alert('System Error can not call ajax')
    });
     
 }
   
/*** 
  
 <div class="modal fade" id="chPasswModal" role="dialog"  tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="padding:25px 20px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4>  <span class="glyphicon glyphicon-lock"></span>Change Password</h4>
                    
                </div>
                <div style="padding:40px 50px;">
 
                        <div class="form-group">
                            <label for="usrtxt"><span class="glyphicon glyphicon-user"></span> UserId</label>
                            <input type="text" class="form-control" name="xuserid" id="usrtxt" value="" disabled='true'>
                        </div>

                        <div class="form-group">
                            <label for="pwdtxt"><span class="glyphicon glyphicon-eye-open"></span>Old Password</label>
                            <input type="password" class="form-control" name="passw" id="pwdtxt" placeholder="Enter password" value="">
                        </div>
 
                 <div class="form-group">
                            <label for="nwdtxt"><span class="glyphicon glyphicon-eye-open"></span>New Password</label>
                            <input type="password" class="form-control" name="passw" id="nwdtxt" placeholder="New  password" value="">
                        </div>

                 <div class="form-group">
                            <label for="cwdtxt"><span class="glyphicon glyphicon-eye-open"></span>Confirm Password</label>
                            <input type="password" class="form-control" name="passw" id="cwdtxt" placeholder="Confirm password" value="">
                 </div>

                 <button type="button" class="btn btn-success btn-block" onclick="confirmClick()">
                            <span class="glyphicon glyphicon-off"></span>Confirm
                </button>

                        <div id='message' class="info info-default">

                        </div>
                   </form>
                </div>

                <div class="modal-footer">
                    <p>Forgot <a href="javascript:resetPassword();">Password?</a></p>
                </div>

            </div>
        </div> 
    </div>

//END **/

