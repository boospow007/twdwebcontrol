
/****
$HDR
<table class='table'>
<thead><th>Step<th>Time <th>Command<th>Duration<th>Status
$SQL
 Select   SNM=left(a.Step_name,50)
  , RD=Run_date 
  , RT=substring(convert(varchar(8), run_time/100+10000 ),2,2)+'.' 
        +substring(convert(varchar(8), run_time/100+10000 ),4,2) 
   ,SEC=run_duration
   ,REM=case when run_status  =1 then 'Success' else 'Fail'  end 
   ,STP=a.step_id 
   ,JOB=convert(varchar(50),a.job_id)
	,CMD=replace(Command,char(13),'<BR>')
	  From msdb..sysjobsteps  a
	  ,msdb..sysjobhistory b
	  ,msdb..sysjobs c  
      where a.job_id=b.job_id 
	  and a.job_id=c.job_id  
      and a.step_id =b.step_id  
      and convert(varchar(50),a.job_id)  ='@jobid'  
      and a.step_id >  0  
      and run_date= '@rundt'  
      order by run_time desc
$BDY
<tr> 
<td align='left'>^SNM^</td>
<td>^RT^ </td>
<td>^CMD^</td>
<td>^SEC^</td>
<td>^REM^</td>

</tr> 
$END
</table>

//END  ***/