 
$HDR
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header" style="padding:10px 10px;">
            <h3></span>Add/Edit Service Zone</h3>
        </div>
    <div style="padding:20px 20px;">
$SQL
if( '@rec'='NEW') 
	Select ZONENAME='', recid='NEW'
else
 	Select * from  SERVICEZONE
	where recid='@rec'
$BDY
<form role="form" id="zoneForm" action="" method="POST">

    <div class="form-group row">
        <label for="rectxt" class="col-sm-3 col-form-label col-form-label-sm">Zone ID</label>
        <div class="col-sm-9">
        <input type="text" class="form-control form-control-sm" id="rectxt"  disabled='true' value="^recid^">
        </div>
    </div>

    <div class="form-group row">
        <label for="nmetxt" class="col-sm-3 col-form-label col-form-label-sm">Zone Name</label>
        <div class="col-sm-9">
        <input type="text" class="form-control form-control-sm" id="nmetxt"  value="^ZONENAME^">
        </div>
    </div>
  
</form>
$ERX
ERROR @error
$END
    </div>

    <div class="modal-footer">
                <button type="button" class="btn btn-success" 
                     onclick="javascript:confirmEntZone()">
                    <span class="glyphicon glyphicon-ok"></span> Confirm  
                </button>
                        
                <button type="button" class="btn btn-danger" onclick="javascript:cancelEntZone()">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>  

    </div>

</div>

//END ***/
