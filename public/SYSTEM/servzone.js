function pageReady(){
    $('#showDiv').mouseleave(function () {         
              $('#popUpMenu').hide();
    });
}
var actionLine = 0;
var editObj;
function menuOver(obj) {
  editObj =obj;
  var pos = $(obj).position();

  actionLine = $(obj).find('td:eq(0)').text();
  $('#popUpMenu').css('top', pos.top);
  var left=$(window).width();
  $('#popUpMenu').css('left', cursorX);
  $('#popUpMenu').show();
 
}
function editItem(rec ){
  //alert('Call for detail');
  $('#popUpMenu').hide();
  try{
      var card=0;
      var pos={top:100,left:0};

      if(editObj){
            card=$(editObj).find('td:eq(0)').text(); 
             pos =$(editObj).position();
      }
      
      
      if(rec==='NEW')card='NEW';
      var pdata={
              patname:'./SYSTEM/editszone.txt',
              para:{
                  stc:storeCode,
                  rec:card
                  }
          }
      ajaxXpandFile(pdata,function(isok,data){
          console.log("AJAX call Edit "+isok);
          $('#editModal').html(data);
          $('#editModal').modal('show')  ;        
      })

   
  }
  catch(ex){alert(ex.message);}
}

function deleteItem(){
  try{
      var rcpt =$(editObj).find('td:eq(0)').text();
      if(confirm('Remove Zone Id: '+rcpt)==0)return;
      //alert('VOid');
      var cmd="delete SERVICEZONE where RECID=  @rec";
      var pdata={
              query:cmd,
              para:{stcode:storeCode, 
                     rec:rcpt} 
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                   //  alert('User has been removed');
                     //--reload this page
                     $('#editModal').modal('hide')  ;
                     pageReload();
              }
              else
                  alert(jsn.dbmessage);
          }
          else
              alert('System error Cant call service');
      });
  }
  catch(ex){alert('delete function Error..'+ex.message);}
}

function hideDetail(){
 // $('#detailCard').hide();
  $('#editModal').modal('hide')  ;
}

function confirmEntZone(){
  console.log("Confrim Entry");
  try{
      var rec = $('#rectxt').val();	
      
      var cmd="Update SERVICEZONE set ZONENAME='@nme'  Where RECID = @rec ";
      if(rec=='NEW')
          cmd="Insert SERVICEZONE(ZONENAME)  " 
              +"values('@nme')";
      
      var pdata={
              query:cmd,
              para:{ 
                    nme:$('#nmetxt').val(),                                        
                    rec:rec
                    }
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                   //  alert('Service Table  has been changed');
                     //--reload this page
                     $('#editModal').modal('hide')  ;
                     pageReload();  //--load same Pattern again
              }
              else
                  alert(jsn.dbmessage);
          }
          else
              alert('System error Cant call service');
      });
  }
  catch(ex){alert('delete function Error..'+ex.message);}
}
function cancelEntZone(){
  //$('#detailCard').hide();
  $('#editModal').modal('hide')  ;
}
//---Add New item
function addNewItem(){
  //alert('Add New');
  editItem('NEW');
}

/*** 
$HDR
<div id ='showDiv' class='container'>
<div class='row'>
<h2>Service Zone</h2>
<a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>

<table class='table'>
<thead><th>ID<th>ZONENAME</thead>
$SQL
Select * from SERVICEZONE
$BDY
<tr onmouseover ='menuOver(this)' >
<td>^recid^<td>^ZONENAME^</tr>
$ERX
<tr><td colspan='3'>Error @error
$END
</table>
  
<div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
   <a href="javascript:deleteItem()" class="btn smn btn-danger">Delete</a>
   <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>

</div>
</div>
$xxx

//END  **/