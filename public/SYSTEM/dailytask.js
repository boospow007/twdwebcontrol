function pageReady(){
	if (profileList.indexOf('0026') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Node Daily Task');
		location.assign("/");
	}
	  
	  $('#showDiv').mouseleave(function () {
	   //     $('#detailCard').hide();
	        $('#popUpMenu').hide();
	    });
}


var actionLine = 0;
var  onmouseover ='menuOver(this)'
function menuOver(obj) {
	editObj =obj;
    var pos = $(obj).position();
  //  var mpos = $('#mainDiv').position();
    actionLine = $(obj).find('td:eq(0)').text();
    $('#popUpMenu').css('top', pos.top);
	var left=$(window).width();
	$('#popUpMenu').css('left', left-160);
    $('#popUpMenu').show();
   
}
function taskHist(){
	try { 
	var id=$(editObj).find('td:eq(0)').text();
	var nm=$(editObj).find('td:eq(1)').text();
    var qry="Select Top 10 RT=lastrun,finish,result from CLOUDPOS..CStaskHistory where taskid="+ id +"  order by lastrun desc";
    var hdr ="<table ><thaed><th>RunTime<th>Finish<th>Result</thead>";
	var bdy ="<tr><td>^RT^<td>^finish^<td>^result^</tr>";
	var end ="</table>";
	var pdata ={hdr:hdr,qry:qry,bdy:bdy,end:end,erx:'@error'}
	
	ajaxXpandPat(pdata,function(isok,ret){
		$('#detailDiv').html(ret);
		$('#histLb').text(nm);
		var pos =$(editObj).position();
			$('#histDiv').css('top',pos.top);
			$('#histDiv').show();
		});
	}
	catch(ex){alert(ex.message);}
}

function editTask(opt){
	//alert('Call for detail');
	try{
    
		var pos =$(editObj).position();
  		var id=$(editObj).find('td:eq(0)').text();
		id=id.trim();
	    if(opt=='NEW')id='NEW'
	    var pdata={
	    		patname:'/SYSTEM/edittask.txt',
	    		para:{
	    			taskid:id
	    			}
	    	}
	    ajaxXpandFile(pdata,function(isok,data){
	    	//alert(data);
			$('#detailCard').html(data);
			$('#detailCard').css('top',pos.top);
			$('#detailCard').show();
	    })

	 
	}
	catch(ex){alert(ex.message);}
}

 

//---Add New item
function addNewItem(){
	//alert('Add New');
	editTask('NEW');
}

function hideDetail(){
	$('#detailCard').hide();
	
}
function confirmEnt(){
	try{
		var id = $('#idtxt').val();	
		
		var cmd="Update CLOUDPOS..CSDailytask set taskname='@nme',Stime='@stm',ETime='@etm' ,STATUS='',nextrun=getdate()" 
		  +  ",Recuring='@rcr',isactive='@act',query='@qry'  where ID=@tid" 

		if(id=='NEW')
			cmd="Insert CLOUDPOS..CsDailytask(taskname,stime,etime,recuring,isactive,query ) "
			+ "values('@nme','@stm', '@etm','@rcr','@act','@qry')";
		
		var pdata={
				query:cmd,
				para:{ 
				      nme:$('#nmetxt').val(),
				      stm:$('#stmtxt').val(),
				      etm:$('#etmtxt').val(),
				      rcr:$('#rcrtxt').val(),
				      act:$('#acttxt').val(),
				      qry:$('#qrytxt').val(),
					   tid:id
				      }
		  }
		ajaxExec(pdata,function(isok,resp){
			if(isok){
				var jsn =JSON.parse(resp);
				if(jsn.dbcode==0){
					   alert('Task has been changed');
					   //--reload this page
					   pageReload();  //--load same Pattern again
				}
				else
					alert(jsn.dbmessage);
			}
			else
				alert('System error Cant call service');
		});
	}
	catch(ex){alert('delete function Error..'+ex.message);}
}
function cancelEnt(){
	$('#detailCard').hide();
}

function removeEnt(){
	var id = $('#idtxt').val();	
	if(confirm("Delete JOB ID "+id)==0)return;
	var cmd="Delete  from  CLOUDPOS..CSDailytask   where ID=" +  id ;
	ajaxGetQuery(cmd,function(isok,resp){
		if(isok){
			var jsn =JSON.parse(resp);
			if(jsn.dbcode==0){
				   pageReload();  //--load same Pattern again
			}
			else
				alert(jsn.dbmessage);
		}
		else
			alert('System error Cant call service');
	});

}
function hideHist(){
	$('#histDiv').hide();
}
/***  Must have atleast 3* 
$HDR
<style>
.wraptd {
white-space: nowrap; 
	text-overflow:ellipsis; 
	overflow: hidden;
	max-width:160px;
}
#dataTab tr:hover{
	background-color:silver;
	background: #eee;
	cursor:pointer;
}
#histDiv {
	border-radius: 12px;
    box-shadow: 1px 1px 5px 6px #ccc; 
}

</style>

<div id ='showDiv'>
 <h3>Daily Task on #server</h3> 
 <a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>
 
<table class='table'>
<thead><th>ID<th>Name<th>Query<th>RunTime<th>Etime
<th>Recur<th>En<th> lastRun<th>NextRun<th>status<th>Result
</thead>
$SQL
SElect id,taskname,
query
,stime=replace(stime,':','.')
,etime=replace(etime,':','.')
,lastrun,lastresult,recuring
,isactive
,nextrun
,status
from CLOUDPOS..csdailytask
$BDY
  <tr onmouseover ='menuOver(this)'><td>^id^
   <td>^taskname^
   <td>^query^
  <td>^stime^
  <td>^etime^
  <td>^recuring^
  <td>^isactive^
  <td>^lastrun^
  <td>^nextrun^
    <td>^status^
  <td class='wraptd'>^lastresult^
$ERX
ERROR @error
$END
</table>

//--must inside 'showDiv' to hode when leave this div
 
<div  class='rndDiv' id='histDiv'  style='display:none;z-index:6;position:absolute;top:80px'
	onmouseout="javascript:hideHist()" > 	 
	<h4><span id='histLb'>Task History</span></h4>
	<div id='detailDiv'>
	</div>
</div>
 

  <div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
     <a href="javascript:taskHist()" class="btn smn btn-danger">History</a>
     <a href="javascript:editTask(0)" class="btn smn btn-primary">Edit</a>
</div>

//--must inside 'showDiv' to hode when leave this div
<div  class='rndDiv' style='display:none;z-index:6;position:absolute;top:80px' id='detailCard'; > 	 
</div>

</div>

$PRE
 select [#server]=@@SERVERNAME

//END ***/