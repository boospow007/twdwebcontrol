
 function pageReady() {
        var pdata={patname:'SYSTEM/sorder.txt',
                    para:null
                    }
        xpand2div(pdata,'sorederDiv')
    } 

 function xpand2div(pat,div){
    try {
        ajaxXpandFile(pat,function(isok,ret){
            if(isok){
                $('#'+div).html(ret);
            }
            else alert('Ajax call Error')
        });
    }
    catch(ex){alert(Ex.message)}
 }

 function showEdetail(obj){
    var ref =$(obj).find('td:eq(0)').text();
    ref=ref.trim();
    var pdata ={patname:'SYSTEM/edetail.txt',
            para: {recid:ref}
             }
       xpand2div(pdata,'detailDiv') ;      
 }

/***
$HDR
<style>
.table tbody tr:hover {
       background-color:silver;
       cursor:pointer ;
    }

 </style>
 <h2>Payment Error Record</h2>
    <hr>
<div class='container-fluid' >
<div class='row'>
    <div class='col col-md-4'>
     <h4>Sales Order Lately</h4>
     <div id ='errorDiv'>
<table class='table'>
<thead><th>Recid<th>Reference<th>Time</thead>
$SQL
Select * from PAYRECORD
where STATUS='E'
order by RECID DESC
$BDY
<tr onclick='javascript:showEdetail(this)' >
<td>^RECID^
<td>^REF^
<td>^RTIME^

$END
</table>
     </div>
    </div>
    <div class='col col-md-8'>
   
       <div id ='detailDiv'>
        Detail to be put here
       </div>
    </div>
</div>
</div>

//END  **/
 