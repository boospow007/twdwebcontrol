

var TwdMapArr;
function pageReady() {
	if (profileList.indexOf('0029') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Store Location');
		location.assign("/");
  }
  
  //Get Map Information from TWDGPS https://medium.com/@limichelle21/integrating-google-maps-api-for-multiple-locations-a4329517977a
  var qry = "select * from CLOUDPOS..TWDGPS";
  ajaxGetQuery(qry, function (isok,ret) {
    if (isok) {
    //  console.log(ret);
      var jsn = JSON.parse(ret);
      var itms  = jsn.dbitems;
      TwdMapArr = new Array();
      for (let i = 0; i < jsn.dbrows; i++) {
        TwdMapArr.push({ "name":itms[i].SName, "lat": parseFloat(itms[i].Lat), "lng": parseFloat(itms[i].lng) });
      }
     // var jString = JSON.stringify(maparr);
      //Send to generate map
      initMap();
    }
    else {
      alert(ret);
    }
  });
}


function initMap(maparr) {
  try {
    var infowindow =  new google.maps.InfoWindow({});
   // inputarr = JSON.parse(maparr);
    var marker, count;
    var myLatLng = { 'lat': TwdMapArr[0].lat, 'lng': TwdMapArr[0].lng };
    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 10
    });
    for (count = 0; count < TwdMapArr.length; count++) {
      var maplatlng = { 'lat': TwdMapArr[count].lat, 'lng': TwdMapArr[count].lng };
      var mapname =  TwdMapArr[count].name;
        marker = new google.maps.Marker({
          position: maplatlng,
          map: map,
          title: mapname
        });
        google.maps.event.addListener(marker, 'mouseover', (function(marker, count) {
          return function() {
            infowindow.setContent(TwdMapArr[count].name);
            infowindow.open(map, marker);
          }
        })(marker, count));
    }
   
  }
  catch (ex) { alert(ex.message); }
}

var  cobj =null ;
var editid=0;
function menuOver(obj) {
    var pos = $(obj).position();
   var mpos = $('#mainDiv').position();
   var tabw = 10; //$('#altTab').width()-160;

    cobj=obj;
    $('#floatMenu').css('top', pos.top+mpos.top);
    $('#floatMenu').css('left',mpos.left+tabw);
    $('#floatMenu').show();
}
function editItem() {
//alert(actionLine);
	editid 		=$(cobj).find('td:eq(3)').text();
    var name 	=$(cobj).find('td:eq(0)').text();
	var lat		=$(cobj).find('td:eq(1)').text();
	var long 	=$(cobj).find('td:eq(2)').text();
	$('#recID').text(editid);
	$('#altname').val(name);
	$('#latitude').val(lat);
	$('#longitude').val(long);
    $('#editModal').modal('show'); 
  }
//-------------------------------
function addNew() {
 
	editid=0;
	$('#recID').text(editid);
    $('#altname').val("NEW");
	$('#latitude').val("12.000");
	$('#longitude').val("100.000");
    $('#editModal').modal('show');
  }

function cancelEntry(){
 $('#editModal').modal('hide');
}
//---------------------
function confirmEntry(){

  try {

	var updcmd 	="Update TWDGPS set Sname='@name' ,Lat=@lat,lng=@long Where RECID =@rec";
  var inscmd 	="insert TWDGPS (Sname,Lat, Lng) values( '@name' , @lat, @long) ";
  
	var name	=$('#altname').val();
	var lat		=$('#latitude').val();
	var lng		=$('#longitude').val();

	var qry	  =updcmd.replace('@name',name).replace('@lat',lat).replace('@long',lng);
    if (editid==0)qry	=inscmd.replace('@name',name).replace('@lat',lat).replace('@long',lng);

	qry= qry.replace('@rec',editid);
console.log(qry);

  ajaxGetQuery(qry,
		   function(bool,ret) {
			   if (bool){
          var jsn=JSON.parse(ret);
          alert(jsn.dbmessage); 
          pageReload();
          return;
         }
         else alert(ret);
		   });

    }
 catch(ex) {alert(ex.message);}

}

function deleteItem() {
	var id =$(cobj).find('td:eq(3)').text();
     if( confirm('Delete ID '+id) ==1) {
        var qry ="Delete from TWDGPS where RECID ="+id;
        ajaxGetQuery(qry,
		   function(bool,ret ) {
        if (bool){
          var jsn=JSON.parse(ret);
          alert(jsn.dbmessage); 
          pageReload();
          return;
         }
         else alert(ret);
		    
		   });
     }
}

 

/******
$HDR


<div class='container-fluid'>
<div class='row'>
 <div id ='mainDiv' class='col-md-3'>
<a href='javascript:addNew();' class='btn btn-primary pull-right'>Add NEW</a>
<table id ='locTab'  class='table'>
<thead><th>Name<th>Lat<th>Long<th>ID</thead>
$SQL
select Sname,Lat=convert(float,lat),Long=convert(float,lng),RECID
 from CLOUDPOS..TWDGPS
order by SNAME
$BDY
<tr onmouseover ='javascript:menuOver(this)'>
<td>:00<td>:01<td>:02<td>:03
$END
</table>

</div>

 <div class='col-md-9'>
	<h2>Thaiwatsadu map</h2>
     <div id="map" style="width: 100%; height: 800px; position: absolute;">
       <div id="map-canvas"></div>
     </div>

 </div>

</div>
</div>
//----------Popupmenu to Edit or Delete Item
<div id='floatMenu' style='display:none;z-index:6;position:absolute;'>
     <a href="javascript:deleteItem()" class="btn smn btn-danger">Remove</a>
     <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>
<!-- ADD/EDIT Modal -------------------->
 <div class="modal fade" id="editModal" role="dialog"  tabindex="-1">

	 <div class="modal-dialog modal-md">

		<!-- Modal content-->
		 <div class="modal-content">
				<div class="modal-header">
					<h3>Add/Edit LOCATION GPS ID#<label id='recID'>XXXX</label> </h3>
				</div>  
	 
		   <div class="modal-body" id="Div1" >

				<form action=" ">		 
					 <div class="form-group">
						 
						<label for="altname">Short Name </label>
						<input type="text" class="form-control"  id="altname" placeholder="Name" value="">

						<label for="latitude">Latitude</label>
						<input type="text" class="form-control"   id="latitude" placeholder="Latitude" value="">

						<label for="longitude">Longitude</label>
						<input type="text" class="form-control"  id="longitude" placeholder="Long" value="">
						</div> 
					
					
					  
					<a href ="javascript:confirmEntry()" class='btn btn-primary'>Confirm</a>
					<a href="javascript:cancelEntry()" class='btn btn-danger'>Cancel</a>
					<br><label id='errLabel'></lable>
					 </div>   
				</form>

			</div>

		</div>
	</div>
 </div>


//END ******/