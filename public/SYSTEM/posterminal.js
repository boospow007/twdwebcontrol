 

function pageReady(){
    $('#showDiv').mouseleave(function () {
         //     $('#detailCard').hide();
              $('#popUpMenu').hide();
          });
}
var actionLine = 0;
var editObj;
function menuOver(obj) {
  editObj =obj;
  var pos = $(obj).position();
//  var mpos = $('#mainDiv').position();
  actionLine = $(obj).find('td:eq(0)').text();
  $('#popUpMenu').css('top', pos.top);
  var left=$(window).width();
  $('#popUpMenu').css('left', cursorX);
  $('#popUpMenu').show();
 
}
function editItem(rec ){

  try{
      var card=0;
      var pos={top:100,left:0};
      if(editObj){
            card=$(editObj).find('td:eq(0)').text(); 
             pos =$(editObj).position();
      }
      
      
      if(rec==='NEW')card='NEW';
      var pdata={
              patname:'./SYSTEM/editpos.txt',
              para:{
                  stc:storeCode,
                  rec:card
                  }
          }
      ajaxXpandFile(pdata,function(isok,data){
         // console.log("AJAX call Edit "+isok);
          $('#editModal').html(data);
          $('#editModal').modal('show')  ;
      })

   
  }
  catch(ex){alert(ex.message);}
}

function deleteItem(){
  try{
      var rcpt =$(editObj).find('td:eq(0)').text();
      if(confirm('Remove this user'+rcpt)==0)return;
      //alert('VOid');
      var cmd="delete POSTERMINAL where posid=  @rec";
      var pdata={
              query:cmd,
              para:{stcode:storeCode, 
                     rec:rcpt} 
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                   //  alert('User has been removed');
                   $('#editModal').modal('hide')  ;
                     //--reload this page
                     pageReload();
              }
              else
                  alert(jsn.dbmessage);
          }
          else
              alert('System error Cant call service');
      });
  }
  catch(ex){alert('delete function Error..'+ex.message);}
}

function hideDetail(){
  $('#editModal').modal('hide')  ;
}

function confirmEntPos(){
  
  try{
      var rec = $('#rectxt').val();	
      
      var cmd="Update POSTERMINAL set taxid='@tax',RCVNO='@rcv', LOCKNO ='@lck' Where posid = @rec ";
      if(rec=='NEW')
          cmd="Insert POSTERMINAL( TAXID,LOCKNO,RCVNO,STCODE)  " 
              +"values( '@tax','@lck',@rcv,'@stc' )";
      
      var pdata={
              query:cmd,
              para:{ 
                    tax:$('#taxtxt').val(),
                    lck:$('#lcktxt').val(),
                    rcv:$('#rcvtxt').val(),
                    stc:storeCode,
                    rec:rec
                    }
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                     alert('Posterminal has been saved');
                     $('#editModal').modal('hide')  ;
                     //--reload this page
                     pageReload();  //--load same Pattern again
              }
              else
                  alert(jsn.dbmessage);
          }
          else
              alert('System error Cant call service');
      });
  }
  catch(ex){alert('delete function Error..'+ex.message);}
}
function cancelEntPos(){
  $('#editModal').modal('hide')  ;
}
//---Add New item
function addNewItem(){
  //alert('Add New');
  editItem('NEW');
}

/*** 
$HDR
<div id ='showDiv' class='container'>
<div class='row'>
<h2>Posterminal</h2>
<a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>

<table class='table'>
<thead><th>Id<th>LockNo.<th>TaxId<th>RcvNo.<th>Last Use</thead>
$SQL
Select * from POSTERMINAL
$BDY
<tr onmouseover ='menuOver(this)' >
<td>^posid^<td>^LOCKNO^<td>^taxid^<td>^RCVNO^ <td>^lastuse^</tr>
$ERX
<tr><td colspan='6'>Error @error
$END
</table>
 

<div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
   <a href="javascript:deleteItem()" class="btn smn btn-danger">Delete</a>
   <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>
</div>

</div>
$xxx

//END  **/