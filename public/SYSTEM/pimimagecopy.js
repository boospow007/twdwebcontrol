function pageReady(){
	if (profileList.indexOf('0027') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู PIM Image Copy');
		location.assign("/");
	}

	getAllTaskName();
}

function getAllTaskName(){
	var qry = `Select distinct taskname as name from CLOUDPOS..nodeschedule order by taskname`;
	ajaxGetQuery(qry, (isok, ret) => {
		var jsn = JSON.parse(ret);
		console.log(jsn)
		if(isok) {
			if(jsn.dbrows > 0){
				var opts = '';
				for(i = 0; i < jsn.dbrows; i++){
					opts += `<option value='${jsn.dbitems[i].name}'>${jsn.dbitems[i].name}</option>`;
				}
				$('#tasktype').append(opts);
			}
		} else {
			alert(jsn.dbmessage)
		}
	})
}

function changeTaskType(obj, value){
	// var tr = $('#nodetasktb tbody tr');
	// for (i = 0; i < tr.length; i++) {
	// 	td = tr[i].getElementsByTagName("td")[0];
	// 	if (td) {
	// 	  txtValue = td.textContent || td.innerText;
	// 	  if (txtValue.toUpperCase().indexOf(value) > -1) {
	// 		tr[i].style.display = "";
	// 	  } else {
	// 		tr[i].style.display = "none";
	// 	  }
	// 	}
	//   }
	obj.blur();
	var qry = `Select top 50 taskname, format(rundate, 'yyyy-MM-dd HH.mm') as rundate, result, recid from CLOUDPOS..nodeschedule where taskname = '${value}' order by RECID DESC`;
	if (value == '') {
		qry = `Select top 50 taskname, format(rundate, 'yyyy-MM-dd HH.mm') as rundate, result, recid from CLOUDPOS..nodeschedule order by RECID DESC`;
	}
	
	ajaxGetQuery(qry, (isok, ret) => {
		var jsn = JSON.parse(ret);
		if(isok) {
			if(jsn.dbrows > 0){
				$('#body').html('')
				for(var i = 0; i < jsn.dbrows; i++){
					$('#body').append(`<tr><td>${jsn.dbitems[i].taskname}</td><td>${jsn.dbitems[i].rundate}</td><td>${jsn.dbitems[i].result}</td><td>${jsn.dbitems[i].recid}</td></tr>`);
				}
			}
		} else {
			alert(jsn.dbmessage)
		}
	})
}

/***  Must have atleast 3* 
$HDR

 <h3>Daily PIM Image Copy Task on #server</h3><br>
 <h4>Task Name 
 <select id='tasktype' style='border-radius:4px' onchange='changeTaskType(this, this.value);'>
	<option value=''>All Task</option>
 </select></h4>
 <a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>
 
<table class='table' id='nodetasktb'>
<thead><th>Name</th><th>Run Date</th><th>Result</th><th>Recid</th>

</thead>
<tbody id='body'>
$SQL
 Select top 50 * 
 from CLOUDPOS..nodeschedule
order by RECID DESC
$BDY
  <tr><td>:00</td>
   <td>:01</td>
   <td>:02</td>
  <td>:03</td>
  </tr>
   
$ERX
ERROR @error
$END
</tbody>
</table>

$PRE
 select [#server]=@@SERVERNAME

//END ***/