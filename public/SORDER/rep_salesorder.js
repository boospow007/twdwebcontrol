 
/// SORDER
var wait="<center><img src='../images/wait.gif' ></center>";
var loading="<center><img src='../images/loading.gif' ></center>";
var storeCode='';
var refStatus='';
    
 function pageReady() {

    // initialize our loader overlay
    //loader.initialize();


    storeCode=readCookie("WDFSTORE");
    pmPerfUserid = readCookie('WUSERID');

    if (profileList.indexOf('0056')>-1) {                         
    }
    else {
        alert('คุณไม่มีสิทธิ์ใช้งานเมนู Sales Order Report');
        location.assign("/report");
    }

    var pdata ={patname:'SORDER/rep_sorder.txt',
                para:{ 
                    stc:userStore,
                    store:storeCode,
                    userid:pmPerfUserid,
                    lockno:''
                    ,srhdate1:''
                    ,srhdate2:''             
                    ,textsrch:''
                    ,textref:''
                    ,sostatus:''
                    ,salesource: '' 
                    ,groupstc: 'All'                 
                }
        };
    xpand2div(pdata,'sorderDiv')    
    
    // $('#exportsum').hide();
    // $('#exportdtl').hide();
    // $('#exportchecker').hide();
    //if (profileList.indexOf('0023')>-1) { 
        //$('#exportsum').show();
        //$('#exportdtl').show();
        //$('#exportchecker').show();
    //}    

} 

 function xpand2div(pat,div){
    try {
        ajaxXpandFile(pat,function(isok,ret){            
            if(isok){
                $('#'+div).html(ret);
                $('.form_datetime').datepicker({
                    format: "yyyymmdd",
                    todayBtn: true,
                    language: "th",
                    orientation: "bottom left",
                    autoclose: true,
                    todayHighlight: true
                });
                $('.form_datetime').datepicker('update', new Date());
                $('#orderstoregroup, #sel-salesource').multiselect({
                    buttonWidth: '100%',
                    enableFiltering:true,
                    includeSelectAllOption:true,
                    inheritClass:true,
                    nonSelectedText: 'กรุณาเลือก',
                    maxHeight: 400
                });
                setNumericFormatDiv(div);
      
                 if(div=='sorderDiv') sorderReady();
                  // After Load --> Gen barcode and check can this ref void
                  setTimeout(afterLoadSO(),500) ;

            }
            else alert('Ajax call Error')
        });
    }
    catch(ex){alert(ex.message)}
 }

 function afterLoadSO(){
    //----creat Barcode image 
    // setTimeout(createRefBarcode(),500) ;  webcontrol dont load barcode
     
    profileList= readCookie('WUPROFILE');
    $('#void_button').hide();
    //if (profileList.indexOf('0021')>-1) {   
    //    if (refStatus=='P'||refStatus=='Q')      
    //        $('#void_button').show();
    //}
    //else {        
    //    $('#void_button').hide();
    //}

     
 
 }

 function showdetail(ref,stts,stc){          
    refStatus=stts;       
    $('#detailDiv').html(wait);
    var pdata ={patname:'SORDER/sdetail.txt',
            para: {ref:ref
                ,stc:stc   
                ,stat:stts     
            }
        }
    xpand2div(pdata,'detailDiv');
    $('#detilModal').modal('show');
       
            
 }

 function createRefBarcode(){
	try { 
		$('.barCode').each(function(){
			var  prcd	=$(this).attr('name');
			var  id	=$(this).attr('id');
                                               
            if (prcd=='#ref') return; // no ref

			   JsBarcode('#'+id , prcd,
					   {
					 height:40,
					 displayValue: false
					   });
		});
		   
		}
	   catch(ex){alert(ex.message);}
   }

 
function voidSorder(r,s) {	 
	try { 
        var msg='ต้องการยกเลิกคำสั่งซื้อ #'+r+' หรือไม่';
        if(confirm(msg)==0)return;

        pmPerfUserid = readCookie('WUSERID');
        var qry="exec CLOUDPOS..cs_voidSorder '@ref' , '@stc' ,'@userid' ";
        var pdata={
                query:qry,
                para: {
                    ref:r
                    ,stc:s
                    ,userid:pmPerfUserid
                }
            } 
        ajaxExec(pdata,function(isok,ret){
         //alert(ret);
            var jsn =JSON.parse(ret);
            if(jsn.dbcode==0)
             {
                alert('ยกเลิกคำสั่งซื้อ #'+r+' เรียบร้อยแล้ว');
                pageReady();
             }
            else
                alert(jsn.dbmessage);
            
        });
    }
    catch(ex){alert(ex.message);} 
}

function printSorder(r,s) {
    var msg='ต้องการพิมพ์คำสั่งซื้อ #'+r+' หรือไม่';
    if(confirm(msg)==0)return;

	// green : page-break
	// More Setting ที่ printer .. Margins ต้องเป็น None
	var datap=$('#sorderRefDiv').html();
	console.log(datap)
 

	var mywindow = window.open('', 'mydivsorder', ' ');
	mywindow.document.write('<html><head><title></title>');
	mywindow.document.write('<link rel="stylesheet" href="/stylesheets/print.css" type="text/css"  media="print" charset="utf-8" />');  
	mywindow.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
	mywindow.document.write('  @media all {  .page-break { display: none; }} @media print { .page-break { display: block; page-break-before: always;overflow: hidden!important; }} ');
	mywindow.document.write('  .hideme {display:none;}');
	mywindow.document.write(' </style></head><body>');
	
	mywindow.document.write(datap);
	
	mywindow.document.write('</body></html>');	 	 

	setTimeout( function(){
		mywindow.print();
		mywindow.close();
	}
		,1000)
 	  
}



function searchSorder(){
	try {
        storeCode=$('#orderstore').val();
		userId = readCookie('WUSERID');

		var ssdate = $('#sdate').val();
        var sedate = $('#edate').val();
		var sstatus = $('#orderstatus').val();		 
		var tsearch = $('#smartsrch').val();
        var tref = $('#smartref').val();
        
	 
		$('#sorderDiv').html(waitgif);
		//--------Create Pattern inside 

	 	 var pdata ={patname:'SORDER/rep_sorder.txt',
            para:{ 
                    stc:userStore,
                    store:storeCode,
                    userid:userId,
                    lockno:''
                    ,srhdate1:ssdate
                    ,srhdate2:sedate             
                    ,textsrch:tsearch
                    ,textref:tref
                    ,sostatus:sstatus
            }
        };
		xpand2div(pdata,'sorderDiv')
	}
   catch(ex){alert(ex.message);}
}

 

/***  

<style>
    .table tbody tr:hover {
       background-color:silver;
       cursor:pointer ;
    }

   
    
    #bg-text
    {
        display:inline-block;
        text-align:center;
        z-index:0;
        position: absolute;   
        top: 10%;         
        left: 30%;    
        opacity:40%;
        color:lightgrey;        
        font-size:120px;
        transform:rotate(300deg);
        -webkit-transform:rotate(350deg);
        text-shadow: 0 0 2px rgb(94, 93, 93);
    }


    .loading-overlay {
        display: none;
        background: rgba(209, 204, 204, 0.7);
        position: fixed;
        width: 100%;
        height: 100%;
        z-index: 5;
        top: 0;
    }
    
    .loading-overlay-image-container {
        display: none;
        position: fixed;
        z-index: 7;
        top: 50%;
        left: 50%;
        transform: translate( -50%, -50% );
    }
    
    .loading-overlay-img {
        width: 200px;
        height: 200px;
        border-radius: 5px;
    }

</style>
  
<div class='container-fluid' >
<div class='row'>
    <div class='col col-md-12'>     
     <div id ='sorderDiv'>

     </div>
    </div>    
</div>

<div class='row'  style='display:none;'>
    <div class='col col-md-12'>     
     <div id ='sorderRepDiv'>

     </div>
    </div>    
</div>

<div class="loading-overlay"></div>
<div class="loading-overlay-image-container">
    <img src="../images/loadsmall.gif" 
    class="loading-overlay-img"/>
</div>

 
<!-- Modal -->
 <div id="detilModal" class="modal fade" role="dialog" style="padding-right: 15px;">
    <div class="modal-lg modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="background-color: rgb(241,241, 241);" >
            <div class="modal-header" style='border-radius: 7px; background-color: white; height: auto; padding: 20px 20px; margin:0px 0px; '>
                <center><div id="demo"></div></center>

                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <span id='detailSpn'>
                    <h3></h3>
                </span>
            </div>
            <div class="modal-body p-modal">
                <div class='container-fluid p-modal'>
                    <div class='row'>
                        <div class='col-md-12'>                            
                            <div id ='detailDiv'>
                                Detail to be put here
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
            
        </div>
        </div>
    </div>

</div>

//END ******/