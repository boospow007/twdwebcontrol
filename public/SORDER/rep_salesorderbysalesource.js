 /// SORDER
 var wait = "<center><img src='../images/wait.gif' ></center>";
 var loading = "<center><img src='../images/loading.gif' ></center>";
 var storeCode = '';
 var refStatus = '';
 var saleSource = '';

 function pageReady() {
     storeCode = readCookie("WDFSTORE");
     pmPerfUserid = readCookie('WUSERID');

     if (profileList.indexOf('0070') > -1) {} else {
         alert('คุณไม่มีสิทธิ์ใช้งานเมนู Sales Order Report By Salesource');
         location.assign("/report");
     }

     var pdata = {
         patname: 'SORDER/rep_sorderbysalesource.txt',
         para: {
             stc: userStore,
             store: storeCode,
             userid: pmPerfUserid,
             lockno: '',
             srhdate1: '',
             srhdate2: '',
             textref: '',
             salesource: '',
             groupstc: 'All'  
         }
     };
     xpand2div(pdata, 'sorderDiv')
 }

 function xpand2div(pat, div) {
     try {
         ajaxXpandFile(pat, function (isok, ret) {
             if (isok) {
                 $('#' + div).html(ret);
                 $('.form_datetime').datepicker({
                    format: "yyyymmdd",
                    todayBtn: true,
                    language: "th",
                    orientation: "bottom left",
                    autoclose: true,
                    todayHighlight: true
                 });
                 $('.form_datetime').datepicker('update', new Date());
                 $('#orderstoregroup, #sel-salesource').multiselect({
                     buttonWidth: '100%',
                     enableFiltering: true,
                     includeSelectAllOption: true,
                     inheritClass: true,
                     nonSelectedText: 'กรุณาเลือก',
                     maxHeight: 400
                 });
                 setNumericFormatDiv(div);

                 if (div == 'sorderDiv') sorderReady();
                 // After Load --> Gen barcode and check can this ref void
                 setTimeout(afterLoadSO(), 500);

             } else alert('Ajax call Error')
         });
     } catch (ex) {
         alert(ex.message)
     }
 }

 function afterLoadSO() {
     profileList = readCookie('WUPROFILE');
     $('#void_button').hide();
 }

 function searchSorder() {
     try {
         storeCode = $('#sel-orderstore').val();
         userId = readCookie('WUSERID');
         saleSource = $('#sel-salesource').val();

         var ssdate = $('#sdate-cf').val();
         var sedate = $('#edate-cf').val();
         var tref = $('#srh-smartref').val();


         $('#sorderDiv').html(waitgif);
         //--------Create Pattern inside 

         var pdata = {
             patname: 'SORDER/rep_sorderbysalesource.txt',
             para: {
                 stc: userStore,
                 store: storeCode,
                 userid: userId,
                 lockno: '',
                 srhdate1: ssdate,
                 srhdate2: sedate,
                 textref: tref,
                 salesource: saleSource
             }
         };
         xpand2div(pdata, 'sorderDiv')
     } catch (ex) {
         alert(ex.message);
     }
 }



 /***  

 <style>
     .table tbody tr:hover {
        background-color:silver;
        cursor:pointer ;
     }

    
     
     #bg-text
     {
         display:inline-block;
         text-align:center;
         z-index:0;
         position: absolute;   
         top: 10%;         
         left: 30%;    
         opacity:40%;
         color:lightgrey;        
         font-size:120px;
         transform:rotate(300deg);
         -webkit-transform:rotate(350deg);
         text-shadow: 0 0 2px rgb(94, 93, 93);
     }


     .loading-overlay {
         display: none;
         background: rgba(209, 204, 204, 0.7);
         position: fixed;
         width: 100%;
         height: 100%;
         z-index: 5;
         top: 0;
     }
     
     .loading-overlay-image-container {
         display: none;
         position: fixed;
         z-index: 7;
         top: 50%;
         left: 50%;
         transform: translate( -50%, -50% );
     }
     
     .loading-overlay-img {
         width: 200px;
         height: 200px;
         border-radius: 5px;
     }

 </style>
   
 <div class='container-fluid' >
 <div class='row'>
     <div class='col col-md-12'>     
      <div id ='sorderDiv'>

      </div>
     </div>    
 </div>

 <div class='row'  style='display:none;'>
     <div class='col col-md-12'>     
      <div id ='sorderRepDiv'>

      </div>
     </div>    
 </div>

 <div class="loading-overlay"></div>
 <div class="loading-overlay-image-container">
     <img src="../images/loadsmall.gif" 
     class="loading-overlay-img"/>
 </div>

  
 <!-- Modal -->
  <div id="detilModal" class="modal fade" role="dialog" style="padding-right: 15px;">
     <div class="modal-lg modal-dialog">
         <!-- Modal content-->
         <div class="modal-content" style="background-color: rgb(241,241, 241);" >
             <div class="modal-header" style='border-radius: 7px; background-color: white; height: auto; padding: 20px 20px; margin:0px 0px; '>
                 <center><div id="demo"></div></center>

                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <span id='detailSpn'>
                     <h3></h3>
                 </span>
             </div>
             <div class="modal-body p-modal">
                 <div class='container-fluid p-modal'>
                     <div class='row'>
                         <div class='col-md-12'>                            
                             <div id ='detailDiv'>
                                 Detail to be put here
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer"></div>
             
         </div>
         </div>
     </div>

 </div>

 //END ******/