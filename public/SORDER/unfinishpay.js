var wait = "<img src='../images/wait.gif' >";
var vertendor = "QRKB";

function pageReady() {

	if (profileList.indexOf('0042') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Unfinished Paid');
		location.assign("/");
  }

  $(function () {
    $("#sdate").datepicker({
      format: "yyyymmdd",
      todayBtn: true,
      language: "th",
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      //,endDate: '0d'
    });

    $("#edate").datepicker({
      format: "yyyymmdd",
      todayBtn: true,
      language: "th",
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      //,endDate: '0d'
    });
  });

  $("#orderstore").multiselect({
    buttonWidth: "100%",
    enableFiltering: true,
    includeSelectAllOption: true,
    inheritClass: true,
    nonSelectedText: "เลือกสาขา",
    maxHeight: 400,
  });

  var tb = $("#sorderTab").DataTable({
    bPaginate: false,
    bSort: true,
    bFilter: false,
    bInfo: false,
  });
  //Sort by columns  and redraw
  tb
    //.order([0, 'asc'])
    .draw();

  $("#sorderTab tbody").on("click", "tr", function () {
    if ($(this).hasClass("selected")) {
      $(this).removeClass("selected");
    } else {
      tb.$("tr.selected").removeClass("selected");
      $(this).addClass("selected");
    }
  });

  var beginref = $("#sorderTab tr:eq(1) td:eq(0)").text().trim();
  if (beginref == "No data available in table" || beginref == "") {
    $("#sorderRefContainer").hide();
    return;
  }
  $("#sorderRefContainer").show();
}

function searchSorder() {
  try {
    // storeCode = $("#orderstore").val();
    var stccookie = readCookie("WDFSTORE");
    storeCode = $("#orderstore").val() == null ? stccookie : $("#orderstore").val();
    userId = readCookie("WUSERID");

    var ssdate = $("#sdate").val();
    var sedate = $("#edate").val();
    var sstatus = $("#orderstatus").val();
    var tsearch = $("#smartsrch").val();
    var tref = $("#smartref").val();

    $("#mainDiv").html(waitgif);
    //--------Create Pattern inside

    var pdata = {
      patname: "SORDER/unfinishpay.js",
      para: {
        stc: userStore,
        store: storeCode,
        userid: userId,
        lockno: "",
        srhdate: ssdate,
        srhenddate: sedate,
        textsrch: tsearch,
        textref: tref,
        sostatus: sstatus,
      },
    };
    xpand2div(pdata, "mainDiv");
  } catch (ex) {
    alert(ex.message);
  }
}

function xpand2div(pat, div) {
  try {
    ajaxXpandFile(pat, function (isok, ret) {
      if (isok) {
        $("#" + div).html(ret);
        setNumericFormatDiv(div);

        if (div == "mainDiv") pageReady();
      } else alert("Ajax call Error");
    });
  } catch (ex) {
    alert(ex.message);
  }
}

var currentref = "";
var cntQRRef = 0;
var salesource = '';
function showUFdetail(r, s, t, ss) {
  try {
    var ref = r;
    salesource = ss;
    currentref = ref.trim();

    var stc = s.trim();
    var tndr = t.trim();
    vertendor = tndr;

    ref = ref.trim();
    $("#detailSpn").html("<h4>Sales Order #" + ref + "</h4>");
    $("#detailDiv").html(wait);
    $("#verifyDiv").html("");

    var pname = "SORDER/unpaiditem.txt";
    var pdata = {
      patname: pname,
      para: {
        ref: ref,
        stc: stc
      }
    };
    ajaxXpandFile(pdata, function (isok, ret) {
      if (isok) {
        $("#detailDiv").html(ret);
        expandAllPayment(ref);
        setNumericFormatDiv("detailDiv");
        // if(div=='sorderDiv') sorderReady();
      } else alert("Ajax call Error");
    });
  } catch (ex) {
    alert(ex.message);
  }
}

function expandAllPayment(ref) {
  var qry = "SELECT PARA FROM CLOUDPOS..LOGKBPAYMENT where ref = '" + ref + "' ORDER BY RECID DESC";
  ajaxGetQuery(qry, function (isok, ret) {
    if (isok) {
      var allQRItms = JSON.parse(ret);
      cntQRRef = allQRItms.dbrows;
      for (i = 0; i < cntQRRef; i++) {
        $('#all-payment').append(`<div class='well' id='paymentRef${i + 1}'>
                                  <h4>Payment#${i + 1}</h4>
                                  <table class='dtable' id ='paymentTab${i + 1}'>
                                  </table>
                                  </div>`);
      }
    } else {
      alert(ret.dbmessage);
    }

    var qry2 = "Select * from OPENJSON((Select jsnpayment from CLOUDPOS..SORDER where ref = '" + ref + "'))";
    ajaxGetQuery(qry2, function (isok, ret) {
      if (isok) {
        var sorderItms = JSON.parse(ret);
        for (var i = 0; i < cntQRRef; i++) {
          var logQRItm = JSON.parse(allQRItms.dbitems[i].PARA);
          var createQRDt = new Date(logQRItm.created.substring(0, 4) + '-' +
            logQRItm.created.substring(4, 6) + '-' +
            logQRItm.created.substring(6, 8) + ' ' +
            logQRItm.created.substring(8, 10) + ':' +
            logQRItm.created.substring(10, 12) + ':' +
            logQRItm.created.substring(12, 14));
          for (var j = 0; j < sorderItms.dbrows; j++) {
            if (sorderItms.dbitems[j].key == "amount") {
              $('#paymentTab' + (i + 1)).append(`<tr><td>${sorderItms.dbitems[j].key}</td><td>${Number(parseFloat(logQRItm.amount).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2})}</td></tr>`);
            } else if (sorderItms.dbitems[j].key == "order_id") {
              $('#paymentTab' + (i + 1)).append(`<tr><td>${sorderItms.dbitems[j].key}</td><td>${logQRItm.id}</td></tr>`);
            } /*else if (sorderItms.dbitems[j].key == "ondate") {
              $('#paymentTab' + (i + 1)).append(`<tr><td>${sorderItms.dbitems[j].key}</td><td>${createQRDt.toLocaleString('en')}</td></tr>`);
            }*/ else {
              $('#paymentTab' + (i + 1)).append(`<tr><td>${sorderItms.dbitems[j].key}</td><td>${sorderItms.dbitems[j].value}</td></tr>`);
            }
          }
        }
      } else {
        alert(ret.dbmessage);
      }
    })
  })
}

async function verifytobank() {
  var orderid = "";
  for (var i = 0; i < cntQRRef; i++) {
    $("#paymentTab" + (i + 1) + " tr:gt(0)").each(function () {
      var ky = $(this).find("td:eq(0)").text();
      ky = ky.trim();
      if (ky == "order_id") {
        orderid = ky = $(this).find("td:eq(1)").text();
      }
    });
    // console.log(orderid);
    if (vertendor == "2C2P") {
      verify2c2p(orderid);
      // return;
      continue;
    }
    /*
        curl -X GET https://dev-kpaymentgatewayservices.kasikornbank.com/qr/v2/qr/chrg_prod_12345678 \
        -H "x-api-key : skey_prod_41Bbw6At8dJjVyKV3ZaXghhLpRro5oAtR   
  
        >>>>>Must Have Headr >>>>
        */

    var host = location.hostname;
    //alert(host);
    var url = "http://10.77.3.11:99/KBANK/kbverify/" + currentref + "/" + orderid;//"https://" + host + ":436/KBANK/kbverify/" + currentref + "/" + orderid;

    if (salesource.includes('E1')) {
      url = "http://10.77.3.12:89/KBANK/kbverify/" + currentref + "/" + orderid;
    }
    //var url ="https://eordering.thaiwatsadu.com:436/KBANK/kbverify/"+currentref +"/"+orderid;

    $("#verifyDiv").html(url);

    var result = await verify(url);
    
    if(result.includes("Status success")) {
      break;
    }
    
  }
}

async function verify(url){
  return new Promise(res => {
    $.get(url, function (ret) {
      $("#verifyDiv").html(ret);
      res(ret)
    });
  })
}

function verify2c2p(sid) {
  alert("Payment by 2C2P is not verifyable by Invoice No.");
  return;
}

/***  
$HDR
<style>
 div , table {  	 
        font-size: 12px ;	 
    }
.table tbody tr:hover {
       background-color:silver;
       cursor:pointer ;
    }
 </style>

<div class='container-fluid' >

  <h4>Unfinished Paid</h4>
 
<div class='row'>
        <div class='col-md-4' >
				สาขา <br>                  
				<select id='orderstore' multiple>                  
					#sbranch
				</select>
		</div>
		<div class='col-md-3' >
			ค้นหาลูกค้า หรือ เบอร์โทร  
			<input type='text'  id='smartsrch'  
				class='form-control' 				
                autocomplete="off"				
				placeholder='ชื่อลูกค้า หรือ เบอร์โทรศัพท์'
				maxlength='100' value ='' >
		</div>
		<div class='col-md-3' >
			เลขที่คำสั่งซื้อ
			<input type='text'  id='smartref'  
				class='form-control' 
				autocomplete="off"				
				placeholder='เลขที่คำสั่งซื้อ'
				maxlength='50' value ='' >
		</div>
        <div class='col-md-2' style='display:none;' >
				สถานภาพ <br>                  
				<select class='form-control' id='orderstatus' >                    
					#sstatus
				</select>
		</div>

	</div>    
	<div class='row'>
			<div class='col-md-2' >
				วันที่สั่งซื้อเริ่มต้น <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="sdate" name ="sdate"  
				onFocus='this.select();' 
				autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
            <div class='col-md-2' >
				วันที่สั่งซื้อสิ้นสุด <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="edate" name ="edate"  
				onFocus='this.select();' 
				autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
			
             <div class='col-md-8' >
                <br>
				<a href="javascript:searchSorder()" 
                class="btn btn-info btn-sm">&nbsp;&nbsp;&nbsp;แสดง&nbsp;&nbsp;&nbsp;</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                
                <div id='loadingDiv' style='display:inline-block;position:absolute; 
                    margin-left:10px; padding-left:10px;margin-top:2px; '>  </div>
            </div>

			 			 
    </div>
<br>
<div class='row'>
<div class='col col-md-5' style='height:800px;overflow:scroll'>
 <table id ='sorderTab' class='table'>
<thead>
<th>Store<th>REF<th>SALESOURCE</th><th>Order date<th>Tendor<th>Status<th>OnDate
</thead>
$SQL

exec CLOUDPOS..cs_ListUnfinishPaid '@srhdate','@srhenddate','@textsrch','@store','@textref','@sostatus','@userid'
                                    
$BDY
<tr onclick="javascript:showUFdetail('^ref^','^stcode^','^tdr^','^SALESOURCE^')">
<td>^stcode^
<td>^ref^
<td>^SALESOURCE^
<td>^orderdate^
<td>^tdr^
<td>^pst^
<td>^ondt^
$END
</tbody>
</table>
 
 </div>

    <div class='col col-md-7'>        
            <div id ='detailDiv'>
            </div>        
    <div>
      
    </div>
</div>

</div>
 

$TAB
|XID #status
|HDR
Remark:
|SQL
SELECT * from CLOUDPOS..SCSTATNAME
where ST<>'' 
|BDY
[:00 :01] 
|END


$TAB 
|XID #sstatus
|HDR
|SQL 
   exec CLOUDPOS..cs_loadSysStatus  
|BDY
<option value=':00'>:00 :01</option>
|END



$TAB 
|XID #sbranch
|HDR
|SQL 
   exec CLOUDPOS..cs_loadStoreByUser '@userid', '@stc'     
|BDY
<option value=':00'>:00 :01</option>
|END

//END **/