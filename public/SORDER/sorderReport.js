/**
 * Handle loading overlays
 *
 * @author Justin Stolpe
 */
var loader = {
    /**
     * Initialize our loading overlays for use
     *
     * @params void
     *
     * @return void
     */
    initialize: function () {
        var html =
            '<div class="loading-overlay"></div>' +
            '<div class="loading-overlay-image-container">' +
            '<img src="../images/loadsmall.gif" class="loading-overlay-img"/>' +
            '</div>';

        // append our html to the DOM body
        $('body').append(html);
    },

    /**
     * Show the loading overlay
     *
     * @params void
     *
     * @return void
     */
    showLoader: function () {
        jQuery('.loading-overlay').show();
        jQuery('.loading-overlay-image-container').show();
    },

    /**
     * Hide the loading overlay
     *
     * @params void
     *
     * @return void
     */
    hideLoader: function () {
        jQuery('.loading-overlay').hide();
        jQuery('.loading-overlay-image-container').hide();
    }
}

var mode, ssdate, sedate, sstatus, tsearch, tref, salesource, stcgroup = "";
var currentDate = new Date();
var stccookie;

function getCondition() {
    stccookie = readCookie("WDFSTORE");
    storeCode = $('#orderstore').val() || stccookie;
    pmPerfUserid = readCookie('WUSERID');
    ssdate = $('#sdate').val().trim();
    sedate = $('#edate').val().trim();
    sstatus = $('#orderstatus').val();
    tsearch = $('#smartsrch').val().trim();
    tref = $('#smartref').val().trim();
    salesource = $('#sel-salesource').val() || "";
    stcgroup = $('#orderstoregroup').val() || "All"

    ssdate = (ssdate == '') ? moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0)).format("YYYYMMDD HH:mm:ss") : ssdate + " 00:00:00";
    sedate = (sedate == '') ? moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 23, 59, 59)).format("YYYYMMDD HH:mm:ss") : sedate + " 23:59:59";
}

function getConditionReportSalesource() {
    stccookie = readCookie("WDFSTORE");
    storeCode = $('#sel-orderstore').val() || stccookie;
    pmPerfUserid = readCookie('WUSERID');
    ssdate = $('#sdate-cf').val().trim();
    sedate = $('#edate-cf').val().trim();
    tref = $('#srh-smartref').val().trim();
    salesource = $('#sel-salesource').val() || "";
    stcgroup = $('#orderstoregroup').val() || "All"

    ssdate = (ssdate == '') ? moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0)).format("YYYYMMDD HH:mm:ss") : ssdate + " 00:00:00";
    sedate = (sedate == '') ? moment(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 23, 59, 59)).format("YYYYMMDD HH:mm:ss") : sedate + " 23:59:59";
}


function reportSorderExportSummary() {
    try {
        //storeCode=readCookie("WDFSTORE");
        getCondition();
        var mode = 'S';

        var sql = ` exec CLOUDPOS..Rep_ExportSorder_Control '${mode}','${ssdate}','${sedate}','${tsearch}','${storeCode}','${salesource}','${tref}','${sstatus}','${pmPerfUserid}','${stcgroup}'`;
        //console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order Summary Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}

///////////////////////////////////////////////////////////////////
function reportSorderExportDetail() {
    try {
        //storeCode=readCookie("WDFSTORE");
        getCondition();
        var mode = 'D';

        var sql = ` exec CLOUDPOS..Rep_ExportSorder_Control '${mode}','${ssdate}','${sedate}','${tsearch}','${storeCode}','${salesource}','${tref}','${sstatus}','${pmPerfUserid}','${stcgroup}'`;
        //console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order Detail Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}
///////////////////////////////////////////////////////////////////
function reportCheckerExport() {
    try {
        //storeCode=readCookie("WDFSTORE");
        getCondition();
        var mode = 'C';

        var sql = ` exec CLOUDPOS..Rep_ExportSorder_Control '${mode}','${ssdate}','${sedate}','${tsearch}','${storeCode}','${salesource}','${tref}','${sstatus}','${pmPerfUserid}','${stcgroup}'`;
        //console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order Checker Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}

function sorderBySalesourceSummary(){
    try {
        getConditionReportSalesource();
        var mode = 'S';

        var sql = ` exec CLOUDPOS..Rep_ExportSorderBySalesource '${mode}','${ssdate}','${sedate}','${storeCode}','${salesource}','${tref}','${pmPerfUserid}','${stcgroup}'`;
        // console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order By Salesource Summary Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}

function sorderBySalesourceDetail() {
    try {
        getConditionReportSalesource();
        var mode = 'D';

        var sql = ` exec CLOUDPOS..Rep_ExportSorderBySalesource '${mode}','${ssdate}','${sedate}','${storeCode}','${salesource}','${tref}','${pmPerfUserid}','${stcgroup}'`;
        //console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order By Salesource Detail Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}

function sorderBySalesourceChecker() {
    try {
        getConditionReportSalesource();
        var mode = 'C';

        var sql = ` exec CLOUDPOS..Rep_ExportSorderBySalesource '${mode}','${ssdate}','${sedate}','${storeCode}','${salesource}','${tref}','${pmPerfUserid}','${stcgroup}'`;
        //console.log(sql);

        loader.showLoader();
        ajaxGetQuery(sql, (isok, resp) => {
            let jsn = JSON.parse(resp);
            if(!(isok && jsn.dbcode == 0)) {
                loader.hideLoader();
                alert(ex.message);
                return;
            }

            if(jsn.dbrows == 0) {
                loader.hideLoader();
                alert('ไม่พบข้อมูล');
                return;
            }

            var htm="<table id='myTableData' class='table'>";
            let header = '';
            let columns = [];
            Object.keys(jsn.dbfields[0]).map((itm, idx, arr) => {
                let column = {
                    data: itm
                }
                columns.push(column);
                header += `<th>${itm}</th>`;
                if(idx == arr.length - 1) {
                    header = `<thead><tr>${header}</tr></thead></table>`;
                    htm = htm + header;
                    $('#sorderRepDiv').html(htm);
                    
                    let obj = {
                        "data": jsn.dbitems,
                        "columns": columns,
                        dom: 'Bfrtip',
                        buttons: [{
                            extend: 'excel',
                            filename: 'Sales Order By Salesource Checker Report',
                            title: null,
                        }],
                    }
                    $('#myTableData').DataTable(obj);
                    $('.buttons-html5').click();
                    loader.hideLoader();
                }
            })
        })
    } catch (ex) {
        loader.hideLoader();
        alert(ex.message);
    }
}