//var waitgif="<image src='./images/wait.gif'>";

function pageReady() {
	if (profileList.indexOf('0038') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Bulk PIM By Date');
		location.assign("/");
  }

  $("#strtdate").datepicker({
    format: "yyyymmdd",
    todayBtn: true,
    language: "th",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true,
    //,endDate: '0d'
  });

  //---convert numeric format
  $(".numeric").each(function () {
    var val = $(this).text();
    val = val.replace(/,/g, "");
    $(this).css("align", "right");
    val = val.trim();
    if (val.length > 0) $(this).html(numericFormat(val));
  });

  $("#mainDiv").mouseleave(function () {
    $("#prodDiv").hide();
  });

  $("#dataTab tbody tr").each(function (idx) {
    $(this)
      .children()
      .first()
      .html(idx + 1);
  });
  var rows = $("#dataTab tbody tr").length;
  $("#datacnt").text(rows);

  $("#dataTab tbody tr").click(function (e) {
    $("#dataTab tbody tr").removeClass("highlighted");
    $(this).toggleClass("highlighted");
  });
}

function showDetailx(cobj) {
  var skc = $(cobj).fimd("td:eq(2)").text();
  url = "/DPAGE?" + btoa("fname=PRODUCT/pimbyskcode.js&skcode=" + skc);
  window.open(url, "DETAIL");
}

var nrow = 0;

function copy2Pim() {
  if (confirm("Transfer These UPFPim to CLD?") == 0) return;
  nrow = $("#dataTab tr:last").index();
  nrow += 1;
  try {
    xferRow(1);
  } catch (ex) {
    alert(ex.message);
  }
}

function xferRow(rwn) {
  if (rwn > nrow) return;
  try {
    var thisobj = $("#dataTab tr:eq(" + rwn + ")");
    var skc = $(thisobj).find("td:eq(2)").text();
    var url = "/XFER/PIM/" + skc;
    $.get(url, function (data) {
      $(thisobj).find("td:eq(9)").html(data);
      rwn += 1;
      xferRow(rwn);
    });
  } catch (ex) {
    alert(ex.message);
  }
}

function copyImage() {
  swal({
      title: "Transfer",
      text: "These images to CLD?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: true,
    },
    function () {
      nrow = $("#dataTab tr:last").index();
      nrow += 1;
      try {
        xferImage(1);
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function copyProductName() {
  swal({
      title: "Transfer",
      text: "These product names to CLD?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: true,
    },
    function () {
      nrow = $("#dataTab tr:last").index();
      nrow += 1;
      try {
        xferName(1);
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function hideCountRow() {
  $("#countrowupdate").hide();
}

function xferImage(rwn) {
  if (rwn > nrow) {
    $("#countrowupdate").html(
      '<p style="padding: 5px 15px; font-weight:bold; margin: 0px; font-size:15px;">Successfully Updated!</p>'
    );
    setTimeout(hideCountRow, 3000);
    return;
  }
  $("#countrowupdate").show();
  $("#countrowupdate").html('<p style="padding: 5px 15px; font-weight:bold; margin: 0px; font-size:20px;">Row updating: ' + rwn + "</p>");
  try {
    var thisobj = $("#dataTab tr:eq(" + rwn + ")");
    var imgurl = $(thisobj).find("td:eq(5)").text().trim();
    var skc = $(thisobj).find("td:eq(2)").text().trim();
    var enrich = $(thisobj).find("td:eq(10)").text().trim();
    var lupd = ($(thisobj).find("td:eq(11)").text().trim()).replace(/\./g,':');
    var url = imgurl.split("|");
    if (url.length <= 1) {
      $(thisobj).find("td:eq(9)").html("NO Image");
      xferImage(++rwn);
    } else {
      //var iurl ='http://203.151.83.183/TWDPIM/web/'+ url[1];
      var iurl = "https://pim.thaiwatsadu.com/TWDPIM/web/" + url[1];

      var surl = `?skc=${skc}&imgurl=${iurl}&lupd=${lupd}&enrich=${enrich}`;
      var url = "/XFER/IMAGE/" + surl;
      $.get(url, function (data) {
        $(thisobj).find("td:eq(9)").html(data);
        xferImage(++rwn);
      });
    }
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function xferName(rwn) {
  if (rwn > nrow) {
    $("#countrowupdate").html(
      '<p style="padding: 5px 15px; font-weight:bold; margin: 0px; font-size:15px;">Successfully Updated!</p>'
    );
    setTimeout(hideCountRow, 3000);
    return;
  }
  $("#countrowupdate").show();
  $("#countrowupdate").html(
    '<p style="padding: 5px 15px; font-weight:bold; margin: 0px; font-size:20px;">Row updating: ' +
    rwn +
    "</p>"
  );
  try {
    var thisobj = $("#dataTab tr:eq(" + rwn + ")");
    var skc = $(thisobj).find("td:eq(2)").text().trim();
    var prodnme = encodeURIComponent($(thisobj).find("td:eq(3)").text().trim());
    var brnd = encodeURIComponent($(thisobj).find("td:eq(4)").text().trim());
    var lstApvDt = $(thisobj).find("td:eq(7)").text().trim();
    var enrich = encodeURIComponent($(thisobj).find("td:eq(10)").text().trim());
    var showOnWeb = encodeURIComponent($(thisobj).find("td:eq(12)").text().trim());
    if(lstApvDt == '') {
      $(thisobj).find("td:eq(9)").html('Can\'t update. This SKU didn\'t approve yet.');
      xferName(++rwn);
    }
    else {
      var itms = {
        name: prodnme,
        brand: brnd,
        skcode: skc,
        enrich: enrich,
        showonweb: showOnWeb
      };
      var surl = `?skcode=${itms.skcode}&name=${itms.name}&brand=${itms.brand}&enrich=${itms.enrich}&showonweb=${itms.showonweb}`;
      var url = "/XFER/UPDEPROD/" + surl;
      $.get(url, function (data) {
        $(thisobj).find("td:eq(9)").html(data);
        xferName(++rwn);
      });
    }
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function xferThis(obj) {
  try {
    var skc = $(obj).find("td:eq(2)").text();
    alert("Xfer " + skc);
    var url = "/XFER/PIM/" + skc;
    $.get(url, function (data) {
      alert(data);
    });
  } catch (ex) {
    alert(ex.message);
  }
}
//============
function hideDiv() {
  $("#prodDiv").hide();
}

var cobj = null;

function showDetail(cobj) {
  var skc = $(cobj).find("td:eq(2)").text().trim();
  var nme = encodeURIComponent($(cobj).find("td:eq(3)").text().trim());
  var brnd = encodeURIComponent($(cobj).find("td:eq(4)").text().trim());
  var lstApvDt = encodeURIComponent($(cobj).find("td:eq(7)").text().trim());
  var enrich =  encodeURIComponent($(cobj).find("td:eq(10)").text().trim());
  var lupd =  encodeURIComponent($(cobj).find("td:eq(11)").text().trim());
  var showOnWeb = encodeURIComponent($(cobj).find("td:eq(12)").text().trim());
  try {
    openDetail("fname=PRODUCT/pimbyskcode.js&skcode=" + skc + "&prodname=" + nme + "&brand=" + brnd + "&lstapvdt=" + lstApvDt + "&enrich=" + enrich + "&lupd=" + lupd + "&showonweb=" + showOnWeb);
  } catch (ex) {
    alert(ex.message);
  }
}
var cobj = null;

function menuOver(obj) {
  cobj = obj;
  var prc = $(obj).find("td:eq(8)").text().trim();
  var imgs = $(obj).find("td:eq(5)").text().trim();
  var nme = $(obj).find("td:eq(3)").text().trim();
  var skc = $(obj).find("td:eq(2)").text().trim();
  var brnd = $(obj).find("td:eq(4)").text().trim();
  var lstapvdt = $(obj).find("td:eq(7)").text().trim();
  var enrich = $(obj).find("td:eq(10)").text().trim();
  var lupd = $(obj).find("td:eq(11)").text().trim();
  var showOnWeb = $(obj).find("td:eq(12)").text().trim();
  var img = imgs.split("|")[1];
  skc = skc.trim();
  $("#isku").text(skc);
  $("#iname").text(nme);
  $("#ibrand").text(brnd);
  $("#ilstapvdt").text(lstapvdt);
  $('#ienrich').text(enrich);
  $('#ilupd').text(lupd);
  $('#ishowonweb').text(showOnWeb);

  $(".card-text").html(
    skc + ".jpg<br>" + nme + "<br><b>ราคา " + prc + " บาท</b>"
  );

  var url = "/B/" + skc;
  $.get(url, function (data) {
    $("#imgDiv").html(" <img src='" + data + "'  width='100%'  >");
  });

  var pos = $(obj).position();
  var mpos = $("#dispDiv").position();
  var tpos = $("#dataTab").position();
  $("#prodDiv").css("top", mpos.top + pos.top - 100);
  $("#prodDiv").css("left", mpos.left + pos.left);
  $("#prodDiv").show();
}

function showDetailprodDiv() {
  var skc = $("#isku").text().trim();
  var nme = encodeURIComponent($("#iname").text().trim());
  var brnd = encodeURIComponent($("#ibrand").text().trim());
  var enrich = encodeURIComponent($("#ienrich").text().trim());
  var lstApvDt = encodeURIComponent($("#ilstapvdt").text().trim());
  var lupd = encodeURIComponent($("#ilupd").text().trim());
  var showOnWeb = encodeURIComponent($("#ishowonweb").text().trim());
  try {
    openDetail(`fname=PRODUCT/pimbyskcode.js&skcode=${skc}&prodname=${nme}&brand=${brnd}&lstapvdt=${lstApvDt}&enrich=${enrich}&lupd=${lupd}&showonweb=${showOnWeb}`);
  } catch (ex) {
    alert(ex.message);
  }
}

function showByCondition() {
  try {
    var ttm = $("#strtdate").val();
    var userid = readCookie("WUSERID");

    $("#mainDiv").html(waitgif);
    //--------Create Pattern inside
    var pat = "./public/PRODUCT/pimbulknewprod.js";
    pat = "PRODUCT/pimbulknewprod.js";

    var pdata = {
      patname: pat,
      para: {
        store: storeCode,
        cashier: userid,
        lockno: lockNumber,
        srhdate: ttm,
        srhtime1: "",
        srhtime2: "",
        textsrch: "",
      },
    };
    ajaxXpandFile(pdata, function (bool, ret) {
      if (ret.substring(0, 3) == "ERR") ret = syserror + ret;

      $("#mainDiv").html(ret);
      setNumericFormatDiv("mainDiv"); //set Numeric format on display --source in common.js
      //-----Call Startpage
      try {
        pageReady();
      } catch (ex) {
        console.log(ex.message);
      }
    });
  } catch (Ex) {
    alert(ex.message);
  }
}

/**********
$HDR
<style>

.wraptd {
white-space: nowrap; 
	text-overflow:ellipsis; 
	overflow: hidden;
	max-width:200px;
}
#dataTab tr:hover{
	background-color:silver;
	cursor:pointer;
}
#dataTab tbody td {
	font-size:12px;	 
}

.highlighted {
    background: #f4e1d2;
}
 


.card:hover{
	background-color:silver;
	cursor:pointer;
}

div.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
}

.card-float{
	border:1px solid lightgray; 
	position:fixed; 
	bottom:20px; 
	right:20px;
	float:right; 
	width:215px;
	border-radius:10px;
	background-color: lightgray;
	box-shadow: 4px 4px;
}



</style>
<div class='container-fulid'>
<br>

<div class='row'>  
<div  id ='mainDiv' class='col col-md-12'>
<h3>Bulk PIM by Date</h3>
<div class='row'>
			<div class='col-md-4' >
				เลือกวันที่ <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="strtdate" name ="strtdate"  
				onFocus='this.select();' 
				autocomplete='false' 
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>		 
			<div class='col-md-4' >
				<br>
				<a href="javascript:showByCondition()" class="btn btn-success btn-sm">Confirm</a>
			</div>	 
</div>

Parameter: <label id='theday'> วันที่: #theday  </label> / Data <label id='datacnt'>  </label> Row(s)



<div class='col col-md-12 pull-right' > 
	//<a href ="javascript:copy2Pim()" class='btn btn-primary pull-right'>Copy to PIM</a> 
	<a href ="javascript:copyProductName()" class='btn btn-primary pull-right' style='margin-left: 15px;'>Copy Product Name to DB</a>	
	<a href ="javascript:copyImage()" class='btn btn-primary pull-right'>Copy Image to DB</a>
</div><br><br><br>


<div id ='dispDiv'>

<table id ='dataTab' class='table'>
 <thead><th><th>Prcode<th>SkUcode<th>Name<th>Brand<th>image<th>LastUpdate<th>LastApvDate<th>Ret Price<th>Rem<th style='display: none;'>Enrich<th style='display: none;'>LUPD<th style='display: none;'>ShowOnWeb</thead>
 <tbody> 

$SQL
use.MYSQL^
select prCode
,product.skCode as skCode
,prNameTH
,brand
,images
,DATE_FORMAT(product.lastupdate, "%Y-%m-%d %H.%i.%s") as lastupdate
,DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")   as pim_apvdate  
,twdNormalPrice as price
,enrich
,case when product.pim_apvdate > product.lastupdate  then DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")  
      else DATE_FORMAT(product.lastupdate, "%Y-%m-%d %H.%i.%s") end as lupd
,showOnWeb
from twdpim.product  join twdpim.price on product.skcode =price.skCode
where  ( ( product.pim_apvdate>='#startdate' and product.pim_apvdate < date_add( '#startdate',interval 1 day))
  or  ( product.lastUpdate >='#startdate' and product.lastUpdate < date_add( '#startdate',interval 1 day)))
order by product.pim_apvdate
// LIMIT 100
$BDY
<tr onmouseover ="javascript:menuOver(this)"  onclick ="javascript:showDetail(this)"> 
<td> 
<td>:00
<td>:01
<td class='wraptd'>:02
<td class='wraptd'>:03
<td  class='wraptd'>:04  
<td>:05
<td>:06
<td align='right' class='numeric'>:07
<td>
<td style='display: none'>:08
<td style='display: none'>:09
<td style='display: none'>:10
</tr>

$END
</tbody>
</table>

</div>
Note :Data from upfrontpim.net
<br><br><br><br><br><br><br><br><br> 
</div>


 <div class="card" style=" width:24rem;display:none;z-index:4;
  		background-color:white;position:absolute;"
		id='prodDiv' onclick="javascript:showDetailprodDiv()" > 
		<span id="isku"></span>
		<span id="iname" hidden></span>
    <span id="ibrand" hidden></span>
    <span id="ilstapvdt" hidden></span>
    <span id='ienrich' hidden></span>
    <span id='ilupd' hidden></span>
    <span id='ishowonweb' hidden></span>
		<figure class="card card-product">
		<div id='imgDiv' class="card-img-top"> 
		</div>
	    <p class="card-text"></p>
 </div>

</div>

 <div class="card-float" id="countrowupdate" hidden>
 
 </div>
 
$xxSQL
 
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
//http://upfrontpim.net/TWDSERVICE/mysqlService.php^
//http://142.11.38.38/SQLSERVICE/curlService.php"
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
//command in musql
Select skCode,prCode,prNameTH,images,lastUpdate 
from twd.product LIMIT 100



$PRE

select [#tday]=convert(varchar(8),getdate(),8)
,[#startdate] = case when len('@srhdate')=0 
                   then convert(char(8),getdate(),112)
                   else    '@srhdate' end
,[#theday] = case when len('@srhdate')=0 
                   then 'Today'
                   else    '@srhdate' end
 
				   

//END****/