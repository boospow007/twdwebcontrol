 
$HDR
 <h3>Edit Foods and Drinks</h3> 
 <hr>
$SQL
if( '@rec'='NEW') 
	Select CATCODE='000',PRNAME='',BARCODE=dbo.getnewbarcode(),UNAME='',UPRICE=0,REMARK='',PRODID='NEW'
else
 	Select * from  POSPROD
	where PRODID='@rec'
$BDY
             <div style="padding:4px 50px;">
                 <form role="form" id="myForm" action="" method="POST">

                        <div class="form-group">
                         <label for="rectxt"><span class="glyphicon glyphicon-asterisk"></span>Record No.</label>
                         <input type="text" class="form-control"  id="rectxt"  disabled='true' value="^PRODID^">
                     </div>
                       <div class="form-group">
                         <label for="bartxt"><span class="glyphicon glyphicon-pencil"></span>BARCODE</label>
                          <input type="text" class="form-control"   id="bartxt" placeholder="Barcode" value="^BARCODE^">   
                     </div> 
                     <div class="form-group">
                         <label for="cdetxt"><span class="glyphicon glyphicon-pencil"></span>CATCODE</label>
                          <select class='form-control' id='cattxt'>
                          #category
                          </select>
                     </div>
                     
                     <div class="form-group">
                         <label for="nmetxt"><span class="glyphicon glyphicon-pencil"></span>PRODUCT NAME</label>
                         <input type="text" class="form-control"   id="nmetxt" placeholder="Name" value="^PRNAME^">
                     </div>
                     <div class="form-group">
                         <label for="unmtxt"><span class="glyphicon glyphicon-pencil"></span>Unit NAME</label>
                         <input type="text" class="form-control"   id="unmtxt" placeholder="Unit Name" value="^UNAME^">
                     </div>
                    <div class="form-group">
                         <label for="prctxt"><span class="glyphicon glyphicon-pencil"></span>Price </label>
                         <input type="text" class="form-control"   id="prctxt" placeholder="Unit Name" value="^UPRICE^">
                     </div>

                    <div class="form-group">
                         <label for="remtxt"><span class="glyphicon glyphicon-pencil"></span>Description</label>
                         <textarea class="form-control rounds-0" rows='5'   id="remtxt">^REMARK^"</textarea>
                     </div>

                     <button type="button" class="btn btn-success btn-block" onclick="confirmEnt()">
                         <span class="glyphicon glyphicon-ok"></span> Confirm
                     </button>
                                
                     <button type="button" class="btn btn-danger btn-block" onclick="cancelEnt()">
                         <span class="glyphicon glyphicon-remove"></span> Cancel
                     </button>  

                 </form>
 
$ERX
ERROR @error
$END

$TAB
|XID #category
|HDR
|SQL
SElect CatCODE,Catname
From CATEGORY
where RIGHT(CATCODE,2)>'00'
|BDY
<option value=':00'>:01</option>
|END

//END ***/