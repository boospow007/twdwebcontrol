//var waitgif="<image src='./images/wait.gif'>";

function pageReady() {
	if (profileList.indexOf('0036') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู New PIM Product');
		location.assign("/");
  }
  
  $("#mainDiv").mouseleave(function () {
    $("#prodDiv").hide();
  });
}

function showDetailx(cobj) {
  var skc = $(cobj).fimd("td:eq(1)").text();
  var stc = readCookie("WDFSTORE");
  url = "/DPAGE?" + btoa("fname=PRODUCT/pimbyskcode.js&skcode=" + skc + "&stcode=" + stc);
  window.open(url, "DETAIL");
}

var nrow = 0;

function copy2Pim() {
  swal({
      title: "Transfer",
      text: "These UPFPim to CLD?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: true,
    },
    function () {
      nrow = $("#dataTab tr:last").index();
      nrow += 1;
      try {
        xferRow(1);
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function xferRow(rwn) {
  if (rwn > nrow) return;
  try {
    var thisobj = $("#dataTab tr:eq(" + rwn + ")");
    var skc = $(thisobj).find("td:eq(1)").text();
    var url = "/XFER/PIM/" + skc;
    $.get(url, function (data) {
      $(thisobj).find("td:eq(8)").html(data);
      rwn += 1;
      xferRow(rwn);
    });
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function copyImage() {
  swal({
      title: "Transfer",
      text: "These images to CLD?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: true,
    },
    function () {
      nrow = $("#dataTab tr:last").index();
      nrow += 1;
      try {
        xferImage(1);
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function xferImage(rwn) {
  if (rwn > nrow) return;
  try {
    var thisobj = $("#dataTab tr:eq(" + rwn + ")");
    var imgurl = $(thisobj).find("td:eq(4)").text().trim();
    var skc = $(thisobj).find("td:eq(1)").text().trim();
    var url = imgurl.split("|");
    var lupd = ($(thisobj).find("td:eq(10)").text().trim()).replace(/\./g,':');
    var enrich = $(thisobj).find("td:eq(9)").text().trim();
    if (url.length <= 1) {
      $(thisobj).find("td:eq(8)").html("NO Image");
      xferImage(++rwn);
    } else {
      //var iurl ='http://203.151.83.183/TWDPIM/web/'+ url[1];
      var iurl = "https://pim.thaiwatsadu.com/TWDPIM/web/" + url[1];

      var surl = `?skc=${skc}&imgurl=${iurl}&lupd=${lupd}&enrich=${enrich}`;
      var url = "/XFER/IMAGE/" + surl;
      $.get(url, function (data) {
        $(thisobj).find("td:eq(8)").html(data);
        xferImage(++rwn);
      });
    }
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function xferThis(obj) {
  try {
    var skc = $(obj).find("td:eq(1)").text();
    alert("Xfer " + skc);
    var url = "/XFER/PIM/" + skc;
    $.get(url, function (data) {
      alert(data);
    });
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}
//============
function hideDiv() {
  $("#prodDiv").hide();
}

var cobj = null;

function xxshowDetailxx(cobj) {
  var skc = $(cobj).find("td:eq(1)").text();
  var stc = readCookie("WDFSTORE");

  try {
    openDetail("fname=PRODUCT/pimbyskcode.js&skcode=" + skc + "&stcode=" + stc);
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function showDetail() {
  var skc = $(cobj).find("td:eq(1)").text().trim();
  var stc = readCookie("WDFSTORE");
  var nme = encodeURIComponent($(cobj).find("td:eq(2)").text().trim());
  var brnd = encodeURIComponent($(cobj).find("td:eq(3)").text().trim());
  var lstapvdt = encodeURIComponent($(cobj).find("td:eq(6)").text().trim());
  var lupd = encodeURIComponent($(cobj).find("td:eq(10)").text().trim());
  var enrich = encodeURIComponent($(cobj).find("td:eq(9)").text().trim());
  var showonweb = encodeURIComponent($(cobj).find("td:eq(11)").text().trim());
  try {
    openDetail(`fname=PRODUCT/pimbyskcode.js&skcode=${skc}&stcode=${stc}&prodname=${nme}&brand=${brnd}&lstapvdt=${lstapvdt}&enrich=${enrich}&lupd=${lupd}&showonweb=${showonweb}`);
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

var cobj = null;

function menuOver(obj) {
  cobj = obj;
  var prc = $(obj).find("td:eq(7)").text().trim();
  var imgs = $(obj).find("td:eq(4)").text().trim();
  var nme = $(obj).find("td:eq(2)").text().trim();
  var skc = $(obj).find("td:eq(1)").text().trim();
  var img = imgs.split("|")[1];
  skc = skc.trim();
  $(".card-text").html(
    skc + ".jpg<br>" + nme + "<br><b>ราคา " + prc + " บาท</b>"
  );

  var url = "/B/" + skc;
  $.get(url, function (data) {
    $("#imgDiv").html(" <img src='" + data + "'  width='100%'  >");
  });

  var pos = $(obj).position();
  var mpos = $("#dispDiv").position();
  var tpos = $("#dataTab").position();
  $("#prodDiv").css("top", mpos.top + pos.top);
  $("#prodDiv").css("left", mpos.left + pos.left);
  $("#prodDiv").show();
}

/**********
$HDR

<style>

.wraptd {
white-space: nowrap; 
	text-overflow:ellipsis; 
	overflow: hidden;
	max-width:200px;
}
#dataTab tr:hover{
	background-color:silver;
	cursor:pointer;
}
.card:hover{
	background-color:silver;
	cursor:pointer;
}

div.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
}



</style>
<div class='container-fulid'>
<br>

<div class='row'>
 

<div class='col col-md-12'> 
<a href ="javascript:copy2Pim()" class='btn btn-primary'>Copy to PIM</a> 
<a href ="javascript:copyImage()" class='btn btn-primary'>Copy Image to DB</a> 
 </div>
 
 
<div  id ='mainDiv' class='col col-md-12'>
<h3>New PIM Product at  Upfront in last 5 days</h3>
 

<div id ='dispDiv'>

<table id ='dataTab' class='table'>
 <thead><th>Prcode<th>SkUcode<th>Name<th>Brand<th>image<th>LastUpdate<th>LastApvDate<th>Ret Price<th>Rem<th style='display: none;'>Enrich<th style='display: none;'>LUPD<th style='display: none;'>ShowOnWeb</thead>
$SQL
use.MYSQL^
Select prCode
,product.skCode as skCode
,prNameTH
,brand
,images
,DATE_FORMAT(product.lastupdate, "%Y-%m-%d %H.%i.%s") as lastupdate
,DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")   as pim_apvdate  
,twdNormalPrice as price
,enrich
,case when product.pim_apvdate > product.lastupdate  then DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")  
      else DATE_FORMAT(product.lastupdate, "%Y-%m-%d %H.%i.%s") end as lupd
,showOnWeb
from twdpim.product join twdpim.price on product.skcode =price.skCode
where ( product.pim_apvdate > DATE_ADD(CURDATE(), INTERVAL -5 DAY) 
or product.lastUpdate > DATE_ADD(CURDATE(), INTERVAL -5 DAY)  )
order by product.pim_apvdate desc, product.skCode
LIMIT 100;

$BDY
//<tr  onclick="javascript:xferThis(this)">
//<tr onclick ="javascript:showDetail(this)">
<tr onmouseover ="javascript:menuOver(this)">
<td>:00<td>:01<td class='wraptd'>:02
<td class='wraptd'>:03
<td  class='wraptd'>:04 
<td>:05<td>:06<td align='right'>:07<td><td style='display: none'>:08<td style='display: none'>:09<td style='display: none'>:10</tr>
$END
</table>

</div>
Note :Data from upfrontpim.net
</div>


 <div class="card" style=" width:24rem;display:none;z-index:4;
  		background-color:white;position:absolute;"
		id='prodDiv' onclick="javascript:showDetail()" > 
		<figure class="card card-product">
		<div id='imgDiv' class="card-img-top"> 
		</div>
	    <p class="card-text"></p>
 </div>

</div>
 
$xxSQL
 
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
//http://upfrontpim.net/TWDSERVICE/mysqlService.php^
//http://142.11.38.38/SQLSERVICE/curlService.php"
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
//command in musql
Select skCode,prCode,prNameTH,images,lastUpdate 
from twd.product LIMIT 100
//END****/