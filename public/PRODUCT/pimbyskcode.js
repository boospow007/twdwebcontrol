var imgsrc = "";
var imgsave = "";
var prodname = "";
var brand = "";
var lstapvdt = "";
var enrich = '';
var lupd= '';

function pageReady() {
  var imgf = $("#imgfile").text();
  imgfs = imgf.split("|");
  imgsrc = "https://pim.thaiwatsadu.com/TWDPIM/web/image/noimage.png";
  imgsave = "https://pim.thaiwatsadu.com/TWDPIM/web/image/noimage.png";
  if (imgfs.length > 0) {
    imgsrc = "https://pim.thaiwatsadu.com/TWDPIM/web/" + imgfs[1];
    imgsave = "https://pim.thaiwatsadu.com/TWDPIM/web/" + imgfs[1];
  }

  var decyptUrlParams = '?' + atob((window.location.search).replace('?', ''));
  var urlParams = new URLSearchParams(decyptUrlParams);
  prodname = urlParams.get("prodname");
  brand = urlParams.get("brand");
  lstapvdt = urlParams.get("lstapvdt");
  enrich = urlParams.get("enrich");
  lupd = urlParams.get("lupd").replace(/\./g,':');
  showonweb = urlParams.get("showonweb");
  console.log(prodname,brand,lstapvdt,enrich,lupd,showonweb)
  $("#pimage").prop("src", imgsrc);
}

function nextProd(cat, skc, opt) {
  var stc = readCookie("WDFSTORE");
  var sql = "use.MYSQL^";
  sql += ` select skcode,prNameTH,brand,enrich,
                  DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")   as pim_apvdate,
                  case when product.pim_apvdate > product.lastupdate  then DATE_FORMAT(product.pim_apvdate, "%Y-%m-%d %H.%i.%s")  
                       else DATE_FORMAT(product.lastupdate, "%Y-%m-%d %H.%i.%s") end as lupd,
                  showOnWeb  from twdpim.product`;
  sql += " Where catcode like '" + cat + "%' ";
  if (opt == 1) sql += " and skCode > " + skc + " order by skcode asc LIMIT 1 ";
  else sql += " and skCode < " + skc + " order by skcode desc LIMIT 1 ";
  // sql ="select top 1  skcode from CLOUDPOS..Posprod ";
  try {
    var imgw = "image/wait.gif";
    $("#pimage").prop("src", imgw);
    ajaxGetQuery(sql, function (bool, ret) {
      try {
        if (bool) {
          var jsn = JSON.parse(ret); //convert to array
          console.log(jsn);
          var ec = jsn.dbcode;
          if (ec > 0) {
            var msg = jsn.dbmessage;
            alert(msg);
            $("#pimage").prop("src", imgsrc);
            return;
          }
          console.log(jsn.dbitems);
          var itm = jsn.dbitems;
          /*
					if(itm==null){
						alert("No More Product");
						$('#pimage').prop('src',imgsrc);
						return;
						}
						*/
          var skc = jsn.dbitems[0].skcode;
          var productname = encodeURIComponent(jsn.dbitems[0].prNameTH);
          var productbrand = encodeURIComponent(jsn.dbitems[0].brand);
          var enrich = encodeURIComponent(jsn.dbitems[0].enrich);
          var lstapvdt = encodeURIComponent(jsn.dbitems[0].pim_apvdate);
          var lupd = encodeURIComponent(jsn.dbitems[0].lupd);
          var showonweb = encodeURIComponent(jsn.dbitems[0].showOnWeb);
          //alert(skc);
          url =
            `fname=PRODUCT/pimbyskcode.js&skcode=${skc}&stcode=${stc}&prodname=${productname}&brand=${productbrand}&lstapvdt=${lstapvdt}&enrich=${enrich}&lupd=${lupd}&showonweb=${showonweb}`;
          openDetail(url);
        }
      } catch (ex) {
        console.log(ret);
        swalAl("error", "Failed", "Ajax.." + ex.message);
      }
    });
  } catch (ex) {
    swalAl("error", "Failed", ex.message);
  }
}

function resizeImg(skc) {
  swal({
      title: "Transfer image?",
      text: skc,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: false,
    },
    function () {
      try {
        var pdata = {
          url: imgsave,
          skc: skc,
          lupd: lupd,
          enrich: enrich
        };
        var sUrl = "./IMAGE/TODB"; // for mssql

        $.ajax({
          type: "POST",
          url: sUrl,
          data: JSON.stringify(pdata),
          dataType: "text",
          contentType: "application/json; charset=UTF-8",
          success: function (ret) {
            //return is array of data in json
            if (ret == "image2DB Copy & Resize Image Successful") {
              swalAl("success", "Successful", ret);
              return;
            }
            swalAl("error", "Failed", ret);
            return;
          },
          error: function (e) {
            swalAl("error", "Failed", e.responseText);
          },
        });
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function updateEPROD(skcode) {
  swal({
      title: "Transfer product name?",
      text: prodname,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, update it!",
      closeOnConfirm: false,
    },
    function () {
      if(lstapvdt == '') {
        swalAl("error", "Failed", 'Can\'t update. This SKU didn\'t approve yet.');
        return;
      }
      try {
        var itms = {
          skcode: skcode,
          name: encodeURIComponent(prodname),
          brand: encodeURIComponent(brand),
          enrich: enrich,
          showonweb: showonweb
        };
        var surl = `?skcode=${itms.skcode}&name=${itms.name}&brand=${itms.brand}&enrich=${itms.enrich}&showonweb=${itms.showonweb}`;
        var url = "./XFER/UPDEPROD/" + surl;
        $.get(url, function (data) {
          if (data.includes("Copy Pim Name")) {
            swalAl("success", "Successful", data);
            return;
          }
          swalAl("error", "Failed", data);
          return;
        });
      } catch (ex) {
        swalAl("error", "Failed", ex.message);
      }
    }
  );
}

function swalAl(type, title, text) {
  swal({
      type: type,
      title: title,
      text: text,
      showConfirmButton: false,
      timer: 2000,
  });
}

/*****
<script>
var skcode ='@skcode';'
</script>
$HDR
<div class='container-fluid'>
 <div class='row border-all'> 
 <div class='row panel-form'>
	<div class='col col-md-12 card-article'>
	<center><h2>Product Information @skcode</h2></center>
	</div>
</div>
<style>
	.panel-form{
		margin-left:150px; margin-right:150px;
	}
	.border-all{
		background-color:lightgray; 
		border-radius:10px; 
		margin-top:1.25rem; 
		height:'100%'; 
		weight:'100%'
	}
	.card-article{
		background-color:white; 
		border-radius:10px; 
		box-shadow:0 1px 1px 0 rgba(0,0,0,.05); 
		margin-top:1.25rem;
	}
	.article{
		background-color:lightgray;
		padding:0.875rem;
		text-transform:capitalize;
		margin: 1.25rem 0px;
		border-radius:10px; 
	}
	.
</style>

$SQL
use.MYSQL^
Select prNameTH as prname
	, longDescTH as ldesp
	, shortDesc1TH as sdesp
	, attrContent  as attrb
	,images as pimg
	,left(catcode,6) as cat
	,skCode as skc
from twdpim.product 
where skCode=@skcode

$BDY
<div class='row panel-form' style='margin-top:1.25rem;'>
	<a href="javascript:nextProd('^cat^','^skc^',0)" class='btn btn-primary' style=''> <<^cat^ </a>
	<a href="javascript:nextProd('^cat^','^skc^',1)" class='btn btn-primary pull-right'> >>^cat^ </a>
</div>
<div class='row panel-form'>
	<div class='col col-md-12' style='padding:0px'>
		<div class='card-article'>
			<div style='padding-left: 12px; padding-top: 12px;'>
				Image <label id='imgfile'>^pimg^</label>
			</div>
			<div>
				<center><img  id ='pimage' src=' ' weight=30% height=30%></center>
			</div>
			<a href="javascript:resizeImg(@skcode)" class='btn btn-primary' style='margin-left:1.25rem; margin-bottom:1.25rem;'>Copy Image to DB</a>
			<a href="javascript:updateEPROD(@skcode)" class='btn btn-primary' style='margin-left:1.25rem; margin-bottom:1.25rem;'>Copy Product Name to DB</a>
		</div>
	</div>
</div>
<div class='row panel-form'>
	<div class='col col-md-12 card-article'>
		<div class='article'>
			<h3>:00</h3>
		</div>
		<hr style="border-top-width: 5px;">
		^ldesp^
	</div>
</div>
<div class='row panel-form' style='margin-bottom:1.25rem'>
	<div class='col-md-6' style='padding-left:0px;padding-right: 8px;'>
		<div class='col-md-12 card-article'>
			<div class='article'>
				<h3>Attribute:</h3>
			</div>
			<hr style="border-top-width: 5px;">
			<div style='margin-bottom:1.25rem;'>
				^attrb^
			</div>
		</div>
	</div>
	<div class='col-md-6' style='padding-right:0px;padding-left: 8px;'>
		<div class='col-md-12 card-article' >
			<div class='article'>
				<h3>Short Description:</h3>
			</div>
			<hr style="border-top-width: 5px;">
			<div style='margin-bottom:1.25rem;'>
				#price
			</div>
		</div>
	</div>
</div>
 </div>
 </div>

$END
 
$TAB
|XID #price
|HDR
|SQL
use.CLOUD^
use PLPPSERVICE
Declare @rem varchar(500)
SElect  top 1 @rem=EvName
  from BMGNEVENT a join BMGNCONTROL b on a.EVNUM=B.EVNUM
				join BMGNQPRODUCTSET c on b.CTLID=c.CTLID
			 	join BMGNEFFECT d on a.evnum=d.evnum
  where SKCODE = @skcode and STATUS='A' 
  and EDATE>getdate()
  order by SDATE DESC

  Select top 1
		  PRICE=PR_PRICE
		 ,PM=isnull(@rem,'--None--')
		,STOCK=STOCKAVAIL
		 from UPFPOS..Posprod
		 where   sk_code=@skcode and STCODE='@stcode'
|BDY
<h4>Retail Price:<b> ^PRICE^</b> Baht </h4>
Stock Avail(@ @stcode) :<b>^STOCK^</b>  
<h4>PLPP Promotion:</h4>  ^PM^ 
|END


//END*****/