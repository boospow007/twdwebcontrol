var imageUpload =false;
var actionLine = 0;
var editObj;
var editskc;
var curcat='000';
var imagelink="";
function pageReady(){
  try  {
        

            $('#showDiv').mouseleave(function () {
            //     $('#detailCard').hide();
                  $('#popUpMenu').hide();
              });
          curcat=readCookie("CURCAT")
          if (curcat==null)
             curcat=$('#catTab tr:eq(1) td:eq(0)').text();

          prodInCat(curcat)
   
    }

    catch(ex){alert(ex.message)}     
}

function handleFileSelect(evt) {
  console.log('File is selected  ')

  var tfile=  $('#files')[0].files;

  uploadFiles(tfile)

}
function handleDrop(e){
  e.stopPropagation();
  e.preventDefault();
  console.log(e.dataTransfer)
  var files = e.dataTransfer.files; // FileList object.
    console.log('Drop files ' +files[0].name);
    uploadFiles(files);
}

function uploadFiles(tfiles){
    var formData = new FormData();
    //--check if Files is jpg
    var fname =tfiles[0].name;
    fname=fname.toLowerCase();

    if(fname.indexOf('.jpg') < 0
         && fname.indexOf('.jpeg') < 0
        &&  fname.indexOf('.png') < 0
      ){
        alert ("file "+fname  +" is not jpg Image file");
        return
    }

    // add assoc key values, this will be posts values
    formData.append("userPhoto", tfiles[0],"userPhoto");
    //!!! Importance Name of the Files must conform with multer in th uplad or the same as input file name
    var url="/upload";

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
          console.log(  data);
          var img = "uploads/userPhoto.jpg?t=" + new Date().getTime();
          $('#prodImage').prop('src',img);
             imagelink=img;
            imageUpload=true;
        },
        error: function(){
            alert("error in ajax form submission");
        }
    });
 
} 
function prodInCat(cat){
  var pdata={
        patname:'PRODUCT/prodincat.txt',
        para:{catcode:cat}
  }
  ajaxXpandFile(pdata,function(isok,ret){
    $('#prodDiv').html(ret);
    setNumericFormatDiv('prodDiv');
    curcat=cat;
    saveCookie("CURCAT",cat);
  })
}


function menuOver(obj) {
  editObj =obj;
  var pos = $(obj).position();
  var mpos = $('#prodUDiv').position();
  
  actionLine = $(obj).find('td:eq(0)').text();
  $('#popUpMenu').css('top', pos.top + mpos.top);
  var left=$(window).width();
  $('#popUpMenu').css('left', mpos.left+100 );
  $('#popUpMenu').show();
 
}
//---------------EDIT ITEM-------------
function editItem(rec){
 
  try{
    imageUpload =false;
      var card=0;
      var pos={top:100,left:0};
     
      if(rec==='NEW')card='NEW';
      else  //--edit from ables displayed
          {  //specified from table display
          card=$(editObj).find('td:eq(0)').text(); 
          pos =$(editObj).position();
         }

     //    alert('Call for Edit '+card);
      var pdata={
              patname:'./PRODUCT/editprod2.txt',
              para:{
                  stc:storeCode,
                  catcode:curcat,
                  rec:card
                  }
          }

      editskc=card;
      var mpos = $('#prodUDiv').position();
      ajaxXpandFile(pdata,function(isok,data){
          console.log("AJAX call Edit "+isok);
          $('#editModal').html(data);
          $('#editModal').modal('show')  ;
       //-------------aign old value
         $('#cattxt').val($('#cattxt').attr('ovalue'));
         $('#dtytxt').val($('#dtytxt').attr('ovalue'));
         $('#vtytxt').val($('#vtytxt').attr('ovalue'));

          //!!! becareful  variable outside here is not accessable
          //getImage(rec) --->>dont work
           getImageFromDB();  //--get image from DB in base64
          //-------------add event to inpute
          document.getElementById('files').addEventListener('change', handleFileSelect, false);
          document.getElementById('imgDiv').addEventListener("drop", handleDrop,false);
 
      });
  }
  catch(ex){alert('Edit Item error '+ex.message);}
}

function getImageFromDB(){
  var skc=editskc; //$('#prodImage').prop('skcode');
  if(skc=='NEW')return

  var url="/B/"+ skc  ;  // this function read image from DB
//		alert(url);
  $.get(url,function(data){
    $('#prodImage').prop('src', data  ) ;
    imagelink='data:'
  });
}
//---------------
function deleteItem(){
  try{
      var rcpt =$(editObj).find('td:eq(0)').text();
      if(confirm('Remove PRODUCT #'+rcpt)==0)return;
      //alert('VOid');
      var cmd="delete POSPROD where PRODIC =  @rec";
      var pdata={
              query:cmd,
              para:{stcode:storeCode, 
                     rec:rcpt} 
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                     alert('Foods has been removed');
                     //--reload this page
                     pageReload();
              }
              else
                  alert(jsn.dbmessage);
          }
          else
              alert('System error Cant call service');
      });
  }
  catch(ex){alert('delete function Error..'+ex.message);}
}

function hideDetail(){
 
  $('#editModal').modal('hide')  ;
}

function confirmEnt(){
  try{
     // var rec = editskc; // $('#rectxt').val();	
      
     // var cmd="Update POSPROD set BARCODE='@bar', CATCODE='@cat',PRNAME='@nme' ,UNAME='@unm', UPRICE=@prc ,REMARK='@rem' Where PRODID= @rec ";
     // if(rec=='NEW')
          //cmd="Insert POSPROD(CATCODE, PRNAME,UNAME,UPRICE,REMARK,BARCODE)  " 
          //    +"values('@cat','@nme','@unm',@prc,'@rem','@bar'  )";
      cmd="exec addUpdateProd  '@rec','@cat','@nme','@unm',@prc,'@rem','@bar','@vty','@dty','@chc' ";  //we want system to return prodic that just inserted
      //---thsi command will return PRODID if successful
      var pdata={
              query:cmd,
              para:{ 
                    nme:$('#nmetxt').val(),
                    cat:$('#cattxt').val(),
                    unm:$('#unmtxt').val(),
                    prc:$('#prctxt').val(),
                    rem:$('#remtxt').val(),
                    bar:$('#bartxt').val(),
                    vty:$('#vtytxt').val(),
                    dty:$('#dtytxt').val(),
                    chc:$('#chctxt').val(),
                    rec:editskc
                    }
        }
      ajaxExec(pdata,function(isok,resp){
          if(isok){
              var jsn =JSON.parse(resp);
              if(jsn.dbcode==0){
                   //---------Save Image
                   if(imageUpload ){
                           var pid =jsn.dbitems[0].PRODID;
                           saveImage2(pid); //?? if NEW image is not Save cuase Rec is NEW
                          }
                   else { 
                      //  alert('Foods has been changed');
                        $("#editModal").modal("hide") 
                     //--reload this page
                        pageReload();  //--load same Pattern again
                     }
            }
              else
                  alert(jsn.dbmessage);
          }
          else
                  alert('System error Cant call service');
      });
 
  }
  catch(ex){alert('Confirm Entry Error..'+ex.message);}
}
//  we dont save to file
function saveImage(skc){
// --move frile from Upload/userPhoto to PRODUCT/Skxxx.jpg
var url='/moveimg/'+skc ;
  $.ajax({url: url, 
    success: function(result){
      //alert(result);
    //  alert('Image has been updated');
   //   pageReload();  //--load same Pattern again
      //--reload this page
      $("#editModal").modal("hide") 
      pageReload();  //--load same Pattern again
    }}) ;
     
}
 

function saveImage2(skc){

  //base64=$('#prodImage').prop('src');
 //console.log(imagelink);

  if(imagelink.slice(0,4)=='data'){
    saveBase64Image(skc);
    return
  }
  if(imagelink.slice(0,4)=='http'){
    saveUrlImage(skc);
    return
  }

  // --save to DB base64
  var sUrl  ='/IMAGE/FILETODB' ;
  var url   = './public/uploads/userPhoto.jpg'
  var pdata={fname:url,
            skc:skc}
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
          //alert(ret);
          pageReload();  //--load same Pattern again
				  },
       error: function (e) { 
            alert( e.responseText);
           }
      });
  }
  //-----------------
  function saveBase64Image (skc){
    // --save to DB base64
  
    var sUrl  ='/IMAGE/BASE64TODB' ;
    var data =$('#prodImage').prop('src');  //--->not work 
    var base =data.split(',')[1];
    console.log('save base64 image ')

    var pdata={base64:base,
              skc:skc}
      $.ajax({
        type: "POST",
        url: sUrl,
        data: JSON.stringify(pdata),
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        success: function (ret) { //return is html string
            //alert(ret);
            pageReload();  //--load same Pattern again
            },
         error: function (e) { 
              alert( e.responseText);
             }
        });
    }
  //-----------------
  function saveUrlImage (skc){
    // --save to DB base64
  
    var sUrl  ='/IMAGE/TODB' ;
    var url =imagelink;  //$('#prodImage').prop('src');  //--->not work 
    
    console.log('save url image '+url)

    var pdata={url:url,
              skc:skc}
      $.ajax({
        type: "POST",
        url: sUrl,
        data: JSON.stringify(pdata),
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        success: function (ret) { //return is html string
            //alert(ret);
            pageReload();  //--load same Pattern again
            },
         error: function (e) { 
              alert( e.responseText);
             }
        });
    }
function cancelEnt(){
  $('#editModal').modal('hide')  ;
}
//---Add New item
function addNewItem(){
  //alert('Add New');
  editItem('NEW');
}
 
function copyUrl2Image(){
  //---copy Image from Url
  var url=$('#urltxt').val();

  if(url.length<3)return;

  //-----------check if it is valid link
  var header=url.slice(0,4);

  //---  for base64  url will begin like htis  'data:image/jpeg;base64,....'
  if(header=='data'){
     //---it is base 64 Image, put it in the image
     $('#prodImage').prop('src', url ) ;
     imageUpload =true;
     imagelink    =url;
     $('#urltxt').val('');
     return
  }
  if(header=='http'){
    $('#prodImage').prop('src',url);
     $('#urltxt').val('');
     imageUpload =true;
     imagelink=url;
    return
  }
  
  //----------------we will upload file in confirm command
  var sUrl ="/IMAGE/TOFILE";
  var pdata={
        imgurl:url,
        tofile:"./public/uploads/userPhoto.jpg"}

    $.ajax({
        type: "POST",
        url: sUrl,
        data: JSON.stringify(pdata),
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        success: function (ret) { //return is html string
           if(ret.slice(0,3)!='ERR'){
             //--reload image
            var img = "uploads/userPhoto.jpg?t=" + new Date().getTime();
              $('#prodImage').prop('src',img);
            imageUpload =true;
            imagelink=img;
           }
           else alert(ret);
          },
        error: function (e) { 
            alert( e.responseText);
          }
    });
}

/*** 
$HDR
<style>
.table tr:hover {
  background-color:silver;
  cursor:pointer;
}
 
  .myForm {
    display: grid;
    grid-template-columns: auto 1fr 1fr;
    grid-auto-flow: row dense;
    grid-gap: .8em;
    background: #eee;
    padding: 1.2em;
    width:800px;
  }
  .myForm > label  {
    grid-column: 1;
    grid-row: auto;
  }
  .myForm > input,
  .myForm > select,
  .myForm > button {
    grid-column:1;
    grid-row: auto;
    padding: 1em;
    border: none;
  }
  
   #inp-box {
   grid-column: 1;
   grid-row: span 1 /2  ;
  }
  #comment-box {
   grid-column: 2 /4 ;
   grid-row: span 1 ;
  }
  
  .myForm > button {
   grid-column: 1 / 2; 
  }
 
</style>

<div  id ='showDiv' class='container'>
<h2> Foods/Drinks</h2>
<a href ='javascript:addNewItem()' class='btn btn-info pull-right'>Add New</a>

<div   class='row'>
<div class ='col col-md-3'>
<h3>Category</h3>
<table id ='catTab' class='table'>
<thead><th>ID <th>Category </thead>
$SQL
Select * from CATEGORY where catCode like '_00'
$BDY
<tr onclick="javascript:prodInCat('^CATCODE^')" >
<td>^CATCODE^<td>^CATNAME^
$ERX
<tr><td colspan='2'>Error @error
$END
</table>  
</div>

<div id='prodUDiv' class='col col-md-9'>
   <div id='prodDiv'>
   PRODUCT To be Display here
  </div>
</div>

</div> 

 

<div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
   <a href="javascript:deleteItem()" class="btn smn btn-danger">Delete</a>
   <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>

</div>



</div>

$xxx

//END  **/