function pageReady(){
    $('#showDiv').mouseleave(function () {
              $('#popUpMenu').hide();
          });
   loadSubDept('01');
   loadClass('0101');      
}

function loadSubDept(dep) {
  var pdata={
    patname:'./PRODUCT/catsubdept.txt',
    para:{
        dept:dep
        }
      }
  ajaxXpandFile(pdata,function(isok,data){
  //  console.log(data)
    $('#subDeptDiv').html(data);
  })          

}
function loadClass(dep) {
  var pdata={
    patname:'./PRODUCT/catclass.txt',
    para:{
        dept:dep
        }
      }
  ajaxXpandFile(pdata,function(isok,data){
  //  console.log(data)
    $('#classDiv').html(data);
  })          

}
var editDept = 0;
var editObj;
var catName;
function menuOver(obj,parent) {
  editObj =obj;
  var pos = $(obj).position();
  
  //console.log(parent)

  var pos2 = $('#'+parent).position();
 
  editDept = $(obj).find('td:eq(0)').text();
  catName = $(obj).find('td:eq(1)').text();
  $('#popUpMenu').css('top', pos2.top+pos.top);
  var left=$(window).width();
  $('#popUpMenu').css('left', pos2.left + pos.left);
  $('#popUpMenu').show();
 
}
var imageUpload ;
 //---------------EDIT ITEM-------------
function editItem(){
  try{
      imageUpload =false;
      var card=0;
      var pos={top:100,left:0};
    
     //    alert('Call for Edit '+card);
      var pdata={
              patname:'./PRODUCT/editcat.txt',
              para:{
                  cat:editDept,
                  cname:catName
                  }
          }

      editskc=editDept;

      //console.log("Edit Cat="+editDept);
   
      ajaxXpandFile(pdata,function(isok,data){
          //console.log("AJAX call Edit "+isok);
          $('#editModal').html(data);
          $('#editModal').modal('show');

       //-------------aign old value
          //!!! becareful  variable outside here is not accessable
          //getImage(rec) --->>dont work
          
          $('#catImage').removeAttr('src');
          //$("#files").val(''); // clear
          //$("#files").val(null); // clear
          resetFileElement($('#files'));

          
          getCatImageFromDB();  //--get image from DB in base64
          //-------------add event to inpute
          document.getElementById('files').addEventListener('change', handleFileSelect, false);
          document.getElementById('imgDiv').addEventListener("drop", handleDrop,false);
 
      });
  }
  catch(ex){alert('Edit Item error '+ex.message);}
}
function keyUrlPress(ev){
  if(ev.keyCode==13){
    copyUrl2CatImage()
 
  }
}
function copyUrl2CatImage(){
  //---copy Image from Url
  var url=$('#urltxt').val();

  if(url.length<3)return;

  console.log(url)

  //-----------check if it is valid link
  var header=url.slice(0,4);

  //---  for base64  url will begin like htis  'data:image/jpeg;base64,....'
  if(header=='data'){
     //---it is base 64 Image, put it in the image
    // $('#catImage').prop('src',url) ;
     $("#catImage").attr("src",url);
     imageUpload =true;
     imagelink    =url;
     $('#urltxt').val('');
     return
  }

  if(header=='http'){
    //$('#catImage').prop('src',url);
    $("#catImage").attr("src",url);
     $('#urltxt').val('');
     imageUpload =true;
     imagelink=url;
    return
  }
  
}

function getCatImageFromDB(){
  var cat=editDept; //$('#prodImage').prop('skcode');

  var url="/CATIMG/"+cat  ;  // this function read image from DB
  
  console.log('getCatImageFromDB :'+url)
  //alert(url);


  $.get(url,function(data){
    
    //console.log(data)        
    
    //$('#catImage').prop('src',data);
    //$('#catImage').removeAttr('src');    
    //$("#catImage").attr("src",data);
    $("#catImage").removeAttr("src").attr("src", data);
    
    imagelink='data:'; 
    
  });
}
 
 function subItem(){
   var dep=editDept.trim();
   console.log('Sub Item '+dep);
   if(dep.length==2)loadSubDept(dep);
   else if(dep.length==4) loadClass(dep)
   else return
 }
 
 function resetFileElement(ele) 
{
    ele.val(''); 
    ele.wrap('<form>').parent('form').trigger('reset');   
    ele.unwrap();
    ele.prop('files')[0] = null;
    ele.replaceWith(ele.clone());    
}

 //----save Image to DB depend on type of Image
 function confirmCatEnt(){
   
  var t=imagelink.slice(0,4);

  console.log('save img mode ='+t +' cat ='+editDept);
  
  
   //-----Save image depned on type of input
  if(imagelink.slice(0,4)=='data'){   //base64 image
    saveBase64Image(editDept);
    return;
  }
  if(imagelink.slice(0,4)=='http'){ //imaage is URL
    saveUrlImage(editDept);
    return;
  }
  if(imagelink.slice(0,4)=='file'){ //imaage file    
    saveFileImage(editDept);
    return;
  }
 
  

}
function saveFileImage(cat){ 
  setTimeout(function(){
    saveImageFromFileImage(cat);
  }, 500);
}
  //-----------------

function saveImageFromFileImage(cat){ 
  // --save to DB base64
  var sUrl  ='/CATIMG/FILETODB' ;
  var url   = "";

  url="./public/uploads/userPhoto.jpg";

  console.log('save img cat: ' + cat +' with url: ' +url);

  var pdata={fname:url,
            cat:cat}
            
  $.ajax({
    type: "POST",
    url: sUrl,
    data: JSON.stringify(pdata),
    dataType: "text",
    contentType: "application/json; charset=UTF-8",
    success: function (ret) { //return is html string
        alert(ret);

        $('#editModal').modal('hide');
        $('#catImage').removeAttr('src');
        resetFileElement($('#files'));

    },
    error: function (e) { 
          alert( e.responseText);
         }
    });
}
  //-------------------
function saveBase64Image (cat){
    // --save to DB base64
  
    var sUrl  ='/CATIMG/BASE64TODB' ;
    //var data =$('#catImage').prop('src');  //--->not work 

    var data =$('#catImage').attr('src');
    
    var base =data.split(',')[1];
    console.log('save base64 image ')

    var pdata={base64:base,
              cat:cat}
      $.ajax({
        type: "POST",
        url: sUrl,
        data: JSON.stringify(pdata),
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        success: function (ret) { //return is html string
            //alert(ret);
            $('#editModal').modal('hide');
            },
         error: function (e) { 
              alert( e.responseText);
             }
        });
    }
  //-----------------
  function saveUrlImage (cat){
    // --save to DB base64
  
    var sUrl  ='/CATIMG/URLTODB' ;
    var url =imagelink;  //$('#prodImage').prop('src');  //--->not work 
    
    console.log('save url image '+url + ' to DB '+cat)

    var pdata={url:url,
              cat:cat}
      $.ajax({
        type: "POST",
        url: sUrl,
        data: JSON.stringify(pdata),
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        success: function (ret) { //return is html string
            alert(ret);
            $('#editModal').modal('hide');
            //pageReload();  //--load same Pattern again
            },
         error: function (e) { 
              alert( e.responseText);
             }
        });
    }
 
function cancelCatEnt(){
 // console.log('Cancel Entry')
 // alert('Cancel')
  $('#editModal').modal('hide');
  return
}

function handleFileSelect(evt) {
  console.log('File is selected  ')
  var tfile=  $('#files')[0].files;
  uploadFiles(tfile)
}

function handleDrop(e){
  e.stopPropagation();
  e.preventDefault();
  console.log(e.dataTransfer)
  var files = e.dataTransfer.files; // FileList object.
  console.log('Drop files ' +files[0].name);
  uploadFiles(files);
}

function uploadFiles(tfiles){
    var formData = new FormData();
    //--check if Files is jpg
    var fname =tfiles[0].name;
    fname=fname.toLowerCase();

    console.log('File Name: ' +fname);

    if(fname.indexOf('.jpg') < 0
         && fname.indexOf('.jpeg') < 0
         &&  fname.indexOf('.png') < 0
      ){
        alert ("file "+fname  +" is not jpg Image file");
        return
    }

    // add assoc key values, this will be posts values
    formData.append("userPhoto",tfiles[0],"userPhoto");
    //!!! Importance Name of the Files must conform with multer in th uplad or the same as input file name
    var url="/upload";

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
          console.log('uploadFiles:'+  data);
          var img = "uploads/userPhoto.jpg?timestamp=" + new Date().getTime()+Math.floor(Math.random()*1000);
          
          $("#catImage").removeAttr("src").attr("src",img);

          $("#catImage").attr('src', $("#catImage")
                        .attr('src') + '?' + Math.random() );

          //$('#catImage').prop('src',img);
          //$("#catImage").attr("src", $("#catImage").attr("src")+"?timestamp=" + new Date().getTime());              

          imagelink="file";
          imageUpload=true;
          //imageUploadComplete(img);
        },
        error: function(){
            alert("error in ajax form submission");
        }
    });
 
} 

function imageUploadComplete (newImageUrl) {
  $("#image_container").html("<img src='" + newImageUrl + "'>");
}


/*** 
$HDR
<div id ='showDiv' class='container'>

<div id='deptDiv' class='col col-sm-4'>
<h2>Department</h2>
<table class='table'>
<thead><th>ID <th>Name</thead>
$SQL
Select DEPT,TNAME
 from CSCATEGORY
 Where right(NEWCLASS,6)='000000'  
$BDY
<tr onmouseover ="menuOver(this,'deptDiv')" >

<td>^DEPT^<td> ^TNAME^ </tr>
$ERX
<tr><td colspan='2'>Error @error
$END
</table>
</div>

<div id='subDeptDiv' class='col col-sm-4'>

  SubDepartment
</div>

<div id='classDiv' class='col col-sm-4'>
 Classs
</div>

<div id='popUpMenu' style='display:none;z-index:6;position:absolute;'>
   <a href="javascript:subItem()" class="btn smn btn-danger">SUB DEPT</a>
   <a href="javascript:editItem()" class="btn smn btn-primary">Edit</a>
</div>

 

</div>



$xxx

//END  **/