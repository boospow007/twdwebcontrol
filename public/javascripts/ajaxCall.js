function ajaxGetQuery(qry,callBack){  //---1 column search
 		var pdata ={query:qry,
 					para:null};
 		
 		 ajaxExec(pdata,function(ok,ret){
 			 try { 
 				
 			 if(ok){
 				 var jsn=ret; //JSON.parse(ret);
 				// alert('ajaxExec ret '+ok+' '+ret);
 				 if(jsn.dbcode>0){
 					 //alert('ajaxExec ret ' +jsn.dbmessage);
 					 callBack(false,jsn.dbmessage);
 					 return;
 				 }
 				 else {
 					 /*
 					 var nitm=jsn.dbitems.length;
 					 if(nitm<=0){
 						 callBack(false,'No result set');
 					 }
 					 else 
 						 callBack(true,jsn);
 					 */	 
 					callBack(true,jsn);
 				 }
 			 }
 			 else {
 				 callBack(false,ret);
 			  }
 			 }
 		catch(ex){callBack(false,'ajaxGetQuery..'+ex.message);}	 
 		
 		 });
  
}
function ajaxExec(pdata,callBack){
	 var sUrl 		="/POSSRV/QUERY";  // for mssql
	 try{ 
	  $.ajax({
		type: "POST",
		url: sUrl,
		data: JSON.stringify(pdata),
		dataType: "text",
		contentType: "application/json; charset=UTF-8",
		success: function (ret) { //return is array of data in json 
		// alert('Ajx exec ' +ret);
				//callBack(true,JSON.parse(ret));
			    callBack(true,ret);    //ret is json return from DB query
			    return;
          },
		error: function (e) { 
		 callBack(false, e.responseText);
		}
	  });
	 }
	 catch(ex){callBack(false,ex.message);}
}
function ajaxExpScript(patstr,callBack){
	var patctl ={
			hdr:null,
			qry:null,
			bdy:null,
			erx:null,
			end:null,
			cmp:null};
	//---split old apptern to json var
	var lns =patstr.split("$");
	for(var n=1;n<lns.length;n++)
			{
			var str =lns[n];
			if(str.substring(0,3)=="HDR" )patctl.hdr=str.substring(4);
			if(str.substring(0,3)=="SQL" )patctl.qry=str.substring(4);
			if(str.substring(0,3)=="BDY" )patctl.bdy=str.substring(4);
			if(str.substring(0,3)=="END" )patctl.end=str.substring(4);
			if(str.substring(0,3)=="ERX" )patctl.erx=str.substring(4);
			}
	ajaxXpandPat(patctl,function(bool,ret){
		callBack(bool,ret);
	});
}
//------REad Pattern ptn in format of json {hdr,qry,bdy,end,erx..}
function ajaxXpandPat(ptn,callBack){
	try {
		 
		//---request to Server
		var sUrl 		="/XPAND/PAT";  // for mssql
 		var pdata =ptn;
   
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
				try {
					 callBack(true,ret);
				  }
				catch(ex){callBack(false,ex.message);}
				
				return;
              },
			error: function (e) { 
			 callBack(false, e.responseText);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}
//-----------old version pat =filename , par =parameter ---------
function ajaxExpTab(pat, par,callBack){
	var pdata ={patname:pat, 
				para:par}
	ajaxXpandFile(pdata,function(err,ret){
		callBack(err,ret);
	});
}
//-----expand patttern the Old way pdata ={patname:xx,para:xxx}
async function  ajaxXpandFile(pdata,callBack){
	try {
		 //pdata ={patname:xx,para: {}}
		//---request to Server
		var sUrl 		="../XPAND/FILE";  // Call expand route
 
		await $.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
				//alert('Ajx ret '+ret);
				try {
					 callBack(true,ret);
				  }
				catch(ex){callBack(false,'AJX Fn Ret.. '+ex.message);}
				
				return;
              },
			error: function (e) { 
			 callBack(false, 'AJX Fn.. '+e.responseText);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}
function  ajaxQuery2table(qry,callBack){
	try {
		 
		//---request to Server
		var sUrl 		="/POSSRV/QRY2TAB";  // for mssql
		var pdata ={query:qry};
   
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
				try {
					 callBack(true,ret);
				  }
				catch(ex){callBack(false,ex.message);}
				
				return;
              },
			error: function (e) { 
			 callBack(false, e.responseText);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}

function  ajaxQuery2tablereport(qry,callBack){
	try {
		 
		//---request to Server
		var sUrl 		="/POSSRV/QRY2TABREPORT";  // for mssql
		var pdata ={query:qry};
   
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
				try {
					 callBack(true,ret);
				  }
				catch(ex){callBack(false,ex.message);}
				
				return;
              },
			error: function (e) { 
			 callBack(false, e.responseText);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}


function  ajaxGetUrl(url,callBack){
	try {
		 
		//---request to Server
		var sUrl 	=url
 		$.ajax({
			type: "GET",
			url: sUrl,
 			dataType: "text",
		//	contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
			    callBack(true,ret);
				return;
              },
			error: function (e) { 
			 callBack(false,"Can not get to "+url);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}
//--------------support speicifc for POSREQUEST
function ajaxPosRequest(qry,callBack){
	try {
 
		//---request to Server
		 var sUrl 		="/POSSRV/POSREQ";  // for mssql
 		var pdata ={query:qry};
   
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is array of data in json 
				try {
					var jsn =JSON.parse(ret);   //if dataType is text then return is text
												//if dat type is json, u dont haave to parse to json
					 var ecode=jsn.error.code;
					
					 if(ecode>0){
					//	$('#resultDiv').text("Error from Web Service..."+ jsn.error.message);
						 callBack(false,"Error from Web Service..."+ jsn.error.message)
						return;
					 }
					 var rets=jsn.resp.dbitems[0];
					 callBack(true,rets);  //--return as json data record
				  }
				catch(ex){callBack(false,ex.message);}
				
				return;
              },
			error: function (e) { 
			 callBack(false, e.responseText);
			}
		});
	}
	catch(ex){ callBack(false,ex.message);}
}