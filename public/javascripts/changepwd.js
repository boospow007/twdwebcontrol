

//change password
function changepwd(){
	var usr = readCookie('USERID');
	
	$('#txtchgUsername').val(usr);
	$('#changePwdModalDiv').modal('show')  ;
}
function formChgCancel(){
	$('#changePwdModalDiv').modal('hide');
}
function formChgConfirm(){	    
        try { 
			 
			var uid 	= $('#txtchgUsername').val();
			var opassw  	= $('#txtOldPass').val();
			var npassw  	= $('#txtNewPass').val();
			var npassw2  	= $('#txtNewPass2').val();
		
			opassw=opassw.trim();
			npassw=npassw.trim();
			npassw2=npassw2.trim();

			if (opassw=='') {
				alert('กรุณาใส่รหัสผ่านเดิม');
				$('#txtOldPass').focus();
				return;
			}

			if (npassw=='') {
				alert('กรุณาใส่รหัสผ่านใหม่');
				$('#txtNewPass').focus();
				return;
			}
			if (npassw2=='') {
				alert('กรุณายืนยันรหัสผ่านใหม่');
				$('#txtNewPass2').focus();
				return;
			}

			if (npassw!=npassw2) {
				alert('กรุณากำหนดรหัสผ่าน และ ยืนยันรหัสผ่านให้ตรงกัน');
				$('#txtNewPass').focus();
				return;
			}

			if (opassw==npassw2) {
				alert('รหัสผ่านใหม่ต้องไม่ซ้ำกับรหัสผ่านเดิม');
				$('#txtNewPass').focus();
				return;
			}

		
            //-------------- change pwd
            var qry="exec ws_changepwd '@user','@opasswd','@npasswd', '@npasswd2' ";
			console.log(qry);
			
            var pdata={ query:qry,
                        para:{user:uid, 
							opasswd:opassw,
							npasswd:npassw,
							npasswd2:npassw2                        
                        }}
						

            ajaxExec(pdata,function(isok,ret){
                if(isok){
                    jsn=JSON.parse(ret);
                    if(jsn.dbcode==0){
						$('#changePwdModalDiv').modal('hide');
						removeCookie('USERID');						
						location.reload();												
                    }
                 else alert(jsn.dbmessage);
                }
              else alert('Error .. Ajax call');
            })
     
        }
    catch(ex){alert(ex.message);}
}