function displayEorderingQRPayment() {
	if (profileList.indexOf('0054') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู E-Ordering By QR Payment');
		location.assign("/report");
	}

	try {
		var userid = readCookie('WUSERID');
		var stc = readCookie('WDFSTORE');
		var b1 = '';
		var b2 = '';
		var ttm = '';
		var ttm2 = '';

		var xMax = 800,
			yMax = 600;


		if (window.screen) {
			xMax = window.innerWidth;
			yMax = window.innerHeight;
		}

		var para = btoa("fname=REPORT/Rep_eorderingPaymentQR.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + userid + "&branch1=" + b1 + "&branch2=" + b2 + "&stc=" + stc);
		//var para="fname="+ btoa(REPORT/Rep_eorderingPaymentQR.js)+ "&sdate="+ttm+"&edate="+ttm2+"&userid="+userid+"&branch1="+b1+"&branch2="+b2+"&stc="+stc) 

		var url = "/DPAGE?" + para;

		window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');

	} catch (ex) {
		alert(ex.message);
	}
}

function displaySalesOnlineAtStore() {
	if (profileList.indexOf('0055') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู รายงานยอดขาย Online At Store ที่ยอดเงินโอนเข้าสำนักงานใหญ่ (2C2P)');
		location.assign("/report");
	}

	try {
		var userid = readCookie('WUSERID');
		var stc = readCookie('WDFSTORE');
		var multibrnchselected = '';

		var ttm = '';
		var ttm2 = '';

		var xMax = 800,
			yMax = 600;

		if (window.screen) {
			xMax = window.innerWidth;
			yMax = window.innerHeight;
		}
		yMax = yMax - 30;


		var url = "/DPAGE?" + btoa("fname=REPORT/Rep_SalesOnlineAtStore.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + userid + "&multibrnch=" + multibrnchselected + "&stc=" + stc);

		window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');

	} catch (ex) {
		alert(ex.message);
	}
}

function displayCancelOrderReport() {
	if (profileList.indexOf('0053') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Cancel Order Report');
		location.assign("/report");
	}

	try {
		var userid = readCookie('WUSERID');
		var stc = readCookie('WDFSTORE');
		var b1 = '';
		var b2 = '';
		var ttm = '';
		var ttm2 = '';

		var xMax = 800,
			yMax = 600;


		if (window.screen) {
			xMax = window.innerWidth;
			yMax = window.innerHeight;
		}
		yMax = yMax - 30;


		var url = "/DPAGE?" + btoa("fname=REPORT/Rep_CancelOrderReport.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + userid + "&branch1=" + b1 + "&branch2=" + b2 + "&stc=" + stc);

		window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');

	} catch (ex) {
		alert(ex.message);
	}
}