var waitimg = "<center><img src='/images/loading.gif' style='width:10vh;'></center>";

function pageReady() {

    $('#sdate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'
    });

    $('#edate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'

    });

    //---convert numeric format
    $('.numeric').each(function () {
        var val = $(this).text();
        val = val.replace(/,/g, "");
        $(this).css('align', 'right');
        val = val.trim();
        if (val.length > 0)
            $(this).html(numericFormat(val))
    });

    //-----
    /* default charset encoding (UTF-8) */

    TableExport.prototype.charset = "charset=utf-8";
    var tableExporter = $('#outputfile').tableExport({
        formats: ["xlsx"],
        bootstrap: false,
        exportButtons: true,
        filename: 'Cancel Order Report',
        ignoreRows: null,
        ignoreCols: null,
        trimWhitespace: true,
        RTL: false
    });
    tableExporter.typeConfig.date.assert = function (v) {
        return false;
    };

} //pageReady




function showRepDetail() {
    var userid = readCookie('WUSERID');
    var st = readCookie('WDFSTORE');

    var t1 = $('#sdate').val();
    var t2 = $('#edate').val();


    showDetail(userid, st, t1, t2);

}


function showDetail(u, s, t, t2) {
    try {

        var ttm = t;
        var ttm2 = t2;
        var xMax = 800,
            yMax = 600
        if (window.screen) {
            xMax = window.innerWidth;
            yMax = window.innerHeight;
        }
        yMax = yMax - 30;
        var url = "/DPAGE?" + btoa("fname=REPORT/Rep_CancelOrderReport.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + u + "&stc=" + s);

        window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');
    } catch (ex) {
        alert(ex.message);
    }
}

function printPKP() {

    var data = $('#detDiv').html();
    var mywindow = window.open('', 'mydiv', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/style.css" type="text/css"  media="print" charset="utf-8" />');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/bootstrap.min.css" />');
    mywindow.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
    mywindow.document.write('  .hideme {display:none;}  #conditionHdr { display:none; }  #conditionDiv {  display:none; }  .printbtn {display:none; } ');
    mywindow.document.write('  .xlsx {display:none; } ');
    mywindow.document.write(' </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    }, 1000)
}
/*****


$HDR

<script>
 
</script>
<style>

.hideme {
    display:none;
}
 
.TotalTable {
     font-weight: bold;
     border-bottom: 2px dotted #262626;
     background-color: #F0F0F0;
}

.Total {
     font-weight: bold;
     border-bottom: 2px solid #262626;
     background-color: #F0F0F0;
}
.GrandTotal {
     font-weight: bold;          
     border-bottom-style: double;
     border-bottom-width: medium;
     border-bottom-color: #262626;
     background-color: #F0F0F0;
}
.thcenter {
    text-align:center;    
    valign:middle;
    vertical-align: middle;
    
}

</style>

<div class='container-fluid'>
 
    <h4  id='conditionHdr'><b>เงื่อนไขการแสดงข้อมูล</b></h4>
    <div  id='conditionDiv'  class='well' style='font-size:14px;' >         
        <div  class='row'>            
            <div class='col-md-3' >
                Date From <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="sdate" name ="sdate"  
                onFocus='this.select();' 
                autocomplete="off"
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3'  >
                To <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="edate" name ="edate"  
                onFocus='this.select();' 
                autocomplete="off"
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3' >
                <br>
                <a href="javascript:showRepDetail()" class="btn btn-success btn-sm">Confirm</a>

            </div>
        </div>
    </div>
 
    
    <span id='uid' style='display:none;'> @userid</span>
    <span id='stc' style='display:none;'> @stc</span>     
    <span  style='display:none;'> @sdate</span>
    <span  style='display:none;'> @edate</span>     
    
<div class='row'> 
    <div id='detDiv'>

        <div class='row'> 
            <div class='col col-md-12'>             
                #summary          
            </div>                
        </div>

        <table  id='outputfile' class='table table-bordered table-condensed' style='font-size:14px;' border='2'>
        <thead bgcolor='#5a9bd5' style='color:#ffffff;text-align:center;'> 
        
            <tr>  
            <td>CASHIER</td>
            <td>Supervisor Name</td>
            <td>Receipt No.</td>
            <td>Order No.</td>
            <td>เลขที่ใบจัดสินค้า</td>
            <td>Location</td>
            <td>Name</td>
            <td>Tel</td>
            <td>Date of Sales</td>
            <td>Return No.</td>
            <td>Date of Return</td>
            <td>TIME</td>
            <td>SKU</td>
            <td>PRODUCT CODE</td>
            <td>QTY</td>
            <td>AMOUNT</td>
            <td>Sales</td>
            <td>Sales type</td>
            <td>เหตุผล</td>
            </tr>        
        </thead> 
        <tbody>
 
        
$SQL  
 
exec CLOUDPOS..Rep_SorderVoid  '@sdate' , '@edate' , '@stc' , '@userid'

$BDY  
	 													
 
<tr class=''>    
    <td align='left' class='tableexport-string'>^CASHIER^</td>
    <td align='left'  class='tableexport-string'>^Supervisor Name^</td>    
    <td align='left'  class='tableexport-string'>^Receipt No.^</td>    
    <td align='left'  class='tableexport-string'>^Order No.^</td>    
    <td align='left'  class='tableexport-string'>^เลขที่ใบจัดสินค้า^</td>    
    <td align='left'  class='tableexport-string'>^Location^</td>    
    <td align='left'  class='tableexport-string'>^Name^</td>       
    <td align='left'  class='tableexport-string'>^Tel^</td> 
    <td align='left'  class='tableexport-string'>^Date of Sales^</td> 
    <td align='left'  class='tableexport-string'>^Return No.^</td>  
    <td align='left' class='tableexport-string'>^Date of Return^</td>

    <td align='left'  class='tableexport-string'>^TIME^</td> 
    <td align='left'  class='tableexport-string'>^SKU^</td>  
    <td align='left'  class='tableexport-string'>^PRODUCT CODE^</td>  

    <td align='right' class='numeric tableexport-string'>^QTY^</td> 
    <td align='right' class='numeric tableexport-string'>^AMOUNT^</td> 

    <td align='left'  class='tableexport-string'>^Sales^</td>  
    <td align='left'  class='tableexport-string'>^Sales type^</td>  
    <td align='left'  class='tableexport-string'>^เหตุผล^</td>       

</tr>
$ERX

$CMP
 

$END
</tbody>
</table>            
</div>
</div>

</div>

<br>
<a href='javascript:printPKP();' 
    class='btn btn-primary pull-right printbtn hideme'>
    <span class="fa fa-wpforms"></span> พิมพ์</a>
<br><br> 

 

$TAB
|XID #summary
|HDR
<center><h2>Cancel Order Report</h2></center> 
|SQL 
 
exec CLOUDPOS..Rep_SorderVoid_Header  '@sdate' , '@edate' , '@stc' , '@userid'

|BDY
<center>
<h4><b>Parameter : </b> 
Branch : ^BRANCH^ 
<br>
Date : ^SDATE^ - ^EDATE^/     
</h4>
</center> 
<hr>
|ERX
|END

 
  


//END*****/