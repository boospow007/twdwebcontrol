var waitimg = "<center><img src='/images/loading.gif' style='width:10vh;'></center>";

function pageReady() {

    $('#sdate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'
    });

    $('#edate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'

    });

    //---convert numeric format
    $('.numeric').each(function () {
        var val = $(this).text();
        val = val.replace(/,/g, "");
        $(this).css('align', 'right');
        val = val.trim();
        if (val.length > 0)
            $(this).html(numericFormat(val))
    });

    //-----
    /* default charset encoding (UTF-8) */

    TableExport.prototype.charset = "charset=utf-8";
    var tableExporter = $('#outputfile').tableExport({
        formats: ["xlsx"],
        bootstrap: false,
        exportButtons: true,
        filename: 'E-Ordering By QR Payment',
        ignoreRows: null,
        ignoreCols: null,
        trimWhitespace: true,
        RTL: false
    });
    tableExporter.typeConfig.date.assert = function (v) {
        return false;
    };

} //pageReady




function showRepDetail() {
    var userid = readCookie('WUSERID');
    var st = readCookie('WDFSTORE');

    var t1 = $('#sdate').val();
    var t2 = $('#edate').val();


    var branch1 = $('#store1').val().trim();
    var branch2 = $('#store2').val().trim();


    showDetail(userid, st, t1, t2, branch1, branch2);

}


function showDetail(u, s, t, t2, b1, b2) {
    try {

        var ttm = t;
        var ttm2 = t2;
        var xMax = 800,
            yMax = 600
        if (window.screen) {
            xMax = window.innerWidth;
            yMax = window.innerHeight;
        }
        yMax = yMax - 30;
        var url = "/DPAGE?" + btoa("fname=REPORT/Rep_eorderingPaymentQR.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + u + "&branch1=" + b1 + "&branch2=" + b2 + "&stc=" + s);

        window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');
    } catch (ex) {
        alert(ex.message);
    }
}

function printPKP() {

    var data = $('#detDiv').html();
    var mywindow = window.open('', 'mydiv', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/style.css" type="text/css"  media="print" charset="utf-8" />');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/bootstrap.min.css" />');
    mywindow.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
    mywindow.document.write('  .hideme {display:none;}  #conditionHdr { display:none; }  #conditionDiv {  display:none; }  .printbtn {display:none; } ');
    mywindow.document.write('  .xlsx {display:none; } ');
    mywindow.document.write(' </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    }, 1000)
}
/*****


$HDR

<script>
 
</script>
<style>

.hideme {
    display:none;
}
 
.TotalTable {
     font-weight: bold;
     border-bottom: 2px dotted #262626;
     background-color: #F0F0F0;
}

.Total {
     font-weight: bold;
     border-bottom: 2px solid #262626;
     background-color: #F0F0F0;
}
.GrandTotal {
     font-weight: bold;          
     border-bottom-style: double;
     border-bottom-width: medium;
     border-bottom-color: #262626;
     background-color: #F0F0F0;
}
.thcenter {
    text-align:center;    
    valign:middle;
    vertical-align: middle;
    
}

</style>

<div class='container-fluid'>
 
    <h4  id='conditionHdr'><b>เงื่อนไขการแสดงข้อมูล</b></h4>
    <div  id='conditionDiv'  class='well' style='font-size:14px;' > 
        <div  class='row'>

            <div class='col-md-4' >
                Branch From  <br>   
                <select class='form-control' id='store1' >                    
                    #storebranch1
                </select>

            </div>
            <div class='col-md-4' >
                To  <br>   
               <div id='subFdGrpDiv'>
                    <select class='form-control' id='store2' >                
                        #storebranch2
                    </select>
               </div>

            </div>                    
        </div>
        <br>
        <div  class='row'>            
            <div class='col-md-3' >
                Date From <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="sdate" name ="sdate"  
                onFocus='this.select();' 
                autocomplete="off"
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3'  >
                To <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="edate" name ="edate"  
                onFocus='this.select();'
                autocomplete="off" 
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3' >
                <br>
                <a href="javascript:showRepDetail()" class="btn btn-success btn-sm">Confirm</a>

            </div>
        </div>
    </div>
 
    
    <span id='uid' style='display:none;'> @userid</span>
    <span id='stc' style='display:none;'> @stc</span>     
    <span  style='display:none;'> @sdate</span>
    <span  style='display:none;'> @edate</span>    
    <span  style='display:none;'> @branch1</span>
    <span  style='display:none;'> @branch2</span>    
    
<div class='row'> 
    <div id='detDiv'>

        <div class='row'> 
            <div class='col col-md-12'>             
                #summary          
            </div>                
        </div>

        <table  id='outputfile' class='table table-bordered table-condensed' style='font-size:14px;' border='2'>
        <thead bgcolor='#5a9bd5' style='color:#ffffff;text-align:center;'> 
 
            <tr  >
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>No.</td>
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Store ID</td>            
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Store Name</td>            
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Bank Name</td>            
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Bank Account</td>

            <td colspan='3' align='center' class='thcenter tableexport-string' valign='middle'>Order Transaction</td>
            <td colspan='3' align='center' class='thcenter tableexport-string' valign='middle'>Paid</td>
   	  		
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Ticket Date	</td>
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Ticket POS No.</td>
            <td  rowspan=2 align='center' class='thcenter tableexport-string' valign='middle'>Amount</td>
             
            </tr>

            <tr > 
            <td>Order ID </td>
            <td>Date </td>
            <td> Time </td>
            <td>Date </td>
            <td>Time   </td>
            <td>Transaction Amount </td>
            </tr>

        </thead> 
        <tbody>
 
        
$SQL

 exec CLOUDPOS..rep_EorderingbyPaymentQR  '@userid' ,'@stc' ,'@branch1', '@branch2', '@sdate' , '@edate'
$BDY  
	 													
 
<tr class=''>    
    <td align='left' class='tableexport-string'>^SEQ^</td>
    <td align='left'  class='tableexport-string'>^STMERCH^</td>    
    <td align='left'  class='tableexport-string'>^STNAME^</td>    
    <td align='left'  class='tableexport-string'>^BANKNAME^</td>    
    <td align='left'  class='tableexport-string'>^BAMKACC^</td>    
    <td align='left'  class='tableexport-string'>^SO_ORDERID^</td>    
    <td align='left'  class='tableexport-string'>^SO_DATE^</td>       
    <td align='left'  class='tableexport-string'>^SO_TIME^</td> 
    <td align='left'  class='tableexport-string'>^PM_DATE^</td> 
    <td align='left'  class='tableexport-string'>^PM_TIME^</td>  
    <td align='right' class='numeric tableexport-string'>^PM_AMOUNT^</td>
    <td align='left'  class='tableexport-string'>^TK_DATE^</td> 
    <td align='left'  class='tableexport-string'>^TK_TICKETNO^</td>  
    <td align='right' class='numeric tableexport-string'>^TK_AMOUNT^</td> 
</tr>
$ERX

$CMP
 

$END
</tbody>
</table>            
</div>
</div>

</div>

<br>
<a href='javascript:printPKP();' 
class='btn btn-primary pull-right printbtn hideme'><span class="fa fa-wpforms"></span> พิมพ์</a>
<br><br> 

 

$TAB
|XID #summary
|HDR
<center><h2>E-Ordering By QR Payment</h2></center> 
|SQL 

exec CLOUDPOS..rep_EorderingbyPaymentQR_Header  '@userid' ,'@stc' ,'@branch1', '@branch2', '@sdate' , '@edate'
|BDY
<center>
<h4><b>Parameter : </b> 

Branch: ^BRANCH1^ -  ^BRANCH2^  /
Date : ^SDATE^ - ^EDATE^/     
</h4>
</center> 
<hr>
|ERX
|END

 
 



$TAB 
|XID #storebranch1
|HDR
|SQL
 
exec CLOUDPOS..rep_loadBranch '@stc','@userid'
 
|BDY
<option value=':00'>:01</option>
|END



$TAB 
|XID #storebranch2
|HDR
|SQL
 
exec CLOUDPOS..rep_loadBranch '@stc','@userid'
 
|BDY
<option value=':00'>:01</option>
|END




//END*****/