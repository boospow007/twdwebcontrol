var waitimg = "<center><img src='/images/loading.gif' style='width:10vh;'></center>";


function pageReady() {

    //https://www.jqueryscript.net/form/jQuery-Plugin-For-Selecting-Multiple-Elements-Multiple-Select.html
    $("#multistore").multipleSelect({

        // placeholder text
        placeholder: 'Please select branch',

        // e.g. 'en-us'
        locale: undefined,

        // displays the Select All checkbox
        selectAll: true,

        // allows to select a single option
        single: false,

        // shows a radio button when single is set to true
        singleRadio: false,

        // allows multiple selection
        multiple: false,

        // hides checkboxes for Optgroup
        hideOptgroupCheckboxes: false,

        // width of the item
        multipleWidth: 80,

        // width
        width: 300,

        // dropdown width
        dropWidth: undefined,

        // max height
        maxHeight: 250,
        maxHeightUnit: 'px',

        // or 'top'
        position: 'bottom',

        displayHtml: false,

        // displays selected values
        displayValues: false,

        // displays titles
        displayTitle: false,

        // custom delimiter
        displayDelimiter: ', ',

        // used to display how many items are selected
        minimumCountSelected: 3,

        // add ... after selected options
        ellipsis: true,

        // is opened on init
        isOpen: false,

        // keeps the select always be opened
        keepOpen: false,

        // opens the select on hover
        openOnHover: false,

        // container element
        container: null,

        // displays a search box to filter through the options
        filter: false,

        // allows to search through optgroups
        filterGroup: false,

        // placeholder text
        filterPlaceholder: '',

        // selects the option on enter
        filterAcceptOnEnter: false,

        // filter by data length
        filterByDataLength: undefined,

        // shows clear icon
        showClear: false,

        // animation type
        animate: 'none',

        // custom filter function
        customFilter: function customFilter(label, text) {
            // originalLabel, originalText
            return label.includes(text);
        },

        formatSelectAll: function () {
            return '[Select all]';
        },

        formatAllSelected: function () {
            return 'All selected';
        },

        formatCountSelected: function () {
            return '# of % selected';
        },

        formatNoMatchesFound: function () {
            return 'No matches found';
        },

        styler: function () {
            return false;
        },

        textTemplate: function ($elm) {
            return $elm.html();
        },

        labelTemplate: function ($elm) {
            return $elm.attr('label');
        }

    });

    // Uncheck all checkboxes. 
    $('#multistore').multipleSelect('uncheckAll');

    //////////////////////////////////////////////////////////////
    $('#sdate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'
    });

    $('#edate').datepicker({
        format: "yyyymmdd",
        todayBtn: true,
        language: "th",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        endDate: '0d'

    });

    //---convert numeric format
    $('.numeric').each(function () {
        var val = $(this).text();
        val = val.replace(/,/g, "");
        $(this).css('align', 'right');
        val = val.trim();
        if (val.length > 0)
            $(this).html(numericFormat(val))
    });

    //-----
    /* default charset encoding (UTF-8) */

    TableExport.prototype.charset = "charset=utf-8";
    var tableExporter = $('#outputfile').tableExport({
        formats: ["xlsx"],
        bootstrap: false,
        exportButtons: true,
        filename: 'รายงานยอดขาย Online At Store ที่ยอดเงินโอนเข้าสำนักงานใหญ่ (2C2P)',
        ignoreRows: null,
        ignoreCols: null,
        trimWhitespace: true,
        RTL: false
    });
    tableExporter.typeConfig.date.assert = function (v) {
        return false;
    };

} //pageReady




function showRepDetail() {

    //alert('Selected values: ' + $('#multistore').multipleSelect('getSelects'));

    var userid = readCookie('WUSERID');
    var st = readCookie('WDFSTORE');

    var t1 = $('#sdate').val();
    var t2 = $('#edate').val();
    var bscm = $('#multistore').multipleSelect('getSelects');

    if (bscm == '') {
        alert('กรุณาเลือกสาขา');
        return;
    }

    //console.log(bscm);
    //var bs = bscm.toString().replace(/,/g, '|');   ///  |
    var bs = bscm.toString(); ///// ,
    //console.log(bs);       
    showDetail(userid, st, t1, t2, bs);
}


function showDetail(u, s, t, t2, branches) {
    try {

        var ttm = t;
        var ttm2 = t2;
        var xMax = 800,
            yMax = 600
        if (window.screen) {
            xMax = window.innerWidth;
            yMax = window.innerHeight;
        }
        yMax = yMax - 30;
        var url = "/DPAGE?" + btoa("fname=REPORT/Rep_SalesOnlineAtStore.js&sdate=" + ttm + "&edate=" + ttm2 + "&userid=" + u + "&multibrnch=" + branches + "&stc=" + s);

        window.open(url, 'REPDETAIL', 'scrollbars=yes,width=' + xMax + ',height=' + yMax + ',top=0,left=0,toolbar=no, menubar=no,location=no, resizable=yes');
    } catch (ex) {
        alert(ex.message);
    }
}

function printPKP() {

    var data = $('#detDiv').html();
    var mywindow = window.open('', 'mydiv', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/style.css" type="text/css"  media="print" charset="utf-8" />');
    mywindow.document.write('<link rel="stylesheet" href="../stylesheets/bootstrap.min.css" />');
    mywindow.document.write('<style type="text/css">.print-slip {font-family: "courier_monothai";  font-size:12px; }  ');
    mywindow.document.write('  .hideme {display:none;}  #conditionHdr { display:none; }  #conditionDiv {  display:none; }  .printbtn {display:none; } ');
    mywindow.document.write('  .xlsx {display:none; } ');
    mywindow.document.write(' </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    }, 1000)
}
/*****


$HDR

<script>
 
</script>
<style>

.hideme {
    display:none;
}
option {
    padding:3px 3px;
}
 
.TotalTable {
     font-weight: bold;
     border-bottom: 2px dotted #262626;
     background-color: #F0F0F0;
}

.Total {
     font-weight: bold;
     border-bottom: 2px solid #262626;
     background-color: #F0F0F0;
}
.GrandTotal {
     font-weight: bold;          
     border-bottom-style: double;
     border-bottom-width: medium;
     border-bottom-color: #262626;
     background-color: #F0F0F0;
}
.thcenter {
    text-align:center;    
    valign:middle;
    vertical-align: middle;
    
}

</style>

<div class='container-fluid'>
 
    <h4  id='conditionHdr'><b>เงื่อนไขการแสดงข้อมูล</b></h4>
    <div  id='conditionDiv'  class='well' style='font-size:14px;' >                 
        <div  class='row'>                   
            <div class='col-md-3' >
                Select Branch  <br>   
                <select  id="multistore"  >                    
                    #storemultiple
                </select>
            </div>                                                
            <div class='col-md-3' >
                Date From <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="sdate" name ="sdate"  
                onFocus='this.select();' 
                autocomplete="off"
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3'  >
                To <br> <input style="margin-top: 0px;" type="text" class="form_datetime form-control" 
                id="edate" name ="edate"  
                onFocus='this.select();' 
                autocomplete="off"
                maxlength="8"  placeholder="format : yyyymmdd"  value="">
            </div>
            <div class='col-md-3' >
                <br>
                <a href="javascript:showRepDetail()" class="btn btn-success btn-sm">Confirm</a>
            </div>
        </div>
    </div>
 
    
    <span id='uid' style='display:none;'> @userid</span>
    <span id='stc' style='display:none;'> @stc</span>     
    <span  style='display:none;'> @sdate</span>
    <span  style='display:none;'> @edate</span>       
    
<div class='row'> 
    <div id='detDiv'>

        <div class='row'> 
            <div class='col col-md-12'>             
                #summary          
            </div>                
        </div>

        <table  id='outputfile' class='table table-bordered table-condensed' style='font-size:14px;' border='2'>
        <thead bgcolor='#5a9bd5' style='color:#ffffff;text-align:center;'> 
               
            <tr>           
            <td>BU Code</td>
            <td>Company Code</td>
            <td>Oracle Branch Code</td>
            <td>POS Store Code</td>
            <td>Merchant Id</td>
            <td>Business Date</td>
            <td>Transaction Date</td>
            <td>Transaction Time</td>
            <td>Sale Type</td>
            <td>Log No</td>
            <td>Cashier ID</td>
            <td>Cashier Name</td>
            <td>Unique Receipt No</td>
            <td>Void/Return Reference No</td>
            <td>Ticket No</td>
            <td>Ref Orig Display Receipt No</td>
            <td>Tender Type</td>
            <td>Tender Type Map</td>
            <td>Card No</td>
            <td>Amount</td>
            <td>Source System</td>
            </tr>
        </thead> 
        <tbody>           
$SQL
    exec CLOUDPOS..Rep_EorderingbyPayment2C2P  '@userid' , '@multibrnch' , '@sdate' , '@edate'
$BDY  	 												
<tr class=''>     
    <td align='left' class='tableexport-string'>^BU Code^</td>
    <td align='left'  class='tableexport-string'>^Company Code^</td>    
    <td align='left'  class='tableexport-string'>^Oracle Branch Code^</td>    
    <td align='left'  class='tableexport-string'>^POS Store Code^</td>    
    <td align='left'  class='tableexport-string'>^Merchant Id^</td>    
    <td align='left'  class='tableexport-string'>^Business Date^</td>    
    <td align='left'  class='tableexport-string'>^Transaction Date^</td>       
    <td align='left'  class='tableexport-string'>^Transaction Time^</td> 
    <td align='left'  class='tableexport-string'>^Sale Type^</td> 
    <td align='left'  class='tableexport-string'>^Log No^</td>  
    <td align='left'  class='tableexport-string'>^Cashier ID^</td> 
    <td align='left'  class='tableexport-string'>^Cashier Name^</td> 
    <td align='left'  class='tableexport-string'>^Unique Receipt No^</td> 
    <td align='left'  class='tableexport-string'>^Void/Return Reference No^</td> 
    <td align='left'  class='tableexport-string'>^Ticket No^</td> 
    <td align='left'  class='tableexport-string'>^Ref Orig Display Receipt No^</td> 
    <td align='left'  class='tableexport-string'>^Tender Type^</td> 
    <td align='left'  class='tableexport-string'>^Tender Type Map^</td> 
    <td align='left'  class='tableexport-string'>^Card No^</td> 
    <td align='right' class='numeric tableexport-string'>^Amount^</td>
    <td align='left'  class='tableexport-string'>^Source System^</td>  
</tr>
$ERX
$CMP 
$END
</tbody>
</table>            
</div>
</div>

</div>

<br>
<a href='javascript:printPKP();' 
class='btn btn-primary pull-right printbtn hideme'><span class="fa fa-wpforms"></span> พิมพ์</a>
<br><br> 

 


$TAB
|XID #summary
|HDR
<center><h2>รายงานยอดขาย Online At Store ที่ยอดเงินโอนเข้าสำนักงานใหญ่ (2C2P)</h2></center>   
|SQL  
exec CLOUDPOS..Rep_EorderingbyPayment2C2P_Header  '@userid' , '@multibrnch' , '@sdate' , '@edate'
|BDY
<center>
<h4><b>Parameter : </b> 

Branch: ^BRANCH^ 
<br>
Date : ^SDATE^ - ^EDATE^
</h4>
</center> 
<hr>
|ERX
|END


  
 
$TAB 
|XID #storemultiple
|HDR
|SQL
 
exec CLOUDPOS..cs_loadStoreByUser '@userid', '@stc' 
 
|BDY
<option value=':00' style='padding:3px 3px;'> :00- :01</option>
|END


//END*****/