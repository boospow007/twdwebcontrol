var currentCtlId=0;
function pageReady() {
    $('#errorDiv').hide();
  
 } 
 
 function eventDetail(ctl) {
	 currentCtlId=ctl;
	 var evn =parseInt(ctl/100);
	 $('#ctlHead').text('Control Detail ID '+ctl);
	  try {
		 qualify(ctl);
		 
		 bonus(ctl);
		 
		 qualDept(ctl);
		
		 bonusDept(ctl);
		
		 bonusStep(ctl);
		
		 storeEffect(ctl)
		
	     getDetail(evn);
	    }
	   catch(ex) {
	    alert(ex.message);
	   }
 }
 
 function storeEffect(ctl) {
	 
	  try {
          var qry ="use.PLPPSRV^ SElect STCODE from BmgnControl a join bmgnEffect b on a.evnum =b.evnum ";
			 qry =qry +" Where ctlid=" + ctl ;
			 
		 var hdr =" "; //<h4>Store Efffect Set " + ctl + "</h4>";
		 var bdy=" :00 ";
		 var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$END\n";
		  ajaxExpScript(ctlf
		       ,function (bool,ret ) {
					if(bool)$('#effectdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
 }
 
function qualify(ctl) {
	 try {
         var qry ="use.PLPPSRV^ Select a.skcode,quant,bvalue,orgPrice from BmgnQproductset a left Outer join  SKUMASTER b ";
			 qry =qry +" on a.skcode=b.skcode Where ctlid=" + ctl +" order by skcode desc ";
			 
		 var hdr ="";
		 hdr 	 = " <table style='width:100%'><thead><th>SKC<th>Quant<th>Bvalue<th>UPC</thead>";
		  var bdy="<tr onclick='javascript:testThis(this);'><td>:00<td>:01<td>:02<td>:03</tr>";
		 
		  var erx=" <tr><td colspan='4'>No Data from DB";

		  var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$ERX \n"+erx +"\n$END\n</table>";

		 ajaxExpScript(ctlf
		       ,function (bool,ret) {
					if(bool)$('#qitemdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}

function bonus(ctl) {
	 try {
         var qry ="use.PLPPSRV^ Select skcode,quant,bvalue from BmgnBproductset Where ctlid=" + ctl;
		 var hdr ="<h4>Bonus Product Set " + ctl + "</h4>";
		 hdr 	 = " <table style='width:100%'><thead><th>SKC<th>Quant<th>Bvalue</thead>";
		  var bdy="<tr onclick='javascript:testThis(this);'><td>:00<td>:01<td>:02 </tr>";
	     var erx=" <tr><td colspan='5'>No Data from DB";

		 var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy  +"\n$ERX \n"+erx +"\n$END\n</table>";
		  ajaxExpScript(ctlf
		       ,function (bool,ret ) {
					if(bool)$('#bitemdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}

function qualDept(ctl) {
	 try {
         var qry ="use.PLPPSRV^ Select sp=dbo.encTag(vendor),bn=dbo.encTag(BRAND),dp=dbo.encTag(DEPART),PR_STATUS,QUANT,Bvalue from BmgnQDept Where ctlid=" + ctl;
		 var hdr ="<h4>Qualify Product Dept " + ctl + "</h4>";
		 hdr 	 =  " <table style='width:100%'><thead><th>Vendor<th>brand<th>Dep<th>Stc<th>Qnt<th>Bval</thead>";
		  var bdy="<tr><td>:00<td>:01<td>:02<td>:03 <td>:04 <td>:05  </tr>";
		  var erx=" <tr><td colspan='5'>No Data from DB";

		  var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$ERX \n"+erx +"\n$END\n</table>";
		  ajaxExpScript(ctlf
		       ,function (bool,ret ) {
					if(bool)$('#qdeptdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}

function bonusDept(ctl) {
	 try {
         var qry ="use.PLPPSRV^ Select sp=dbo.encTag(vendor),bn=dbo.encTag(BRAND),dp=dbo.encTag(DEPART),PR_STATUS,QUANT,Bvalue from BmgnBDept Where ctlid=" + ctl;
		 var hdr ="<h4>Bonus Product Dept " + ctl + "</h4>";
		 hdr 	 = " <table style='width:100%'><thead><th>Vendor<th>brand<th>Dep<th>Stc<th>Qnt<th>Bval  </thead>";
		 var bdy="<tr><td>:00<td>:01<td>:02<td>:03 <td>:04 <td>:05 <td>:06  </tr>";
		var erx=" <tr><td colspan='7'>No Data from DB";

		  var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$ERX \n"+erx  +"\n$END\n</table>";
		  ajaxExpScript(ctlf
		       ,function (bool,ret) {
					if(bool)$('#bdeptdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}

function bonusStep(ctl) {
	 try {
         var qry ="use.PLPPSRV^ Select Bstep,MinValue,MaxValue,Bvalue,Max_disc  from BmgnBStep Where ctlid=" + ctl;
		var hdr ="<h4>Bonus Volume Step " + ctl + "</h4>";
		hdr 	 = " <table style='width:100%'><thead><th>Step<th>Minval<th>MaxVal<th>Bvalue<th>MxDisc</thead>";
		var bdy="<tr><td>:00<td>:01<td>:02<td>:03 <td>:04  </tr>";
		var erx=" <tr><td colspan='5'>No Data from DB";
		var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$ERX \n"+ erx +"\n$END\n</table>";
		
		ajaxExpScript(ctlf
		       ,function (bool,ret ) {
					if(bool)$('#bstepdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}
 
function getDetail(evn) {
	 try {
         var qry ="use.PLPPSRV^Select SD=convert(char(8),SDATE,112),ED=convert(char(8),EDATE,112)"
			+" ,STATUS,MEMBER,SCHEDULE,WKDAY,MNTHDAY,EVNAME "
			+" from BMGNEVENT Where EVNUM =" + evn;
		var hdr ="<h4>Event Detail " + evn + "</h4>";
		hdr 	 =  " <table style='width:100%'>";
		var bdy ="<tr><td>SDATE <b>:00</b><td>EDATE <b>:01</b><td> ST <b>:02</b><td>MEMBER <b>:03</b></tr>";
			bdy +="<tr><td>SCHEDULE <b>:04</b> <td  Colspan='3'>WEKKDAY <b>:05</b></td></tr>";
			bdy +="<tr><td Colspan='4'>MONTHDAY <b>:06</b></td><tr><td colspan='4'>^EVNAME^</tr>";
		var ctlf = "$HDR\n" + hdr +"\n$SQL\n" + qry  +"\n$BDY\n"+ bdy +"\n$END\n</table>";
		
		ajaxExpScript(ctlf
		       ,function (bool,ret ) {
					if(bool)$('#eventDetdiv').html(ret);
					else alert(ret);
				}
		    );
		
	    }
	   catch(ex) {
	   alert(ex.message);}
}
 
var qset 	=0;
var bset 	=0;
var bby		=0;
var qsetStr = ['Q By Item','q By Set','Q By Invoice'];
var qbyStr 	= ['Volume','Spc Price','% Disc','Baht Dic'];

function byItem(opt) {
	qset=opt;
	 filter();
	
}
function bonusSet( ) {
	bset =$('#bonusSet').val();
	filter();
	
}
function bonusBy( ) {
	bby =$('#bonusBy').val();
	filter();
	
}
function showControl(qy,by){
	qset=qy;
	bset =by;
	filter();
}
function filter () {

	try {
		var pdata ={patname:"/PLPP/filterevent.txt",
				para:{stc:"920",
					qsty:qset,
					bsty:bset,
					bty:bby}};
	 
		var sUrl  ="../XPAND/FILE";  // Call expand route
		$.ajax({
			type: "POST",
			url: sUrl,
			data: JSON.stringify(pdata),
			dataType: "text",
			contentType: "application/json; charset=UTF-8",
			success: function (ret) { //return is html string
				try {
					if(ret.substring(0,3)=='ERR'){
						$('#eventDiv').html(syserror +ret);
					}else { 
						$('#eventDiv').html(ret);
					  }
				}
				catch(ex){alert( 'Err. '+ex.message);}
				 return;
	          },
			error: function (e) { 
			 alert('xpandfile Err.. '+e.responseText);
			}
		});
 
	   $('#headerLB').text('[ Qual:' + qset + '][Bonus:'+bset + ']');
	    
		
	}
	catch(ex) 
	  {
	   alert(ex.message);
	   }
}
function ctlKeyPress(ev) {
 if(ev.charCode==13){
		var ctl =$('#inpctlid').val();
		eventDetail(ctl);
         }
}
function openDeptDetail(){
	 //var url="/DPAGE?fname=PLPP/QBbyDept.js&ctlid="+currentCtlId;
	 var url="http://twpim.com:81/DPAGE?fname=PRODUCT/QBbyDept.js&ctlid="+currentCtlId;
	 /*
	 url=url.replace('@spc',spc);
	 url=url.replace('@spc',spc);
	 url=url.replace('@spc',spc);
	 */
	// var url='./DPAGE?'+pat;
	 //window.open(url,'possale' );
	 window.open(url,'DETAIL','toolbar=no, menubar=no,location=no, resizable=no' );
}
function testThis(obj){
	//alert('show This');
		 try {
			 var skc=$(obj).find('td:eq(0)').text();
			 
		   //  var  url="http://twpim.com:81/S/"+skc;
			  
			//  window.open(url,'SCAN');
		    }
		   catch(ex) {
		     alert(ex.message);}
	 } 
/***********
$HDR
<style>
#dataTab tr:hover {
	background-color:silver;
	cursor:pointer;
}
.dataTab tr:hover {
	background-color:silver;
	cursor:pointer;
}
.wrap {
	white-space: nowrap; 
	text-overflow:ellipsis; 
	overflow: hidden;
	max-width:100px;
}
.rndDiv{
		border:2px solid #a1a1a1;
		background: #A0D0D0;
		padding: 10px 40px;
		color:black;
	 	text-align:left;
		width:500px;
		max-height:200px;
		overflow-y:scroll;
		 overflow-y:auto;
		border-radius: 12px;
     }
</style>
<div class='containner-fluid'>
<br>  
<div id ='errorDiv' style='width:100%;color:crimson;text-align:center;'>
  Warning ... Javascript is not Loaded Properly !!!
</div>
 

<div class='row'>

<div id ='eventDiV' class='col col-md-7'>
<h3>Summary of PLPP on Server [#server]</h3>
<div style='width:100%;height:200px;overflow:scroll' >
#sumevent
</div>
 
<h3><label id='headerLB'>Active BMGN Event</label></h3>

<div id ='eventDiv'>
 <table id ='dataTab' class='table table-condensed'>
 <thead><th>Ctlid</th><th>Ename</th>
 <th>QSetTy<th>QBby <th>QLine<th>QbyQ <th>QAmt
 <th>BSetTy<th>BTyp<th>BbyQ <th>BonBy
 //<th>pmcode
 </thead>
$SQL

use.PLPPSRV^
Select top 100   c.Ctlid, ename=  rtrim(a.EVNAME)
       ,qsettype,QBonusby,QbyLine,QbyQuant,Qamount 
       ,bsettype,bbyType,BbyQuant,BBonusBy,pmcode 
	   Into #event
  from [dbo].[BMGNEvent] a		
  inner join  BMGNEFFECT  f on a.EVNUM=f.EVNUM 
			and STCODE='@stc' 
  inner join  BMGNCONTROL  c on a.EVNUM=c.EVNUM 
			and getdate()  Between SDATE and EDATE
//			--and (     substring(WKDAY,1,1)='Y'
//			--      or  substring(MNTHDAY,@mdy,1)='Y')
//   			--And '@sTime' > MinTime
//			--And '@sTime' < MaxTime
where  a.STATUS = 'A'
order by c.CTLID DESC

Select * from #event
order by ctlid desc
 //------???wonder ?? order by Desc is not work in sqlsrv ??????
$BDY
<tr onclick="javascript:eventDetail(':00')"  >
<td>:00<td  class='wrap'>:01
<td>:02<td>:03<td>:04<td>:05
<td>:06<td>:07<td>:08<td>:09
<td>:10
//<td>:11
$END
 </table>
</div>

  Click Item to see Detail
</div>

<div class='col col-md-5'>
 <h3><label id ='ctlHead'>Control Detail </label></h3>
Enter Control Number:
<input type='text' onkeypress='javascript:ctlKeyPress(event)' id ='inpctlid' value=''>

   <div class='row'>
		<div id ='eventDetdiv'   class='rndDiv'>
		
		</div>

		<b>QUALIFY Product:(BMGNQproductSet)</b>
		<div id ='qitemdiv'   class='rndDiv'>
		Qualify  Item
		</div>

		<b>QUALIFY by DEPT:(BMGNQDEDEPT)</b><br>click for Detail
		<div id ='qdeptdiv' class='rndDiv' onclick='javascript:openDeptDetail()'>
		Qual Depart
		</div>
		<b>BONUS Product:(BmgnBproductset)</b>
		<div id ='bitemdiv' class='rndDiv'>
		Bonus Item
		</div> 

		<b>BONUS By DEPT:(BmgnBDept)</b>
		<div id ='bdeptdiv' class='rndDiv'>
		Bonus Depart
		</div>

		<b>BONUS STEP:(BmgnBStep)</b>
		<div id ='bstepdiv' class='rndDiv'>
		Bonus Step
		</div>

		<b>EFFECTIVE at STORE:</b>
		  <div id ='effectdiv' class='rndDiv'>
		 Store Effect
		</div>
	</div>

</div>
</div>
</div>

$TAB
|XID #sumevent
|HDR
<table class='dataTab' style='width:100%'  >
<thead><th>QTY<th>BTY<th>CNT
<th>Desp<th>Procedure
 
</thead>
|SQL
use.PLPPSRV^ 
declare @sum table (qty char(2),bty char(2) ,cnt int,id int  identity(1,1) )
insert @sum(qty,bty,cnt)
select  qsettype
, bsetType
, count(*)
From bmgnControl a inner join BMGNEVENT b on a.EVNUM=B.EVNUM 
where b.STATUS='A'
group by qsettype, bsetType
 order by qsettype, bsetType

Select  a.qty,a.bty,cnt
,nme=isnull(name,'?? Unknown ??')
,prc=isnull(pname,'!! Not Support yet !!')
from @sum a left outer join PLPPType b on a.qty=b.qty and a.bty=b.bty
order by id
|BDY
<tr onclick='javascript:showControl(^qty^,^bty^)'><td> ^qty^ <td> ^bty^ <td align='right'>^cnt^ 
<td>. ^nme^<td>^prc^
|END
</table>
 
$PRE
use.PLPPSRV^
Select [@sdate]=convert(char(8),getdate(),112)
,'@stc'='920' 
,[#server]=@@SERVERNAME

$XXX
 /*

*/
 //**END ***/