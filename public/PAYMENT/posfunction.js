  
var tendorType="Tendor is not Defined";
var reloadWarn =0;
var cloudConnect=0;
var totalAmount = 0;
 
var discount ={	name:"Manager",
				auth:"NONE",
				dable:0,
				perc:0,
				amnt:0}

function startPage() {
    history.forward();  //--suppose to eliminate Backward action
    
    try {
        $(".numOnly").on('focus', function () {
            $(this).select();
        });

	/*
	    //-----------ask before leaving the page add this to every edit form
       $(window).bind('beforeunload', function(){
      //    return "Do you really want to leave now?";
    	   if ( reloadWarn==1) {
    		   return("Save Sales Item first");
    	     }
         });
        //---------Chrome debug will disable
        $(window).bind('keydown', function (e) {
            if (e.keyCode >= 112  ) {
                functionKey(e.keyCode);
                e.preventDefault(); //disable   key that >112-->include thai character
                $('#checkno').focus();
            }
            var st=$('#receiptDiv').is(':visible');
			if(st)location.reload();
		});
		 
*/
		//location.reload();
    }
    catch (ex) {
        alert(ex.message);
    }
    //  alert("page Ready");
   pageReady();

    /**/
}

function pageReady() {
//	alert("READY");
try {
   
     //---ckeck User In 
   $("#salesDiv").scrollTop($("#salesDiv")[0].scrollHeight);

    $('#barcode').focus();
   // ----When modal on, set fofus on this
    $('#paymentDiv').on('shown.bs.modal', function () {
        $('#PpayAmt').focus();
    })
    //------search PRoduc
    $('#posUpProduct').bind('show', function () {
        $('#searchProd').focus();
    })
    //------search PRoduc
  //  $('#popUpCustomer').on('show', function () {
   //     $('#searchCust').focus();
	//})
	
  $('#popUpPM').mouseleave(function () {
        $('#popUpPM').hide();
    });
  
    $('#popUpProduct').mouseleave(function () {
        $('#popUpProduct').hide();
    });
    

    $('#popUpCustomer').mouseleave(function () {
        $('#popUpCustomer').hide();
    });
  
    $('#showDiv').mouseleave(function () {
        $('#popUpImage').hide();
        $('#editMenu').hide();
    });
   
    currentTime();
	//-----load tendor to payment ethod
	loadTendor();
    showSItem();
}
catch(ex) {alert(ex.message);}
}


//------load tendor
function loadTendor(){
	qry="select top 5 TENDOR,TNAME  from CLOUDPOS..TENDORS order by PRIORITY";
	hdr= "<select id ='PpayPmc' name ='PpayPmc' >"
	bdy ="<option value=':00' >:01</option>";
	end ="</select>";
	var xdata ={
			   "hdr":hdr,
			   "qry":qry,
			   "bdy":bdy,
			   "end":end,
			   "erx":"Err... @error"
	   };
	ajaxXpandPat(xdata, function (bool,ret ) {
	    	 if (bool) {           
	    		 tendorType=ret;
	           }
	         else
	             alert(ret);
	 		});
	
}
//-------------Map Key Input
function functionKey(key) {
    	    //---if modal is show then return
    	    if ($('#paymentDiv').hasClass('in')) return; //--if modal is show

    	  switch(key){
    	 
			case 112:  //F1
			       
    	    		return(null);  //Usually use by Web Browser
    	  		break;
    	    case 113:  //F2 Payment
    	    		paymentCmd();
    	    		break;
       	    case 114:  //F3 Manager Discount
			//	checkPmCmd();
				addDiscount();
	    		break;
	    		
     	    case 115:  //F4 Show last REceipt
	    		detailRcpt(null);
	    		break;
       	    case 116:  //F5 Temporary Save
	    		save2Temp();
	    		break;
       	    case 117:  //F6 Recall from Temp
	    		recallTemp();
	    		break;
       	    case 119:  //F8 Diaply Item Incart
	    		itemInCart();
	    		break;	
       	    case 120: //F9 --display unpaid Guest Bill
       	    	checkBill();
       	    	break;
    	    case 121:  //F10
    	    		clearItem();
					break;
			/*		
    	     case 122:  //F11 --find customer
    	    		customer();
    	    		break;
    	  case 123:  //F12 --find Product
    	      showProd();
			  break;	
			  */
    	    default:
    	    		break;
    	    }
    	}
     
//-------------Clear All ITems  
 function clearItem() {
  try { 
	  if(confirm("Delete all sales item?")==0)return;

	  var qry ="Delete from CLOUDPOS..xtrans where stc ='"+storeCode +"' and   lck ='"+lockNumber +"'"; 
	  ajaxGetQuery(qry,function(bool,ret){
		   if(bool)showSItem();
		   else showInfo(ret);
	   });

    	 } 
 catch(ex) {alert(ex.message);}
 }
 
//-------------------------
function keySRPPress(ev) {
   // var x = ev.keyCode;
   var src = $('#searchProd').val();
    if (src.length>=3) {
        productSearch(src);
    }
}

 
 
//--------FInd an Add Item to Sales ITEm
function findAndAdd(prc) {
	try { 
    var qry = "exec  CLOUDPOS..ws_addxtrans '" + storeCode + "','" + lockNumber + "','" + prc + "',1  ";
    ajaxGetQuery(qry, function (bool,ret ) { //--ret is XML colum is RES
        if (bool) {
        	 var jsn =JSON.parse(ret);
        	 //alert(jsn.dbmessage);
        	 if(jsn.dbcode>0){
        		 showInfo(jsn.dbmessage);
        		 $('#barcode').val('');
        		 return;
        	    }
        	 showSItem(); //-----Show All Sales Item
        	  $('#barcode').val('');
             }
         else {
                showInfo(ret);
        	   // upfAlert(ret);
                $('#barcode').val('');
              }
        });
    
    $('#barcode').focus();
    }
    catch(ex) {alert(ex.message);}
}
 
//-------sahow SaleItems
function showSItem() {
	 $('#popUpImage').hide();
	 $('#editMenu').hide();
	 var pdata ={patname:'PAYMENT/saleitem.txt',
				para:{stc:storeCode,
				lck:lockNumber,
				cref:sorder.id}};

	ajaxXpandFile(pdata,function(isok,ret){
		$('#salesDiv').html(ret);   
		var stotal =$('#subtotal').text();

		discount.dable =$('#discable').text();

		discount.amnt=discount.dable*discount.perc/100;
		totalAmount	=stotal -discount.amnt;

		$('#discper').text(discount.perc);
		$('#discamt').text(discount.amnt);
 
		$("#gtotal").text(totalAmount )
		//---convert nemeric display
		setNumericFormatDiv('salesDiv');
		 
	    reloadWarn= parseInt(totalAmount) >0;
	})
	 
  }

//---display Item in cart image
var iteminimage=0;

function itemInCart(){
	//showSItem();
	return;
 
}
//-----------Add manager discount
function addDiscount(){

	 if( $('#addDicountDiv').length==0){
	//	alert('Add discount');
		 //-------loaf discount div
		 var url ='/PAYMENT/adddiscModal.htm';

		 $.ajax(url).done(function(data) {
			$("body").append(data);
			formDLoaded();
		  }).fail(function(data){
			alert("Ajax Error! " +data);
		  });
 
	 }
	else
	//$('#discModalDiv').modal('show');
    formDLoaded();
	   
}

//***********for payament*****************
function cancelCmd() {
	window.close();
}
function paymentCmd() {
	//showSItem();
	setTimeout(start2pay,100);
}

function start2pay(){ 	
try {
  // alert('start to pay')
   // dscAmt = $("#discLine td:eq(1)").text();
    if ((totalAmount==0) || (totalAmount==null)) {
	   alert("NO Sales Item",function(ret){
	    	 return;
	    });
	    return;
   }

   discount.perc=$('#discper').text();
   discount.amnt=$('#discamt').text();
   totalAmount = $("#gtotal").text();
  // alert(totalAmount)
   //----------Check if paymentModal iis loaded
   if($('#paymentModalDiv').length==0){
			var url ='/PAYMENT/payment.htm';

			$.ajax(url).done(function(data) {
			$("body").append(data);
			formPLoaded(totalAmount);

			$("#eorderid").text(sorder.id);

			}).fail(function(data){
				alert("Ajax Error Loading ! " +url);
			});

   }
   else
	  formPLoaded(totalAmount);
 
  }
   catch (er) { showInfo(er.message); }
}
 
//--------------Barcode scan ine
function keyBCPress(ev) {
 
    var x = ev.keyCode;
    if (x == 13) {
    	var prcode= $('#barcode').val();
    	findAndAdd(prcode);
      }
   
}
//--------------Barcode scan ine
function keyCKPress(ev) {
 
    var x = ev.keyCode;
    if (x == 13) {
    	var ref= $('#checkno').val();
		getCheckBill(ref)
      }
   
}

 
function showInfo(str) {
    $('#errorDiv').html(str);
    $('#errorDiv').show();
    setTimeout(function(){ $('#errorDiv').hide(); },3000); //clear message in 3sec
   //$('#errorDiv').show();
  // webAlert(str);
}
/****
function logOut() {
    var mdal = $("#popupModal");
    $('#mdHeader').html("LOGOUT");
    $("#mdBody").text("Confirm Logout");
    $("#myBtnOK").show();
    $("#myModal").modal();
    $("#myBtnOK").unbind();
    $("#myBtnOK").click(function () {
        location.assign("mainmenu,php");
    });
    $("#myBtnCC").unbind();
    $("#myBtnCC").click(function () {
        $("#popupModal").modal("hide");
    });
}
**/
 
//---------------save to Temporary table
function save2Temp(){
	//alert('Save to temp');
	try { 
	 
	var sUrl="../POSSRV/SAVE/"+ storeCode+ "/" +lockNumber;
//alert(sUrl);
	ajaxGetUrl(sUrl,function(isok,resp){
		if(!isok)alert( "Error " +isok); 
		else {
			var jsn=JSON.parse(resp);  //--if U use ajxCall then result is in String
			var ecode=jsn.dbcode;		// U have to encode it back to json
			if(ecode==0){
				var msg=jsn.dbitems[0].MSG;
				upfAlert(msg,function(){
					reloadWarn=0;  //---dont warning when reoload
					location.reload();
				});
			  }
			else alert(jsn.dbmessage);
			}		
	});
	/**** erro dont fire!!!
	$.get(sUrl,
	  function(resp){
		var ecode=resp.dbcode;
		if(ecode==0){
			var msg=resp.dbitems[0].MSG;
			upfAlert(msg,function(){
				reloadWarn=0;  //---dont warning when reoload
				location.reload();
			});
		  }
		else alert(resp.dbmessage);
		}
	   ).fail(function( jqxhr, settings, exception ) {
  		alert( "System Error Can not save to temporary " +sUrl);
	   } 
      );
        */
	}
catch(ex){alert(ex.message);}
	
}
//---------------save to Temporary table
function recallTemp(){
	//----check if current input is not empty
	   var totAmt = $("#totalLine td:eq(2)").text();
	   if(parseInt(totAmt) > 0 ){
	        alert("Finished Sales First");
	        return 
	       }
	try {  
	 
	var sUrl="../POSSRV/RECALL/"+ storeCode+ "/" +lockNumber;
 
	$.get(sUrl,
	    function(resp){
		var jsn =resp;  //JSON.parse(resp);//!!!!if u direct get then is is reutnr in json
		var ecode=jsn.dbcode;
		if(ecode==0){
			showSItem();
		  }
		else alert(jsn.dbmessage);
		}
	   ).fail(function( jqxhr, settings, exception ) {
  		alert( "System Error Can not save to temporary " +sUrl);
	   } 
	   );
	}
	catch(ex){alert(ex.message);}
}
//-----Current Time
function currentTime() {
    var nd = new Date;

    $('#cdate').html(nd.toLocaleString());
    if(nd.getSeconds()==0)chekCloudSrv();  //1 minute interval
    setTimeout(currentTime, 1000);
}
//-----Check Cld server
function chekCloudSrv() {
	/*
    $.get("./POSSRV/RMCNN",function(data){
   	   cloudConnect=1;
    }).fail(function(){
       cloudConnect=0;
     }
    );
   */
    
}
//--Popup receipt when sales is donw
//var lastReceipt=nul;
function detailRcpt(rcpt){
	try{
		var printrcv =true;
		if(rcpt==null){
			rcpt=lockNumber+'%' ;  //----to get last receiept
			printrcv =false;
		}
	    var pdata={
	    		patname:'/PAYMENT/rcptdetail.txt',
	    		para:{
	    			stc:storeCode,
	    			ref:rcpt
	    			}
	    	}
	    ajaxXpandFile(pdata,function(isok,data){
			console.log(' Print RCP return ' +isok) 
			
			$('#receiptDiv').html(data);
			$('#receiptDiv').show();
			 setNumericFormatDiv('receiptDiv');

			//--------Print this Division
			//if(printrcv)   //tot test printer
			 setTimeout(printRcpt('receiptDiv'),500);
			//----------------------
			//setTimeout(function(){
			//	// $('#receiptDiv').hide();
			//	location.reload();
			// },12000);
			//lastReceipt=rcpt;
 	    })
 
	}
	catch(ex){alert(ex.message);}
}
function hideDetail(){
	//location.reload();
	 
}
function closeThis(){  //---from receiptDetail
	location.reload();
}	 
  
 
//---------to print to printer
function printRcpt(div) {
	 w=window.open();
	 w.document.write($('#'+div).html());
	 w.print();
     w.close();
     location.reload();
}
 