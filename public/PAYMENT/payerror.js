var wait = "<img src='../images/wait.gif' >";

function pageReady() {
	if (profileList.indexOf('0044') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู Payment Error');
		location.assign("/");
  }

  $(function () {
    $("#sdate").datepicker({
      format: "yyyymmdd",
      todayBtn: true,
      language: "th",
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      //,endDate: '0d'
    });

    $("#edate").datepicker({
      format: "yyyymmdd",
      todayBtn: true,
      language: "th",
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      //,endDate: '0d'
    });
  });

  var tb = $("#orderTab").DataTable({
    bPaginate: false,
    bSort: true,
    bFilter: false,
    bInfo: false,
  });

  tb.order([0, "desc"]) // REF
    .draw();

  $("#orderstore").multiselect({
    buttonWidth: "100%",
    enableFiltering: true,
    includeSelectAllOption: true,
    inheritClass: true,
    nonSelectedText: "เลือกสาขา",
    maxHeight: 400,
  });

  $("#orderTab tbody").on("click", "tr", function () {
    if ($(this).hasClass("selected")) {
      $(this).removeClass("selected");
    } else {
      tb.$("tr.selected").removeClass("selected");
      $(this).addClass("selected");
    }
  });

  var beginref = $("#orderTab tr:eq(1) td:eq(0)").text().trim();
  if (beginref == "No data available in table" || beginref == "") {
    return;
  }

  var obj = $("#orderTab tr:eq(1)");
  var beginstore = $("#orderTab tr:eq(1) td:eq(2)").text().trim();
  if (obj) showdetail(beginref, beginstore);
}

function searchSorder() {
  try {
    // storeCode = $("#orderstore").val();
    var stccookie = readCookie("WDFSTORE");
    storeCode = $("#orderstore").val() == null ? stccookie : $("#orderstore").val();
    userId = readCookie("WUSERID");

    var ssdate = $("#sdate").val();
    var sedate = $("#edate").val();
    var sstatus = $("#orderstatus").val();
    var tsearch = $("#smartsrch").val();
    var tref = $("#smartref").val();

    sstatus = sstatus.trim();

    $("#mainDiv").html(waitgif);
    //--------Create Pattern inside

    var pdata = {
      patname: "PAYMENT/payerror.js",
      para: {
        stc: userStore,
        store: storeCode,
        userid: userId,
        lockno: "",
        srhdate: ssdate,
        srhenddate: sedate,
        textsrch: tsearch,
        textref: tref,
        sostatus: sstatus,
      },
    };
    xpand2div(pdata, "mainDiv");
  } catch (ex) {
    alert(ex.message);
  }
}

function xpand2div(pat, div) {
  try {
    ajaxXpandFile(pat, function (isok, ret) {
      if (isok) {
        $("#" + div).html(ret);
        setNumericFormatDiv(div);
        if (div == "mainDiv") pageReady();
      } else alert("Ajax call Error :" + ret);
    });
  } catch (ex) {
    alert(Ex.message);
  }
}

function showdetail(ref, stc) {
  $("#detailDiv").html(wait);

  var pdata = {
    patname: "PAYMENT/payerrordetail.txt",
    para: { ref: ref, stc: stc },
  };
  xpand2div(pdata, "detailDiv");
}

/***  
 
$HDR
<style>
 div , table {  	 
        font-size: 12px ;	 
    }

.table tbody tr:hover {
       background-color:silver;
       cursor:pointer ;
    }
 
#pmerr .dtl , #pmerr th {    
    border: 1px solid #cccccc;
}
 </style>

<div class='container-fluid' >

<h4>Payment Error/Incomplete</h4>
<div class='row'>
        <div class='col-md-4' >
				สาขา <br>                  
				<select id='orderstore' multiple>              
					#sbranch
				</select>
		</div>
		<div class='col-md-3' >
			ค้นหาลูกค้า หรือ เบอร์โทร  
			<input type='text'  id='smartsrch'  
				class='form-control' 				
                autocomplete="off"				
				placeholder='ชื่อลูกค้า หรือ เบอร์โทรศัพท์'
				maxlength='100' value ='' >
		</div>
		<div class='col-md-3' >
			เลขที่คำสั่งซื้อ
			<input type='text'  id='smartref'  
				class='form-control' 
				autocomplete="off" 
				placeholder='เลขที่คำสั่งซื้อ'
				maxlength='50' value ='' >
		</div>
        <div class='col-md-2' style='display:none;' >
				สถานภาพ <br>                  
				<select class='form-control' id='orderstatus' >                    
					#sstatus
				</select>
		</div>

	</div>    
	<div class='row'>
			<div class='col-md-2' >
				วันที่สั่งซื้อเริ่มต้น <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="sdate" name ="sdate"  
				onFocus='this.select();' 
				autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
            <div class='col-md-2' >
				วันที่สั่งซื้อสิ้นสุด <br> <input style="margin-top: 0px;" type="text" 
				class="form_datetime form-control" 
				id="edate" name ="edate"  
				onFocus='this.select();' 
				autocomplete="off"
				maxlength="8"  placeholder="format : yyyymmdd"  value="">
			</div>
			
             <div class='col-md-8' >
                <br>
				<a href="javascript:searchSorder()" 
                class="btn btn-info btn-sm">&nbsp;&nbsp;&nbsp;แสดง&nbsp;&nbsp;&nbsp;</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                
                <div id='loadingDiv' style='display:inline-block;position:absolute; 
                    margin-left:10px; padding-left:10px;margin-top:2px; '>  </div>
            </div>

			 			 
    </div>
    <br> 

<div class='row'>

 <div class='col col-md-7' style='height:800px;overflow:scroll'>

     

 <table id ='orderTab' class='table'>
<thead>
<th>Ref<th>Try Ondate<th>Store <th>Paid By<th>DlvType
</thead>
$SQL

exec CLOUDPOS..cs_ListPayError '@srhdate','@srhenddate','@textsrch','@store','@textref','@sostatus','@userid'
 
$BDY
<tr onclick="javascript:showdetail('^REF^','^STCODE^')">
<td>^REF^
<td>^PTIME^
<td>^STCODE^ ^STNAME^
<td>^PAYBY^
<td>^DLVTYPE^
 
$END
</tbody>
</table>
 

 </div>

 <div class='col col-md-5'>
      <span id='detailSpn' ><h4> </h4> </span>
       <div id ='detailDiv'>
   
       </div>
    
   
</div>

</div>






$TAB
|XID #status
|HDR
Remark:
|SQL
SELECT * from CLOUDPOS..SCSTATNAME
where ST<>'' 
|BDY
[:00 :01] 
|END


$TAB 
|XID #sstatus
|HDR
|SQL 
   exec CLOUDPOS..cs_loadSysStatus  
|BDY
<option value=':00'>:00 :01</option>
|END



$TAB 
|XID #sbranch
|HDR
|SQL 
   exec CLOUDPOS..cs_loadStoreByUser '@userid', '@stc'    
|BDY
<option value=':00'>:00 :01</option>
|END


//END **/
