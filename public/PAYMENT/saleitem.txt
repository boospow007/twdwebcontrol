/***
$HDR 

<table id='salesItem' class='table'>
 <thead class='info'><th>Seq</th> <th>Qnt</th> <th>Prcode</th> <th>Name</th>  
 <th>V<th>D<th>Price</th> 
 <th>Discount</th> 
 <th>Total</th>
 <th style='display:none'>SKC
 <th>Remark
 </thead>
$SQL
//exec  CLOUDPOS..sorder2xtrans '@cref','@stc','@lck' ,1 
use CLOUDPOS
Select SEQ,QNT,PCD,PRN,VTY,DTY,UPC,DSC
,TOT =case when STT='V' then 0 else TOT end
,REM
,STT
,SKC
,ATR =case when STT='V' then 'silver' else 'white' end
,DAB =case when DTY='Y' and STT!='V' then TOT else 0.0 end
 from Xtrans 
where STC ='@stc' 
and LCK='@lck' 
order by SEQ 

$CMP 
<tr  style='background-color:#aee;color:navy;'>
 <td align='center' colspan='7'>Total</td>
 <td class='numeric'>@SUM(DSC)</td>
 <td class='numeric' id='subtotal'>@SUM(TOT)</td><td><td ></tr>  
 //------------Discount Line
 <tr class='info' >
  <td>
  <td  align='right' colspan='4'>Discountable</td>
  <td class='numeric' id='discable'>@SUM(DAB)</td>
 <td class='numeric' id ='discaut'>....</td>
 <td class='numeric' id ='discper'>0.00</td>
 <td class='numeric' id ='discamt' >0.00</td>
 <td>
//<a href='javascript:addDiscount()' class='btn btn-primary'>F3 ADD DISC</a>
 <td ></tr>  

<tr id='totalLine' style='background-color:teal;color:yellow;font-size:large'>
 <td align='center' colspan='7'>Grand Total</td>
 <td/td>
 <td class='numeric' id='gtotal'>@SUM(TOT)</td><td><td ></tr>  

$BDY 
<tr onmouseover ='menuOver(this)' class='sitems' bgcolor='^ATR^'> 
<td>^SEQ^</td>
<td>^QNT^</td>
<td>^PCD^</td>
<td>^PRN^</td>
<td>^VTY^
<td>^DTY^
<td  class='numeric'>^UPC^</td>
<td  class='numeric'>^DSC^</td>
<td  class='numeric' >^TOT^</td>
<td  style='display:none'>^SKC^</td>
<td  class='tdWrap'>^REM^</tr>
$ERX
 <tr bgcolor='silver'><td colspan='11' align='center'>@error  </td>
$END
</table>
<a href='javascript:paymentCmd()' class='btn btn-primary'>F2 PAYMENT</a>
<a href='javascript:cancelCmd()' class='btn btn-info'>Cancel</a>
<hr>

$REM

//END **/