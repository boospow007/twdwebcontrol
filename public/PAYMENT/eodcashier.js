﻿function pageReady() {
    // alert("EOD Cashier");
    oprDate =yyyymmdd();

    $('#forWho').html("FOR CASHIER: <b>"	 	+cashierId 
					+ "</b><br>STORE: <b>" 	+storeCode 
					+ "</b><br>LOCKNO: <b>" 	+lockNumber 
					+ "</b><br> OprDATE: <b>"	+oprDate +  " </b>");

    $('select').blur(function () {
        $('#iamount').val('');
        $('#iquant').val('');
        $('#iquant').focus();
    });

    //checkSales();

	//sumTotal();
	
    $('#iamount').blur(function () {
        addTender();
    });

  
}

function checkSales(){
var cmd  ="Select NRCV=NRECEIPT  from CLOUDPOS..CASHIERCTL ";
	cmd +=" where stcode='"+ storeCode +"' and status=' ' and datediff(dy, LOGINDATE,getdate())=0 ";
	cmd +=" and USERID='"+cashierId +"'";
	ajaxGetQuery(cmd,
			function(isok,resp){
				if(isok){
					var jsn = JSON.parse(resp);  //i ok then result is json 
					var nrcv= jsn.dbitems[0].NRCV;
					if (nrcv==0){
						upfAlert('Cashier ['+ casheirId +'] has not sales any item');
						//alert('No sales transaction for cashier '+cashierId);
						location.assign('/');
					 }
					//else getPrevious();
				}
				else alert('checkSales ..'+resp);
				}
			 );

}
//------------Read previous Data
function getPrevious(){
	var ctlf ="PAYMENT/eodcashier.txt";
	var para={'stcode':storeCode,'userid':cashierId};
       
		ajaxExpTab(ctlf, para, 
			function(bool,ret){
		//	alert(ret);
				$('#tenderTab').html(ret);
				}
		)  ;
			
  }
function keyAMPress(ev) {
    var x = ev.keyCode;
    if (x == 13) {
        addTender();
    }
}


function addTender() {
    try {
        //var insAt = $('#sumary tr:last').index(); //??? return 0 ????
        var tid = $('#itender').val();   //tender Id
        var tname = $('#itender').find('option:selected').text();  //---name of the tender
        var qnt = $('#iquant').val();
        var amt = $('#iamount').val();

        if (qnt.trim() == '' || amt.trim() == '') return; //--------Not Numeric
     
        //   alert(insAt);
        //---is tender already There ???
        var found=false;
        var insAt = 0;
        $('#sumary .citems').each(function(){
     
          var tend = $(this).find('td:eq(0)').text();
          console.log('Find '+tend+ ' to ' + tid);
          tend= tend.trim()
          if ( tend  == tid) {
            console.log('Found ');
            found=true;
            $(this).find('td:eq(2)').text(numericFormat(qnt));
            $(this).find('td:eq(3)').text(numericFormat(amt));
           // sumTotal();
          //return false;  //--exit from each ???
          }
          insAt+=1;
        });
 
        if(!found){
               //---------Add Item to Table summary
               console.log('Insert New '+insAt);
            var itm = "<tr class='citems' ><td>" + tid
            + "<td>" + tname
            + "<td align='right'>" + numericFormat(qnt)
            + "<td align='right'>" + numericFormat(amt) + "</tr>";
            $("#sumary tr:eq(" + insAt + ")").after(itm);
      
        }
        sumTotal();
        //---get back to Select
        $('#itender').focus();
    }
    catch (ex) { alert(ex.message); }
}

//------Sum total
function sumTotal() {
    var tqnt = 0;
    var tamt = 0;
    var mxrw =$('#sumary tr:last').index();

   for(var i =1;i<=mxrw;i++){
       var thisR =$('#sumary tr:eq('+i+')' );
       var qnt = $(thisR).find('td:eq(2)').text();
        var amt = $(thisR).find('td:eq(3)').text();
        tqnt += str2Number(qnt);
        tamt += str2Number(amt);
    };

    //----Update Total
    $('#totalLine td:eq(1)').text(numericFormat(tqnt));
    $('#totalLine td:eq(2)').text(numericFormat(tamt));
    return;
}
//--------Confirm Tender Count
function confirmEOC() {
 // alert('Confirm EOC to be defined');
 
try {
	var xml='';
 
	var xml ="<REQUEST><STCODE>"	+ storeCode 	+"</STCODE>"
            +"<LOCK>"		+ lockNumber	+"</LOCK>"
			+"<OPRDATE>"	+ oprDate 	+"</OPRDATE>"
            +"<CASHIER>" 	+ cashierId	+"</CASHIER>";

		$('#sumary tr:gt(0)').each(function(){
			var tdr=$(this).find('td:eq(0)').text();
			var qnt=$(this).find('td:eq(2)').text();
			var amt=$(this).find('td:eq(3)').text();
			qnt=qnt.replace(/,/g,'');
			amt=amt.replace(/,/g,'');
			if(tdr!='TOTAL')
				xml=xml+ '<ITEM><TDR>' + tdr +'</TDR><QNT>'+ qnt + '</QNT><AMT>'+ amt +'</AMT></ITEM>';
            });
			xml=xml+'</REQUEST>';

			 
			var cmd ="exec CLOUDPOS..ws_TenderCount  '" + xml +"' ";
			var pdata ={query:cmd,
					para:null}
			       ajaxExec(pdata, function(bool,ret ){
					if(bool){
						// alert(ret);
						var jsn=JSON.parse(ret);
						if (jsn.dbcode>0){
							alert(jsn.dbmessage);
						}
						else {
							upfAlert('Save EOC Succesful', 
									function(){
										//location.assign('default.htm');
										 //getPrevious();
										pageReload();
										});

									} 
						}
					
						else alert(ret);
					});
    		 
	}
 catch(ex){alert(ex.message);}
 
}

//--------Confirm Tender Cunt
function cancelEOC() {
  // alert('Cancel EOC to be defined');
 if(confirm('Change is not Save to DB, are you sure  (Y/N)')==1)
			location.assign('/');
}
//--------Confirm Tender Cunt
function clearEOC() {
   alert('You can only mark Tender amount to be 0.00 ');
return;

   if(confirm('Clear all input(Y/N)')==1)
       {
		var htm = "<thead><th>Tendor<th>Name <th> Quant<th>Amount</thead>";
		 htm +="<tr class='success' id ='totalLine'>";
		 htm +="<td colspan='2'>TOTAL <td align='right'>0.00<td align='right'>0.00</tr>";
		$('#sumary').html(htm);
		}

}

/**********
 $HDR

  <div class='container'>

  <div class='row'>
    <div  class='col col-md-6'>  
  <h2>Tender  Count: input</h2>

 <div id ='forWho'>

   FOR CASHIER <b>[#usr]</b> STORE <b>[#stc]</b> LOCKNO <b>[#lck] </b>ODATE<b>[#ldt] </b><br>

</div>

  <p>Count payment tender and put it to this box:</p>
  <form>
    <div class="form-group">
      <label for="itender">Tender:</label>
      <select class='form-control' id ='itender'>
      <option value='PPOS'>PPOS Pay@POS(CASH)</option>
      </select>
    </div>
    <div class="form-group">
      <label for="iquant">Count:</label>
      <input type="number" class="form-control" id="iquant">
    </div>
        <div class="form-group">
      <label for="iamount">Amount:</label>
      <input type="number" class="form-control" id="iamount"
        onkeypess="javascript:keyAMPress(event)" >
    </div>
  </form>
        <button type="button" id="addTender"  onclick="javascript:addTender()" class="btn btn-primary pull-left"  >Add Tender</button>
 
	
 </div>

 <div class='col col-md-6'>  
  <br>
  <h3>Summary of Tendor</h3>
   My last EOC REF is #eocref 
 <table id ='sumary' class="table table-striped"  >
 <thead><th>Tendor<th>Name <th> Quant<th>Amount</thead>
$SQL
use CLOUDPOS
  Select PCD,NM='PAY@POS',QNT,AMT 
	From   DLYPTRANS  
	where REF='#eocref' 
	and  datediff(dy,TTM,getdate()) =0
	and STY='C' and PCD='PPOS'
$BDY
	<tr class='citems'><td>:00
	<td>:01
	<td align='right'>:02
	<td align='right'>:03</tr>
$ERX
	<tr><td colspan='3'>@error
$CMP
  <tr class='success' id ='totalLine'>
  <td colspan='2'>TOTAL <td align='right'>@SUM(QNT)<td align='right'>@SUM(AMT) </tr>
$END
   </table>
  
   
 </div>
  
                
     <div class="modal-footer">
 
        <button type="button" id="payBtnOK"  onclick="javascript:confirmEOC()" class="btn btn-primary"  > Confirm</button>
        <button type="button" id="payBtnCC"  onclick="javascript:cancelEOC();" class="btn btn-default"  >Cancel</button>
		<button type="button" id="payBtnCL"  onclick="javascript:clearEOC();" class="btn btn-danger"  >Clear Entry</button>
      </div>
 </div>
 </div>


 $TXX
 |XID #option
 |HDR
 |SQL
  select  TENDOR,rtrim(TNAME) 
     from  TENDORS
    order by TENDOR
 |BDY
  <option VALUE =':00'>:01</option>
 |END

 $PRE
 Select [#eocref]=isnull(max(REF),'NONE')
	from CLOUDPOS..DLYPTRANS 
	where 
	STCODE ='@store' 
  and USR='@cashier'
  and STY='C'
	and left(REF,3)= '900'
 
 
 //***END***/
