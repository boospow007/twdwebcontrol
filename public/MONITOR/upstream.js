﻿function pageReady() {
	if (profileList.indexOf('0048') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู POS Upstream');
		location.assign("/");
	}
	
    $('#popUpDiv').mouseleave(function () {
        $('#popUpDiv').hide();
    });
}

function showXML(rm,rec) {
	 var url ="./XPDXML/UPSTREAM/"+rm+"/"+rec;
	 window.open(url,"XMLDETAIL");
	return;
}

function showThis(obj) {
	  try {
		  var stc = $(obj).find('td:eq(0)').text();
		  var art = $(obj).find('td:eq(1)').text();
		  var stt = $(obj).find('td:eq(2)').text();
		 			
		  $('#detDiv').html(waitgif);
		    var pat='MONITOR/upstreamDet.txt'; 
			var par ={'stcode':stc,'article':art,'status':stt  }; 

			 ajaxExpTab(pat, par,
		       function(bres,ret) {
				  if  (bres)  {
				    //-------Add object to wobj 
				     $('#detDiv').html(ret);
				   }
				 else  
					$('#detDiv').html("<b>ERROR..</b><br>"+ret);
			   });
		 }
		 catch(ex) {alert(ex.message);}
}
function showError(obj) {
	   try {
	     var stc = $(obj).find('td:eq(0)').text();
		 var art = $(obj).find('td:eq(1)').text();
		 var stt = $(obj).find('td:eq(2)').text();
	    // openMenu("SYSTEM\upstream.js&article=" + art+"&stcode="+stc);
  
		  $('#detDiv').html(waitgif);
		    var pat='./public/SYSTEM/upstreamErr.txt'; 
			var par ={'stcode':stc,'article':art,'status':stt  }; 

		 ajaxExpTab(pat, par,
	       function(bres,ret) {
			  if  (bres)  {
			    //-------Add object to wobj 
			     $('#detDiv').html(ret);
			   }
			 else  
				$('#detDiv').html("<b>ERROR..</b><br>"+ret);
		   });
	  
	}
	catch (ex) {alert(ex.message);}
}
/******
$HDR
<style>
#dataTab tr:hover {
  background-color:silver;
  cursor:pointer;
}
 .rndDiv2 {
		border:2px solid #a1a1a1;
		background: #A0C0D0;
		padding: 10px 40px;
		color:black;
		text-align:left;
		border-radius: 12px;
     }
#sumTab tr:hover{
   background-color:silver;
  cursor:pointer;
}
</style>


 

<div class='row'>
<br>
//<a href ='javascript:showError()' class='btn btn-info pull-right'>ERROR ONLY</a>
//<a href ='javascript:showSystem()' class='btn btn-primary pull-right'>System</a>

<div class='col-md-6' >
<h3> Upstream at [#server]</h3>
 	#summary
</div>

<div class='col-md-6' id ='detDiv'>

<h3>UPSTREAM on Cloud Server</h3>

<table id ='dataTab'  style='width:96%'>
<thead><th>Stcode<th>Time<th>Article<th>REF<th>ST<th>RECID</thead>
$SQL
use.PLPPSRV^
Select top 20 STCODE,MDATE,ARTICLE
,REF    
,XTIME=convert(varchar(8),MDATE,8)
,STATUS
,RECID
from  POSXFER..UPSTREAM 
where STCODE like '@store'
//where STCODE = '@store'
//and ARTICLE like '#article%'
order by MDATE DESC
$BDY
<tr onclick="javascript:showXML(1,'^RECID^');">
<td>^STCODE^</td>
<td>^MDATE^</td>
<td>^ARTICLE^</td>
<td>:03</td>
<td>^STATUS^</td>
<td>^RECID^</td>
$ERX
 <tr><td colspan='5'>@error
$END
</table>

</div>
</div>

<div id='popUpDiv' class='rndDiv2' style='display:none;z-index:6;position:absolute;top:80px;left:100px'>
	<h4>Task call this Procedure every 10 sec</h4>

	<div id='xmlTable' style='width:100%;'>

	</div>
</div>



$TAB
|XID #summary
|HDR
<table id ='sumTab' style='width:96%' >
<thead><th>Stcode<th>Time<th>Article<th>REF<th>ST<th>RECID</thead>
|SQL
use CLOUDPOS
Select top 50 STCODE,MDATE,ARTICLE
,REF 
,XTIME=convert(varchar(8),MDATE,8)
,STATUS
,RECID
from  UPSTREAM
where STCODE like '@store'
//where STCODE = '@store'
//and ARTICLE like '#article%'
order by MDATE DESC
|BDY
<tr  onclick="javascript:showXML(0,'^RECID^')";>
<td>^STCODE^</td>
<td>^MDATE^</td>
<td>^ARTICLE^</td>
<td>:03</td>
<td>^STATUS^</td>
<td>^RECID^</td>
|ERX
 <tr><td colspan='5'>@error
|END
</table>
 


$PRE
Select [#article]=Case When left('@article',1)='@' then '%' else '@article' end
       ,[#stcode]=Case When left('@stcode',1)='@' then '%' else '@stcode' end
		,[#server]=@@SERVERNAME
//END ***/

 