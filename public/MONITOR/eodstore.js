﻿function pageReady() {
	if (profileList.indexOf('0050') > -1) {} 
	else {
		alert('คุณไม่มีสิทธิ์ใช้งานเมนู EOD Monitor');
		location.assign("/");
	}

   var obj =$('#dataTab tr:eq(1)');
   showDetail(obj);
}
 

function showDetail(obj) {
	  try {
		  var stc   = $(obj).find('td:eq(1)').text();
		  var dte   = $(obj).find('td:eq(0)').text();
		  
		 			
		  $('#detDiv').html(waitgif);
		    var pat='MONITOR/eodstoreDet.txt'; 
			var par ={'stcode':stc,'sdate':dte  }; 

			 ajaxExpTab(pat, par,
		       function(bres,ret) {
				  if  (bres)  {
				    //-------Add object to wobj 
					 $('#detDiv').html(ret);
					 setNumericFormatDiv('detDiv')
				   }
				 else  
					$('#detDiv').html("<b>ERROR..</b><br>"+ret);
			   });
		 }
		 catch(ex) {alert('Show Detail Error ..'+ ex.message);}
}
 
/******
$HDR
<style>
#dataTab tr:hover {
  background-color:silver;
  cursor:pointer;
}
 .rndDiv2 {
		border:2px solid #a1a1a1;
		background: #A0C0D0;
		padding: 10px 40px;
		color:black;
		text-align:left;
		border-radius: 12px;
     }
#sumTab tr:hover{
   background-color:silver;
  cursor:pointer;
}
</style>
 

<div class='row'>
 
<div class='col-md-4' >
 <h3> EOD Monitor on [#server]</h3>
 <h4>Table UPFPOS..EODSTORE</h4>

<table id ='dataTab' style='width:96%' >
<thead><th>SDATE<th>Store<th>NRCV<th>AMOUNT<th>NLOCKIN</thead>

$SQL
use.PLPPSRV^
select top 40  *  from  UPFPOS.dbo.EODSTORE 
where STCODE in('920','974')
order by SDATE DESC
$BDY
<tr  onclick="javascript:showDetail(this)";>
<td>^SDATE^</td>
<td>^STCODE^</td>
<td>^NRCV^</td>
<td class='Numeric'>^AMOUNT^</td>
<td align ='right'>^NLOCK^</td>
 
$ERX
 <tr><td colspan='5'>@error
$END
</table>
</div>
<div class='col col-md-8'>
   
	<div id='detDiv' style='width:100%;'>
        Detail to be displayed here
    </div>
    
</div>

 

</div>



$PRE
use.PLPPSRV^
Select [#article]=Case When left('@article',1)='@' then '%' else '@article' end
    ,[#stcode]=Case When left('@stcode',1)='@' then '%' else '@stcode' end
	,[#server]=@@SERVERNAME
//END ***/

 